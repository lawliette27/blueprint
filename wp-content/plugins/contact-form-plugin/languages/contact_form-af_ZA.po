msgid ""
msgstr ""
"Project-Id-Version: contact_form\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-04-08 13:34+0300\n"
"PO-Revision-Date: 2014-04-08 13:34+0300\n"
"Last-Translator: bestwebsoft.com <plugins@bestwebsoft.com>\n"
"Language-Team: Aldu <alducornelissen@gmail.com>\n"
"Language: af\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Generator: Poedit 1.6.4\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SearchPath-0: .\n"

#: contact_form.php:73
#: contact_form.php:788
msgid "Contact Form Settings"
msgstr "Kontak Form Verstellings"

#: contact_form.php:73
msgid "Contact Form"
msgstr "Kontak Form"

#: contact_form.php:149
#: contact_form.php:1106
#: contact_form.php:1140
msgid "Name:"
msgstr "Naam:"

#: contact_form.php:150
#: contact_form.php:1107
#: contact_form.php:1141
msgid "Address:"
msgstr "Adres:"

#: contact_form.php:151
#: contact_form.php:1108
#: contact_form.php:1142
msgid "Email Address:"
msgstr "Epos Adres:"

#: contact_form.php:152
#: contact_form.php:1109
#: contact_form.php:1143
msgid "Phone number:"
msgstr "Telefoon nommer:"

#: contact_form.php:153
#: contact_form.php:1110
#: contact_form.php:1144
msgid "Subject:"
msgstr "Onderwerp:"

#: contact_form.php:154
#: contact_form.php:1111
#: contact_form.php:1145
msgid "Message:"
msgstr "Boodskap:"

#: contact_form.php:155
#: contact_form.php:1112
#: contact_form.php:1146
msgid "Attachment:"
msgstr "Aanhegsel:"

#: contact_form.php:156
msgid "Supported file types: HTML, TXT, CSS, GIF, PNG, JPEG, JPG, TIFF, BMP, AI, EPS, PS, RTF, PDF, DOC, DOCX, XLS, ZIP, RAR, WAV, MP3, PPT. Max file size: 2MB"
msgstr "Ondersteunde datalêer tipes: HTML, TXT, CSS, GIF, PNG, JPEG, JPG, TIFF, BMP, AI, EPS, PS, RTF, PDF, DOC, DOCX, XLS, ZIP, RAR, WAV, MP3, PPT. Max Datei-Größe: 2MB"

#: contact_form.php:157
#: contact_form.php:1114
#: contact_form.php:1148
msgid "Send me a copy"
msgstr "Stuur vir my 'n kopie"

#: contact_form.php:158
#: contact_form.php:1115
#: contact_form.php:1149
msgid "Submit"
msgstr "Dien in"

#: contact_form.php:159
msgid "Your name is required."
msgstr "Jou naam word benodig."

#: contact_form.php:160
msgid "Address is required."
msgstr "Adres word benodig."

#: contact_form.php:161
msgid "A valid email address is required."
msgstr "'n Geldige epos adres word benodig."

#: contact_form.php:162
msgid "Phone number is required."
msgstr "'n Telefoon nommer word benodig."

#: contact_form.php:163
msgid "Subject is required."
msgstr "'n Onderwerp word benodig."

#: contact_form.php:164
msgid "Message text is required."
msgstr "'n Boodskap teks word benodig ."

#: contact_form.php:165
msgid "File format is not valid."
msgstr "Datalêer formaat is nie geldig nie."

#: contact_form.php:166
msgid "File upload error."
msgstr "Oplaai van datalêer foutief."

#: contact_form.php:167
msgid "The file could not be uploaded."
msgstr "Die datalêer kon nie opgelaai word nie."

#: contact_form.php:168
msgid "This file is too large."
msgstr "Die datalêer is te groot."

#: contact_form.php:169
msgid "Please fill out the CAPTCHA."
msgstr "Vul asseblief die CAPTCHA in."

#: contact_form.php:170
msgid "Please make corrections below and try again."
msgstr "Korregeer asseblief die foute hierdoner en probeer weer."

#: contact_form.php:172
msgid "Thank you for contacting us."
msgstr "Dankie dat jy ons gekontak het."

#: contact_form.php:325
msgid "requires"
msgstr "benodig"

#: contact_form.php:325
msgid "or higher, that is why it has been deactivated! Please upgrade WordPress and try again."
msgstr "of hoër, dit is waarom dit gedeaktiveer is! Opgradeer asseblief WordPress, en probeer weer."

#: contact_form.php:325
msgid "Back to the WordPress"
msgstr "Terug na WordPress."

#: contact_form.php:325
msgid "Plugins page"
msgstr "Plugin blad"

#: contact_form.php:628
msgid "If the 'Redirect to page' option is selected then the URL field should be in the following format"
msgstr "As die 'Redirect to page' opsie geselekteer is moet die URL veld in die volgende formaat wees"

#: contact_form.php:637
msgid "Such user does not exist. Settings are not saved."
msgstr "Hierdie gebruiker bestaan nie. Die instellings is nie gestoor nie."

#: contact_form.php:641
#: contact_form.php:647
msgid "Please enter a valid email address in the 'FROM' field. Settings are not saved."
msgstr "Vul asseblief 'n geldige epos adres in die 'FROM/VAN' veld. Die verstellings is nie gestoor nie."

#: contact_form.php:663
msgid "Settings saved."
msgstr "Verstellings is gestoor."

#: contact_form.php:690
#: contact_form.php:722
msgid "Wrong license key"
msgstr "Verkeerde lisensie sleutel"

#: contact_form.php:715
msgid "Something went wrong. Try again later. If the error will appear again, please, contact us <a href=http://support.bestwebsoft.com>BestWebSoft</a>. We are sorry for inconvenience."
msgstr "Iets het fout gegaan. Probeer weer later. As die foutboodskap weer verskyn, kontak ons asseblief by <a href=http://support.bestwebsoft.com>BestWebSoft</a>. Ons is jammer vir die ongerief."

#: contact_form.php:724
msgid "This license key is bind to another site"
msgstr "Hierdie lisensie sleutel is verbind met 'n ander webblad."

#: contact_form.php:726
#: contact_form.php:1536
msgid "Unfortunately, you have exceeded the number of available tries per day. Please, upload the plugin manually."
msgstr "Ongelukkig het u die aantal beskikbare probeerslae per dag oorskry. Laai asseblief die plugin handmatig op."

#: contact_form.php:743
msgid "Failed to open the zip archive. Please, upload the plugin manually"
msgstr "Mislukking om zip Argiewe oop te maak. Laai asseblief die plugin handmatig op."

#: contact_form.php:749
msgid "Your server does not support either ZipArchive or Phar. Please, upload the plugin manually"
msgstr "U server ondersteun of nie ZipArchive of Phar nie. Laai asseblief die plugin handmatig op."

#: contact_form.php:753
#: contact_form.php:762
msgid "Failed to download the zip archive. Please, upload the plugin manually"
msgstr "Mislukking om zip Argiewe af te laai. Laai asseblief die plugin handmatig op."

#: contact_form.php:766
msgid "Something went wrong. Try again later or upload the plugin manually. We are sorry for inconvienience."
msgstr "Iets het verkeerd gegaan. Probeer weer later, of laai die plugin op handmatig. Ons is jammer vir die ongerief."

#: contact_form.php:781
msgid "Please, enter Your license key"
msgstr "Sleutel asseblief u lisensie sleutel in"

#: contact_form.php:790
#: contact_form.php:2181
#: contact_form.php:2192
msgid "Settings"
msgstr "Verstellings"

#: contact_form.php:791
msgid "Extra settings"
msgstr "Ekstra verstellings"

#: contact_form.php:792
msgid "Go PRO"
msgstr "Gaan PRO"

#: contact_form.php:795
msgid "Notice:"
msgstr "Kennisgewing:"

#: contact_form.php:795
msgid "The plugin's settings have been changed. In order to save them please don't forget to click the 'Save Changes' button."
msgstr "Die plugin se verstellings het verander. Onthou om 'Save Changes' te kliek om die veranderinge te stoor."

#: contact_form.php:801
#: contact_form.php:806
#: contact_form.php:1495
msgid "If you would like to add the Contact Form to your website, just copy and paste this shortcode to your post or page or widget:"
msgstr "As u die Kontak Vorm by u webstuiste wil bylas, kopieër en plak net die kortkode na u post, bladsy of widget:"

#: contact_form.php:801
#: contact_form.php:802
#: contact_form.php:806
#: contact_form.php:807
#: contact_form.php:1130
#: contact_form.php:1132
#: contact_form.php:1187
#: contact_form.php:1189
msgid "or"
msgstr "of"

#: contact_form.php:802
#: contact_form.php:807
msgid "If have any problems with the standard shortcode [contact_form], you should use the shortcode"
msgstr "As u enige probelem het met die standaard kortkode [contact_form], moet u die volgende kortkodes gebruik"

#: contact_form.php:803
#: contact_form.php:808
msgid "They work the same way."
msgstr "Hulle werk op dieselfde manier."

#: contact_form.php:804
#: contact_form.php:809
msgid "If you leave the fields empty, the messages will be sent to the email address specified during registration."
msgstr "As u die velde leeg laat, sal die boodskappe gestuur word na die epos adres wat gespesifiseer was gedurende registrasie"

#: contact_form.php:814
msgid "The user's email address:"
msgstr "Die gebruiker se epos adres:"

#: contact_form.php:818
msgid "Create a username"
msgstr "Skep 'n gebruikersnaam"

#: contact_form.php:823
msgid "Enter a username of the person who should get the messages from the contact form."
msgstr "Sleutel 'n gebruikersnaam in vir die persoon wat die boodskappe moet kry van die kontak vorm."

#: contact_form.php:827
msgid "Use this email address:"
msgstr "Gebruik hierdie epos adres:"

#: contact_form.php:830
msgid "Enter the email address you want the messages forwarded to."
msgstr "Sleutel in die epos adres in waar u die boodskappe aangestuur wil he."

#: contact_form.php:836
msgid "Add department selectbox to the contact form:"
msgstr "Voeg 'n departementele keuse-boks by die kontak vorm:"

#: contact_form.php:844
#: contact_form.php:1390
msgid "If you upgrade to Pro version all your settings will be saved."
msgstr "As u opgradeer na die Pro weergawe sal die verstellings gestoor word."

#: contact_form.php:849
#: contact_form.php:982
#: contact_form.php:1051
#: contact_form.php:1240
msgid "This functionality is available in the Pro version of the plugin. For more details, please follow the link"
msgstr "Hierdie funksie is beskikbaar in die Pro weergawe van die plugin. Vir meer besonderhede, volg die skakel:"

#: contact_form.php:850
#: contact_form.php:983
#: contact_form.php:1052
#: contact_form.php:1241
msgid "Contact Form Pro"
msgstr "Kontak Form Pro"

#: contact_form.php:856
msgid "Save emails to the database"
msgstr "Stoor eposse na die databasis."

#: contact_form.php:862
msgid "Using"
msgstr "In Gebruik"

#: contact_form.php:862
#: contact_form.php:1029
#: contact_form.php:1032
#: contact_form.php:1036
msgid "powered by"
msgstr "Verrig deur"

#: contact_form.php:865
#: contact_form.php:869
msgid "Using Contact Form to DB powered by"
msgstr "Gebruik van Kontak Vorm na DB verrig deur"

#: contact_form.php:865
msgid "Activate Contact Form to DB"
msgstr "Aktiveer Kontak Vorm na DB"

#: contact_form.php:869
msgid "Download Contact Form to DB"
msgstr "Laai Kontak Vorm na DB af"

#: contact_form.php:874
msgid "Additional options"
msgstr "Addisionele opsies"

#: contact_form.php:876
msgid "Show"
msgstr "Besigtig"

#: contact_form.php:877
msgid "Hide"
msgstr "Versteek"

#: contact_form.php:881
msgid "What to use?"
msgstr "Wat om te gebruik?"

#: contact_form.php:884
msgid "Wp-mail"
msgstr "Wp-mail"

#: contact_form.php:884
msgid "You can use the wp_mail function for mailing"
msgstr "U kan die wp_mail funksie gebruik vir eposse"

#: contact_form.php:886
msgid "Mail"
msgstr "Mail"

#: contact_form.php:886
msgid "To send mail you can use the php mail function"
msgstr "Om eposse te stuur kan u die php epos funksie gebruik"

#: contact_form.php:890
msgid "The text in the 'From' field"
msgstr "Die teks in die 'From/Van' veld"

#: contact_form.php:892
msgid "User name"
msgstr "Gebruikersnaam"

#: contact_form.php:893
msgid "The name of the user who fills the form will be used in the field 'From'."
msgstr "Die naam van die gebruiker wie die vorm invul sal gebruik word vir die 'From/Van' veld."

#: contact_form.php:896
msgid "This text will be used in the 'FROM' field"
msgstr "Hierdie teks sal gebruik word in die 'FROM/VAN' veld"

#: contact_form.php:900
msgid "The email address in the 'From' field"
msgstr "Die epos adres in die 'From/Van' veld"

#: contact_form.php:902
msgid "User email"
msgstr "Gebruiker epos"

#: contact_form.php:903
msgid "The email address of the user who fills the form will be used in the field 'From'."
msgstr "Die epos adres van die gebruiker wie die vorm invul sal gebruik word in die 'From/van' veld."

#: contact_form.php:906
msgid "This email address will be used in the 'From' field."
msgstr "Hierdie epos adres sal gebruik word in die 'From/Van' veld."

#: contact_form.php:910
msgid "Required symbol"
msgstr "Benodigde simbool"

#: contact_form.php:920
msgid "Fields"
msgstr "Velde"

#: contact_form.php:921
msgid "Used"
msgstr "Gebruik"

#: contact_form.php:922
msgid "Required"
msgstr "Benodig"

#: contact_form.php:923
msgid "Visible"
msgstr "Sigbaar"

#: contact_form.php:924
msgid "Disabled for editing"
msgstr "Gedeaktiveer vir wysiging"

#: contact_form.php:925
msgid "Field's default value"
msgstr "Veld se verstekwaarde"

#: contact_form.php:930
#: contact_form.php:1265
#: contact_form.php:2024
#: contact_form.php:2054
msgid "Name"
msgstr "Naam"

#: contact_form.php:938
#: contact_form.php:1270
#: contact_form.php:2028
#: contact_form.php:2056
msgid "Address"
msgstr "Adres: "

#: contact_form.php:946
msgid "Email Address"
msgstr "Epos Adres:"

#: contact_form.php:954
msgid "Phone number"
msgstr "Telefoon nommer:"

#: contact_form.php:962
#: contact_form.php:1285
#: contact_form.php:2038
#: contact_form.php:2060
msgid "Subject"
msgstr "Onderwerp"

#: contact_form.php:970
#: contact_form.php:1289
#: contact_form.php:2041
#: contact_form.php:2061
msgid "Message"
msgstr "Boodskap"

#: contact_form.php:988
msgid "Attachment block"
msgstr "Aanhegsel blok"

#: contact_form.php:990
msgid "Users can attach the following file formats"
msgstr "Gebruikers kan die volgende dataleêr formate aanheg"

#: contact_form.php:1004
msgid "Add to the form"
msgstr "Voeg by die vorm by"

#: contact_form.php:1009
msgid "Tips below the Attachment"
msgstr "Wenke onder die Aanhegsel"

#: contact_form.php:1018
msgid "'Send me a copy' block"
msgstr "'Stuur vir my 'n kopie' blok"

#: contact_form.php:1029
#: contact_form.php:1032
#: contact_form.php:1036
#: contact_form.php:1299
msgid "Captcha"
msgstr "Captcha"

#: contact_form.php:1032
msgid "Activate captcha"
msgstr "Aktiveer captcha"

#: contact_form.php:1036
msgid "Download captcha"
msgstr "Laai captcha af"

#: contact_form.php:1044
msgid "Agreement checkbox"
msgstr "Ooreenstemming checkbox"

#: contact_form.php:1044
msgid "Required checkbox for submitting the form"
msgstr "Benodigde checkbox om die vorm in te dien"

#: contact_form.php:1045
msgid "Optional checkbox"
msgstr "Opsie checkbox"

#: contact_form.php:1045
msgid "Optional checkbox, the results of which will be displayed in email"
msgstr "Opsionele checkbox, die resultate waarvan in die epos sal verskyn"

#: contact_form.php:1056
msgid "Delete an attachment file from the server after the email is sent"
msgstr "Verwyder 'n aanhegsel dataleêr van die server na epos gestuur is"

#: contact_form.php:1062
msgid "Email in HTML format sending"
msgstr "E-Mail in HTML Format"

#: contact_form.php:1066
msgid "Display additional info in the email"
msgstr "Toon addisionele inligting in die epos"

#: contact_form.php:1071
#: contact_form.php:1991
#: contact_form.php:1993
msgid "Sent from (ip address)"
msgstr "Gestuur van (IP-Adres)"

#: contact_form.php:1071
msgid "Example: Sent from (IP address):\t127.0.0.1"
msgstr "Voorbeeld: Gestuur van (IP-Adres):\t127.0.0.1"

#: contact_form.php:1072
#: contact_form.php:1997
#: contact_form.php:1999
msgid "Date/Time"
msgstr "Datum/Tyd"

#: contact_form.php:1072
msgid "Example: Date/Time:\tAugust 19, 2013 8:50 pm"
msgstr "Voorbeeld: Datum/Tyd:\tAugustus 19, 2013 8:50 pm"

#: contact_form.php:1073
#: contact_form.php:2003
#: contact_form.php:2005
msgid "Sent from (referer)"
msgstr "Gestuur van (referer)"

#: contact_form.php:1073
msgid "Example: Sent from (referer):\thttp://bestwebsoft.com/contacts/contact-us/"
msgstr "Voorbeeld: Gestuur van (referer):\thttp://bestwebsoft.com/kontakte/kontak-ons/"

#: contact_form.php:1074
#: contact_form.php:2009
#: contact_form.php:2011
msgid "Using (user agent)"
msgstr "Gebruik (user agent)"

#: contact_form.php:1074
msgid "Example: Using (user agent):\tMozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36"
msgstr "Voorbeeld: Gebruik (user agent):\tMozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36"

#: contact_form.php:1078
msgid "Language settings for the field names in the form"
msgstr "Taal verstellings vir die naamvelde in die vorm"

#: contact_form.php:1087
msgid "Add a language"
msgstr "Voeg 'n taal by"

#: contact_form.php:1091
msgid "Change the names of the contact form fields and error messages"
msgstr "Verander die name van die kontak vorm velde en foutboodskappe"

#: contact_form.php:1096
#: contact_form.php:1177
msgid "English"
msgstr "Engels"

#: contact_form.php:1104
#: contact_form.php:1138
msgid "click to expand/hide the list"
msgstr "Kliek om uit te brei/lys te versteek"

#: contact_form.php:1113
#: contact_form.php:1147
msgid "Tips below the Attachment block"
msgstr "Wenke onder toe Aanhegsel blok"

#: contact_form.php:1116
#: contact_form.php:1150
msgid "Error message for the Name field"
msgstr "FFoutboodskap vir die Naamveld"

#: contact_form.php:1117
#: contact_form.php:1151
msgid "Error message for the Address field"
msgstr "Foutboodskap vir die Adres veld"

#: contact_form.php:1118
#: contact_form.php:1152
msgid "Error message for the Email field"
msgstr "Foutboodskap vir die Epos veld"

#: contact_form.php:1119
#: contact_form.php:1153
msgid "Error message for the Phone field"
msgstr "Foutboodskap vir die Telefoon Nommer veld"

#: contact_form.php:1120
#: contact_form.php:1154
msgid "Error message for the Subject field"
msgstr "Foutboodskap vir die Onderwerp veld"

#: contact_form.php:1121
#: contact_form.php:1155
msgid "Error message for the Message field"
msgstr "Foutboodskap vir die Boodskap veld"

#: contact_form.php:1122
#: contact_form.php:1156
msgid "Error message about the file type for the Attachment field"
msgstr "Foutboodskap oor die dataleêr tipe vir die Aanhegsel veld"

#: contact_form.php:1123
#: contact_form.php:1157
msgid "Error message while uploading a file for the Attachment field to the server"
msgstr "Foutboodskap tydens dataleêr oplaai vir die Aanhegsel veld na die server"

#: contact_form.php:1124
#: contact_form.php:1158
msgid "Error message while moving the file for the Attachment field"
msgstr "Foutboodskap tydens die skuif van dataleêr vir die Aanhegsel veld"

#: contact_form.php:1125
#: contact_form.php:1159
msgid "Error message when file size limit for the Attachment field is exceeded"
msgstr "Foutboodskap wanneer die dataleêr limiet vir die Aanhegsel veld oorskry is"

#: contact_form.php:1126
#: contact_form.php:1160
msgid "Error message for the Captcha field"
msgstr "Foutboodskap vir die Captcha veld"

#: contact_form.php:1127
#: contact_form.php:1161
msgid "Error message for the whole form"
msgstr "Foutboodskap vir die hele vorm"

#: contact_form.php:1130
#: contact_form.php:1132
#: contact_form.php:1164
#: contact_form.php:1166
#: contact_form.php:1187
#: contact_form.php:1189
#: contact_form.php:1197
#: contact_form.php:1199
msgid "Use shortcode"
msgstr "Gebruik kortkode"

#: contact_form.php:1130
#: contact_form.php:1132
#: contact_form.php:1164
#: contact_form.php:1166
#: contact_form.php:1187
#: contact_form.php:1189
#: contact_form.php:1197
#: contact_form.php:1199
msgid "for this language"
msgstr "vir hierdie taal"

#: contact_form.php:1174
msgid "Action after email is sent"
msgstr "Aksie na epos gestuur is"

#: contact_form.php:1176
msgid "Display text"
msgstr "Toon teks"

#: contact_form.php:1185
#: contact_form.php:1195
msgid "Text"
msgstr "teks"

#: contact_form.php:1206
msgid "Redirect to the page"
msgstr "Herlei na die bladsy"

#: contact_form.php:1207
msgid "Url"
msgstr "Url"

#: contact_form.php:1211
msgid "The $_SERVER variable that is used to build a URL of the form"
msgstr ""

#: contact_form.php:1215
msgid "If you are not sure whether to change this setting or not, please do not do that."
msgstr ""

#: contact_form.php:1221
#: contact_form.php:1396
msgid "Save Changes"
msgstr "Stoor veranderinge"

#: contact_form.php:1226
msgid "If you enjoy our plugin, please give it 5 stars on WordPress"
msgstr "As u die plugin geniet, gee dit asseblief 5 sterre op WordPress"

#: contact_form.php:1227
msgid "Rate the plugin"
msgstr "Prys die plugin"

#: contact_form.php:1230
msgid "If there is something wrong about it, please contact us"
msgstr "As daar iets fout is met dit, kontak ons asseblief"

#: contact_form.php:1245
msgid "Errors output"
msgstr "Foutboodskap uitset"

#: contact_form.php:1248
msgid "Display error messages"
msgstr "Toon foutboodskappe"

#: contact_form.php:1249
msgid "Color of the input field errors."
msgstr "Kleur van die inset veld foutboodskappe"

#: contact_form.php:1250
msgid "Display error messages & color of the input field errors"
msgstr "Toon foutboodskappe & kleur van die inset veld foutboodskappe"

#: contact_form.php:1255
msgid "Add placeholder to the input blocks"
msgstr "Voeg 'n plekhouer by die inset blokke"

#: contact_form.php:1261
msgid "Add tooltips"
msgstr "Voeg tool-wenke"

#: contact_form.php:1275
msgid "Email address"
msgstr "Epos Adres:"

#: contact_form.php:1280
msgid "Phone Number"
msgstr "Telefoon Nommer:"

#: contact_form.php:1294
msgid "Attachment"
msgstr "Aanhegsel:"

#: contact_form.php:1299
msgid "(powered by bestwebsoft.com)"
msgstr "(Verrig deur bestwebsoft.com)"

#: contact_form.php:1304
msgid "Style options"
msgstr "Styl opsies"

#: contact_form.php:1307
msgid "Text color"
msgstr "Teks kleur"

#: contact_form.php:1310
#: contact_form.php:1315
#: contact_form.php:1325
#: contact_form.php:1330
#: contact_form.php:1335
#: contact_form.php:1340
#: contact_form.php:1350
#: contact_form.php:1355
#: contact_form.php:1361
#: contact_form.php:1372
#: contact_form.php:1377
#: contact_form.php:1382
msgid "Default"
msgstr "Verstek"

#: contact_form.php:1312
msgid "Label text color"
msgstr "Etiket teks kleur"

#: contact_form.php:1317
msgid "Placeholder color"
msgstr "Plekhouer kleur"

#: contact_form.php:1322
msgid "Errors color"
msgstr "Foutboodskap kleur"

#: contact_form.php:1327
msgid "Error text color"
msgstr "Foutboodskap teks kleur"

#: contact_form.php:1332
msgid "Background color of the input field errors"
msgstr "Agtergrond kleur van die inset veld foutboodskappe"

#: contact_form.php:1337
msgid "Border color of the input field errors"
msgstr "Rand-kleur van die inset veld foutboodskappe"

#: contact_form.php:1342
msgid "Placeholder color of the input field errors"
msgstr "Plekhouer kleur van die inset veld foutboodskappe"

#: contact_form.php:1347
msgid "Input fields"
msgstr "Inset velde"

#: contact_form.php:1352
msgid "Input fields background color"
msgstr "Inset velde agtergrond kleur"

#: contact_form.php:1357
msgid "Text fields color"
msgstr "Teks velde kleur"

#: contact_form.php:1359
msgid "Border width in px, numbers only"
msgstr "Rand wydte in px, nommers alleenlik"

#: contact_form.php:1363
#: contact_form.php:1384
msgid "Border color"
msgstr "Rand kleur"

#: contact_form.php:1368
msgid "Submit button"
msgstr "Indien knoppie"

#: contact_form.php:1370
msgid "Width in px, numbers only"
msgstr "Wydte in px, nommers alleenlik"

#: contact_form.php:1374
msgid "Button color"
msgstr "Knoppie Kleur"

#: contact_form.php:1379
msgid "Button text color"
msgstr "Knoppie teks kleur"

#: contact_form.php:1400
msgid "Contact Form Pro | Preview"
msgstr "Kontak Vorm Pro | Voorskou"

#: contact_form.php:1403
msgid "Show with errors"
msgstr "Toon met foutboodskappe"

#: contact_form.php:1411
#: contact_form.php:1413
msgid "Please enter your full name..."
msgstr "Vul asseblief u volle name in..."

#: contact_form.php:1424
#: contact_form.php:1426
msgid "Please enter your address..."
msgstr "Vul asseblief u adres in... "

#: contact_form.php:1435
#: contact_form.php:1437
msgid "Please enter your email address..."
msgstr "Vul asseblief u epos adres in..."

#: contact_form.php:1446
#: contact_form.php:1448
msgid "Please enter your phone number..."
msgstr "Vul asseblief u telefoon nommer in... "

#: contact_form.php:1457
#: contact_form.php:1459
msgid "Please enter subject..."
msgstr "Vul asseblief 'n onderwerp in... "

#: contact_form.php:1467
#: contact_form.php:1469
msgid "Please enter your message..."
msgstr "Vul asseblief jou boodskap in... "

#: contact_form.php:1511
msgid "Congratulations! The PRO version of the plugin is successfully download and activated."
msgstr "Geluk! Die PRO weergawe van die plugin het suksesvol afgelaai en is geaktiveer"

#: contact_form.php:1513
msgid "Please, go to"
msgstr "Gaan asseblief na"

#: contact_form.php:1513
msgid "the setting page"
msgstr "Die verstelling bladsy"

#: contact_form.php:1514
msgid "You will be redirected automatically in 5 seconds."
msgstr "U sal outomaties herlei word in 5 sekondes."

#: contact_form.php:1519
msgid "You can download and activate"
msgstr "U kan aflaai en aktiveer"

#: contact_form.php:1521
msgid "version of this plugin by entering Your license key."
msgstr "weergawe van hierdie plugin deurom u lisensiekode in te vul."

#: contact_form.php:1523
msgid "You can find your license key on your personal page Client area, by clicking on the link"
msgstr "U kan u lisensiekode vind op u persoonlike bladsy Klient area, deurom te kliek op die skakel"

#: contact_form.php:1525
msgid "(your username is the email you specify when purchasing the product)."
msgstr "(u gebruikersnaam is die epos wat u gespesifiseer het toe u die produk aangekoop het)."

#: contact_form.php:1533
#: contact_form.php:1543
msgid "Go!"
msgstr "Go!"

#: contact_form.php:1613
msgid "Sorry, email message could not be delivered."
msgstr "Verskoning, die epos kon nie gestuur word nie."

#: contact_form.php:2018
msgid "Contact from"
msgstr "Kontak Vorm"

#: contact_form.php:2031
#: contact_form.php:2057
msgid "Email"
msgstr "Epos"

#: contact_form.php:2035
#: contact_form.php:2059
msgid "Phone"
msgstr "Telefoon Nommer"

#: contact_form.php:2044
#: contact_form.php:2062
msgid "Site"
msgstr "Webtuiste"

#: contact_form.php:2125
msgid "If you can see this MIME, it means that the MIME type is not supported by your email client!"
msgstr "As u die MIME kan sie, beteken dit dat die MIME tipe nie ondersteun word deur u epos klient nie!"

#: contact_form.php:2193
msgid "FAQ"
msgstr "Gereeld Gestelde Vrae (FAQ)"

#: contact_form.php:2194
msgid "Support"
msgstr "Steun"

#: contact_form.php:2243
msgid "Are you sure that you want to delete this language data?"
msgstr "Is u seker dat u die taal data wil verwyder?"

#: contact_form.php:2438
msgid "It’s time to upgrade your <strong>Contact Form plugin</strong> to <strong>PRO</strong> version"
msgstr ""

#: contact_form.php:2439
msgid "Extend standard plugin functionality with new great options."
msgstr ""

#: contact_form.php:2454
msgid "<strong>Contact Form to DB</strong> allows to store your messages to the database."
msgstr ""

#: contact_form.php:2455
msgid "Manage messages that have been sent from your website."
msgstr ""

#~ msgid "Not set"
#~ msgstr "Nie gestel nie"

#~ msgid "On"
#~ msgstr "Aan"

#~ msgid "Off"
#~ msgstr "Af"

#~ msgid "N/A"
#~ msgstr "N / A"

#~ msgid "Yes"
#~ msgstr "Ja"

#~ msgid "No"
#~ msgstr "Nee"

#~ msgid "Operating System"
#~ msgstr "Operating system"

#~ msgid "Server"
#~ msgstr "Server"

#~ msgid "Memory usage"
#~ msgstr "Geheue in gebruik"

#~ msgid "MYSQL Version"
#~ msgstr "MYSQL Weergawe"

#~ msgid "SQL Mode"
#~ msgstr "SQL Mode"

#~ msgid "PHP Version"
#~ msgstr "PHP Weergawe"

#~ msgid "PHP Safe Mode"
#~ msgstr "PHP Veilige Mode"

#~ msgid "PHP Allow URL fopen"
#~ msgstr "PHP Laat URLfopen toe"

#~ msgid "PHP Memory Limit"
#~ msgstr "PHP Geheue Limiet"

#~ msgid "PHP Max Upload Size"
#~ msgstr "PHP Maks Oplaai Grootte"

#~ msgid "PHP Max Post Size"
#~ msgstr "PHP Maks 'Post' Grootte"

#~ msgid "PHP Max Script Execute Time"
#~ msgstr "PHP Maks 'Script' Uitvoer Tyd"

#~ msgid "PHP XML support"
#~ msgstr "PHP XML ondersteuning"

#~ msgid "Site URL"
#~ msgstr "Webtuiste URL"

#~ msgid "Home URL"
#~ msgstr "Tuis URL"

#~ msgid "Please enter a valid email address."
#~ msgstr "Vul asseblief 'n geldige epos adres in:"

#~ msgid "Installed"
#~ msgstr "Geïnstalleer"

#~ msgid "Recommended"
#~ msgstr "Aanbeveel"

#~ msgid "Installed plugins"
#~ msgstr "Geïnstalleerde Plugins"

#~ msgid "Activate this plugin"
#~ msgstr "Geaktiveer hierdie plugin"

#~ msgid "Activate"
#~ msgstr "Aktiveer"

#~ msgid "Recommended plugins"
#~ msgstr "Aanbevolen Plugins"

#~ msgid "Install now"
#~ msgstr "Installeer nou"

#~ msgid "Install %s"
#~ msgstr "Installeer %s"

#~ msgid "Install Now"
#~ msgstr "Installa Ora"

#~ msgid "Active Plugins"
#~ msgstr "Aktiewe Plugins"

#~ msgid "Inactive Plugins"
#~ msgstr "Onaktiewe Plugins"

#~ msgid "Send to support"
#~ msgstr "Stuur vir bystand"

#, fuzzy
#~ msgid "Contact Form Pro Extra Settings"
#~ msgstr "Contact Form Opties"

#, fuzzy
#~ msgid "Contact Form Pro | Extra Settings"
#~ msgstr "Contact Form Opties"

#, fuzzy
#~ msgid "Display fields"
#~ msgstr "Toon tekst"

#, fuzzy
#~ msgid "Display tips below the Attachment block"
#~ msgstr "Toon uitleg na Bijlage knop"

#~ msgid "Required fields"
#~ msgstr "Verplicht veld"

#, fuzzy
#~ msgid "Display the asterisk near required fields"
#~ msgstr "Toon telefoonnummer veld"

#~ msgid "You can attach the following file formats"
#~ msgstr "Bestanden van de volgende formaten kunnen worden bijgevoegd"

#~ msgid "Download"
#~ msgstr "Download"

#~ msgid "Install now from wordpress.org"
#~ msgstr "Installeer nu via wordpress.org"

#~ msgid "Contact Form Options"
#~ msgstr "Contact Form Opties"

#~ msgid "Display Attachment tips"
#~ msgstr "Toon Bijlage uitleg"

#~ msgid "Please enter a valid email address. Settings are not saved."
#~ msgstr "Geef een geldig email adres op. Instellingen zijn niet opgeslagen."

#~ msgid "E-Mail Address"
#~ msgstr "Email adres:"

#~ msgid "E-Mail Addresse:"
#~ msgstr "Indirizzo e-mail:"
