<?php

/**
 * Frontend dependency manager.
 */
class AgmDependencies {


	private static $_include = false;
	private static $_has_global = false;

	private function __construct () {}

	public static function serve () {
		self::_add_hooks();
	}

	private static function _add_hooks () {
		if (defined('AGM_LEGACY_DEPENDENCY_LOAD_MODE') && AGM_LEGACY_DEPENDENCY_LOAD_MODE) {
			add_action('wp_print_scripts', array("AgmDependencies", 'js_init_maps'));
			add_action('wp_print_scripts', array("AgmDependencies", 'js_data_object'));
			add_action('wp_print_styles', array("AgmDependencies", 'css_load_styles'));
			add_action('wp_print_scripts', array("AgmDependencies", 'js_google_maps_api'));
		} else {
			$footer_hook = self::get_footer_hook();
			add_action('wp_head', array("AgmDependencies", 'js_init_maps'));
			add_action($footer_hook, array("AgmDependencies", 'process_dependencies'), 1);
		}
	}

	public static function ensure_presence () {
		self::$_include = true;
	}

	public static function process_dependencies () {
		if (self::$_include) {
			AgmDependencies::js_data_object();
			AgmDependencies::css_load_styles();
			AgmDependencies::js_google_maps_api();
		}
	}

	public static function get_footer_hook () {
		$footer_hook = defined('AGM_FOOTER_HOOK') && AGM_FOOTER_HOOK
			? AGM_FOOTER_HOOK
			: 'wp_footer'
		;
		return apply_filters('agm-dependencies-footer_hook', $footer_hook);
	}

	/**
	 * Include Google Maps dependencies.
	 */
	public static function js_google_maps_api () {
		wp_enqueue_script('jquery');
		wp_enqueue_script('google_maps_api', AGM_PLUGIN_URL . '/js/google_maps_loader.js', array('jquery'));

		wp_enqueue_script('agm_google_user_maps', AGM_PLUGIN_URL . '/js/google_maps_user.js', array('jquery'));
		wp_localize_script('agm_google_user_maps', 'l10nStrings', array(
			'close' => __('Close', 'agm_google_maps'),
			'get_directions' => __('Get Directions', 'agm_google_maps'),
			'geocoding_error' => __('There was an error geocoding your location. Check the address and try again', 'agm_google_maps'),
			'missing_waypoint' => __('Please, enter values for both point A and point B', 'agm_google_maps'),
			'directions' => __('Directions', 'agm_google_maps'),
			'posts' => __('Posts', 'agm_google_maps'),
			'showAll' => __('Show All', 'agm_google_maps'),
			'hide' => __('Hide', 'agm_google_maps'),
			'oops_no_directions' => __('Oops, we couldn\'t calculate the directions', 'agm_google_maps'),
		));
		do_action('agm_google_maps-load_user_scripts');
	}

	/**
	 * Introduces plugins_url() as root variable (global).
	 */
	public static function js_data_object () {
		$defaults = array(
			'ajax_url' => admin_url('admin-ajax.php'),
			'root_url' => AGM_PLUGIN_URL,
			'is_multisite' => (int)is_multisite(),
			'libraries' => array('panoramio'),
		);
		$vars = apply_filters('agm_google_maps-javascript-data_object',
			apply_filters('agm_google_maps-javascript-data_object-user', $defaults)
		);
		echo '<script type="text/javascript">var _agm = ' . json_encode($vars) . ';</script>';
	}

	/**
	 * Introduces global list of maps to be initialized.
	 */
	public static function js_init_maps () {
		if (self::$_has_global) return true;
		echo '<script type="text/javascript">if (typeof(_agmMaps) == "undefined") _agmMaps = [];</script>';
		do_action('agm_google_maps-add_javascript_data');
		self::$_has_global = true;
	}

	/**
	 * Includes required styles.
	 */
	public static function css_load_styles () {
		wp_enqueue_style('agm_google_maps_user_style', AGM_PLUGIN_URL . '/css/google_maps_user.css');
	}

}