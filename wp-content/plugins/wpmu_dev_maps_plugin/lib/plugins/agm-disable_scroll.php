<?php
/*
Plugin Name: Disable scroll
Description: Allows you to disable scroll on a map. You can eiter disable the zoom in "Map Options", or with the shortcode attribute like so: <code>disable_scroll="true"</code>.<br />E.g. <code>[map id="1" disable_scroll="true"]</code>
Plugin URI: http://premium.wpmudev.org/project/wordpress-google-maps-plugin
Version: 1.0
Author: Ve Bailovity (Incsub)
*/

class Agm_DZ_AdminPages {

	private function __construct () {}

	public static function serve () {
		$me = new Agm_DZ_AdminPages;
		$me->_add_hooks();
	}

	private function _add_hooks () {
		// Basic KML overlay funcitonality
		add_action('agm_google_maps-load_admin_scripts', array($this, 'load_scripts'));
		add_action('agm_google_maps-prepare_for_save-map_options', array($this, 'prepare_for_save'), 10, 2);
		add_action('agm_google_maps-prepare_map-options', array($this, 'prepare_for_load'), 10, 2);
	}

	function load_scripts () {
		wp_enqueue_script('disable_scroll-admin', AGM_PLUGIN_URL . '/js/disable_scroll-admin.js', array('jquery'));
	}

	function prepare_for_save ($options, $raw) {
		if (isset($raw['disable_scroll'])) $options['disable_scroll'] = $raw['disable_scroll'];
		return $options;
	}

	function prepare_for_load ($options, $raw) {
		if (isset($raw['disable_scroll'])) $options['disable_scroll'] = $raw['disable_scroll'];
		return $options;
	}
}

class Agm_DZ_UserPages {

	private function __construct () {}

	public static function serve () {
		$me = new Agm_DZ_UserPages;
		$me->_add_hooks();
	}

	private function _add_hooks () {
		// Basic funcitonality
		add_action('agm_google_maps-load_user_scripts', array($this, 'load_scripts'));
		add_action('agm_google_maps-prepare_map-options', array($this, 'prepare_for_load'), 10, 2);

		// Disabling in shortcode attribute
		add_filter('agm_google_map-shortcode-attributes_defaults', array($this, 'attributes_defaults'));
		add_filter('agm_google_map-shortcode-overrides_process', array($this, 'overrides_process'), 10, 2);
	}

	function load_scripts () {
		wp_enqueue_script('disable_scroll-user', AGM_PLUGIN_URL . '/js/disable_scroll-user.js', array('jquery'));
	}

	function prepare_for_load ($options, $raw) {
		if (isset($raw['disable_scroll'])) $options['disable_scroll'] = $raw['disable_scroll'];
		return $options;
	}

	function attributes_defaults ($defaults) {
		$defaults['disable_scroll'] = false;
		return $defaults;
	}

	function overrides_process ($overrides, $atts) {
		if (@$atts['disable_scroll']) $overrides['disable_scroll'] = $atts['disable_scroll'];
		return $overrides;
	}

}


if (is_admin()) Agm_DZ_AdminPages::serve();
else Agm_DZ_UserPages::serve();