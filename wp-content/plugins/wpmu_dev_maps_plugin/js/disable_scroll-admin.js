(function ($) {
$(function () {

// Add options
$(document).bind("agm_google_maps-admin-markup_created", function (e, el, data) {
	var has_scroll = false;
	try { has_scroll = data.disable_scroll ? data.disable_scroll : false; } catch (e) { has_scroll = false; }
	el.find("#agm_mh_options").append(
		'<fieldset id="agm-disable_scroll">' +
			'<legend>Disable scroll</legend>' +
			'<input type="checkbox" id="agm-disable_scroll" value="' + scroll + '" />&nbsp;' +
			'<label for="agm-disable_scroll">Disable scroll</label>' +
		'</fieldset>'
	);
	$("#agm-disable_scroll").attr("checked", has_scroll);
});

// Save
$(document).bind("agm_google_maps-admin-save_request", function (e, request) {
	request.disable_scroll = $("#agm-disable_scroll").is(":checked");
});

});
})(jQuery);