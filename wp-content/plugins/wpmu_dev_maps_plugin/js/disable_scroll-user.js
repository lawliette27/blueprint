(function ($) {
$(function () {

$(document).bind("agm_google_maps-user-map_initialized", function (e, map, data) {
	var has_scroll = false;
	try { has_scroll = data.disable_scroll ? data.disable_scroll : false; } catch (e) { has_scroll = false; }
	has_scroll = !has_scroll;

	map.setOptions({scrollwheel: has_scroll});
});

});
})(jQuery);