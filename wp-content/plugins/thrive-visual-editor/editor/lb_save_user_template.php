<h2>Save User Template</h2>
<p>You can save your work as a template for use on another post/page on your site.</p>
<br/>
<input type="hidden" name="tve_lb_type" value="user_template">
<input class="tve_lightbox_input" name="template_name" id="template_name" placeholder="Enter template name" />
<br/><br/>