// jQuery selector of all elements that should be sortable and editable in the builder
editable_elements = "p,h1,h2,h3,h4,h5,h6,address,pre,img,th,td,li,blockquote,.thrv_wrapper, .tve_ts_o span, .tve_btnLink span";

// a list of elements that shouldn't be editable
block_editing_elements = ".thrv_custom_html_shortcode *, .tve_no_edit, .tve_more_tag";

// jquery selector of elements that shouldn't clear the focus when clicked and another element is in focus (for instance, lightbox option menus etc.)
block_clear_focus = ".edit_mode, .edit_mode *, .tve_secondLayer, .tve_secondLayer *, .tve_lightbox_content, .tve_lightbox_content *";

// jQuery selector of all text elements
text_elements = "p,h1,h2,h3,h4,h5,h6,span,li,blockquote,th,td,address,pre";

// jQuery selector of text elements that should only show inline styles (inline font size, alignment, bold, italic and underline)
text_elements_inline_only = ".tve_cb_cnt h3, .tve_fg h1, .tve_fg h3, .tve_ca h3, .tve_ca h1, .tve_tS, .tve_grt h3, .tve_ts span, .tve_btnLink";

//jQuery selector of all elements that should be handled but without text formatting controls sub-menu
text_element_no_formatting = ".tve_no_edit";

// jQuery selector of elements which lose selection and have to be escaped from caret placement function
selection_loss_tags = "font, b, i, u, span";

//jQuery selector of elements which don't have either duplicate or delete buttons on hover
elements_no_left_menu = ".tve_gri .image_placeholder";

//jQuery selector of items that should have the left menu displayed on hover.
left_menu_hover_items = ".thrv_wrapper, .tve_faq, p:not(.tve_more_tag), h1, h2, h3, h4, h5, h6";

// jQuery selector of items that should have the right menu displayed on hover
right_menu_hover_items = "p, h1, h2, h3, h4, h5, h6, address, pre, .thrv_wrapper, img, blockquote, .tve_clearfix li, .thrv_wrapper p, ol, ul, ol li, ul li, .tve_faq";

// jQuery selector of items that you don't want to show right menu for. This overrides right_menu_hover_items above.
// If user hovers over one of the elements in the selector below, it will look in the DOM to see if the element is part of a thrive shortcode
// If element is part of thrive shortcode then the shortcode wrapper will display the delete button, instead.
elements_without_delete_button = ".thrv_toggle_shortcode h4, .thrv_testimonial_shortcode img, .tve_colm, .thrv_custom_html_shortcode *";

// array of elements that contain child elements.  Unordered lists and Ordered lists, for example, contain LI
contains_nested_elements = new Array("UL", "OL");

// reminder, need to set REGEX in getBlockContainer() to correspond with block_tags
block_tags = new Array("ADDRESS", "BLOCKQUOTE", "P", "H1", "H2", "H3", "H4", "H5", "H6", "OL", "UL", "PRE");

// jquery selector for all elements that should be sortable
sortable_elements = ".thrv_wrapper, p:not(.edit_mode, .thrv_custom_html_shortcode p, .wp-caption p), img:not(.wp-caption img), .clear, ul:not(ul.tve_clearfix), ol, h1, h2, h3, h4 :not('.tve_faqB h4'), h5, h6, address, pre, hr";

// a list of elements that shouldn't be able to be sorted.
cancel_sortable_elements = ".thrv_custom_html_shortcode *, .tve_clearfix span"

//jQuery selector for elements that should contain a dropzone if empty (for example columns - so that users can drop elements into an empty column.)
dropzone_elements = ".tve_colm, .tve_cb div, .tve_faqC, td, .fwi, .tve_content_inner";

//jquery selector for elements that need to have a dropzone element add to the DOM because they're not sortable and elements should be allowed to be added before and after them.
prefix_with_dropzone = ".thrv_wrapper";

//jQuery selector for elements that should have elements added to the bottom if they are selected while the control panel buttons are pressed
add_element_on_click = ".thrv_wrapper:not(.thrv_columns)";

// define global variable to "remember" colours selections. Set default to red.
window.last_colour_used = "tve_red";

// remember custom colours
window.tve_custom_colours = new Array();
window.tve_custom_colours = tve_path_params.tve_colour_picker_colours;

// set number of colours to remember in colour picker
window.max_colour_history = 10;

// javascript array of classes that should be ignored by the custom class functionality
thrv_custom_classes = new Array("edit_light", "edit_dark", "edit_mode", "tve_image");

// create new undo manager variable
tve_undoManager = new UndoManager();

// IE6,7 support for indexOf
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (searchElement, fromIndex) {
        var i,
            pivot = (fromIndex) ? fromIndex : 0,
            length;

        if (!this) {
            throw new TypeError();
        }
        length = this.length;

        if (length === 0 || pivot >= length) {
            return -1;
        }
        if (pivot < 0) {
            pivot = length - Math.abs(pivot);
        }
        for (i = pivot; i < length; i++) {
            if (this[i] === searchElement) {
                return i;
            }
        }
        return -1;
    };
}

// IE6,7 add support for map
if (!Array.prototype.map) {
    Array.prototype.map = function (callback, thisArg) {
        var T, A, k;
        if (this == null) {
            throw new TypeError(" this is null or not defined");
        }
        var O = Object(this);
        var len = O.length >>> 0;
        if (typeof callback !== "function") {
            throw new TypeError(callback + " is not a function");
        }
        if (arguments.length > 1) {
            T = thisArg;
        }
        A = new Array(len);
        k = 0;
        while (k < len) {

            var kValue, mappedValue;
            if (k in O) {
                kValue = O[ k ];
                mappedValue = callback.call(T, kValue, k, O);
                A[ k ] = mappedValue;
            }
            k++;
        }
        return A;
    };
}

// IE6,7 add support for filter
if (!Array.prototype.filter) {
    Array.prototype.filter = function (fun /*, thisp */) {
        "use strict";
        if (this == null)
            throw new TypeError();

        var t = Object(this);
        var len = t.length >>> 0;
        if (typeof fun != "function")
            throw new TypeError();
        var res = [];
        var thisp = arguments[1];
        for (var i = 0; i < len; i++) {
            if (i in t) {
                var val = t[i]; // in case fun mutates this
                if (fun.call(thisp, val, i, t))
                    res.push(val);
            }
        }
        return res;
    };
}

jQuery(document).ready(function () {
    activate_editmode_keyboard_listener();
    add_element_menu_icons();
    tve_refresh(true);

    // if not edit mode, then make editable on click
    jQuery("#tve_editor").on("click", editable_elements, function (e) {
        tve_editor_init(e, jQuery(this));
    });

    // binding for uploading image through default Wordpress image upload library
    jQuery(document).on("mousedown", ".upload_image", function (e) {
        e.preventDefault();
        thrive_open_media(jQuery(this), "load", "image_element");
    });

    jQuery(document).on("mousedown", ".upload_image_cpanel", function (e) {
        e.preventDefault();
        hide_control_panel_menu();
        thrive_open_media(jQuery(".edit_mode"), "save", "image_element");
    });

    // binding for deleting elements on menu click
    jQuery("html").on("click", "#delete_icon", function (e) {
        element = jQuery(".active_delete");
        tve_set_window_content_before_action();
        remove_element(element, element.parent());
        tve_set_window_content_after_action();
        hide_menu_icons();
        refresh_dropzones();
        e.stopPropagation();
    });

    // binding to duplicate shortcodes
    jQuery("html").on("click", "#duplicate_icon", function (e) {
        element = jQuery(".active_duplicate");
        tve_set_window_content_before_action();
        duplicate_element(element);
        tve_set_window_content_after_action();
        hide_menu_icons();
        refresh_dropzones();
        e.stopPropagation();
    });

    // initialise sortable
    jQuery("#tve_editor").sortable({
        items: sortable_elements + ", .tve_dropzone",
        cancel: cancel_sortable_elements,
        helper: "clone",
        opacity: 0,
        placeholder: "tve_placeholder",
        dropOnEmpty: true,
        connectWith: "#tve_editor, .colm",
        tolerance: "pointer",
        scroll: true,
        start: function (event, ui) {
            if (jQuery(event.target).is(".tve_dropzone")) {
                return false;
            }
            // if sortable element in editor
            if (!jQuery(ui.item).is(".cp_draggable")) {
                jQuery(ui.item).show();
            }
            // if control panel icon dragged onto canvas
            else {
                jQuery(ui.item).addClass("tve-ui-helper");
            }
            jQuery(ui.helper).hide().addClass("tve-ui-helper");
            // get copy of DOM for undo manager
            tve_set_window_content_before_action();
            remove_elements_while_dragging();
            ui.placeholder.addClass(ui.item.context.className);
            event.stopPropagation();
        },
        change: function (event, ui) {
            if (jQuery(ui.item).find(ui.placeholder).length != 0) {
                // placeholder is within element being dragged
            }
        },
        stop: function (event, ui) {
            // TODO: try and stop users from dragging items into themselves / identify when this happens and handle it so that
            // doesn't break user experience.
            var $item = jQuery(ui.item);
            if ($item.is(".cp_draggable")) {
                $item.addClass(".tve-ui-helper");
                $item.hide();
                jQuery(ui.helper).hide();
            }
            convert_into_element(event, ui, true);
            add_elements_after_dragging();
            refresh_dropzones();
            tve_set_window_content_after_action();
        }
    });

    // handle paste
    // have to do minor hackery in order to capture paste data before inserting into DOM
    jQuery("#tve_editor").on("paste", ".edit_mode", function (e) {
        var savedSel = rangy.saveSelection();
        active_editor = jQuery(".edit_mode");
        var textarea = jQuery("<div contenteditable></div>");
        textarea.css("position", "absolute").css("left", "-1000px").css("top", active_editor.offset().top + "px").attr("id", "pasteHelper").appendTo("body");
        textarea.focus();
        setTimeout(function () {
            jQuery(".edit_mode").focus();
            var sel = rangy.getSelection();
            rangy.restoreSelection(savedSel);
            sel.deleteFromDocument();
            var range = sel.getRangeAt(0);
            var textarea_filtered = tve_filter_paste(textarea);
            var el = document.createElement("div");
            el.innerHTML = textarea_filtered;
            var frag = document.createDocumentFragment(), node, lastNode;
            while ((node = el.firstChild)) {
                lastNode = frag.appendChild(node);
            }
            range.insertNode(frag);
            range.collapseAfter(lastNode);
            rangy.getSelection().setSingleRange(range);
            textarea.remove();
        }, 0.1);
    });

    /** old controls.js ready function **/

        // load control panel and send data necessary for build through ajax
    data = {
        style_families: tve_path_params.style_families,
        user_templates: tve_path_params.user_templates,
        editor_dir: tve_path_params.editor_dir,
        preview_url: tve_path_params.preview_url,
        tve_api_data: tve_path_params.tve_api_data,
        is_thrive_theme: tve_path_params.is_thrive_theme,
        page_section_colours: tve_path_params.page_section_colours,
        display_options: tve_path_params.cpanel_display_options,
    };

    jQuery.post(tve_path_params.cpanel_dir + '/control_panel.php', data, function (response) {
        jQuery("body").prepend(response);

        // there is no standard WP hook for inserting before body tag, so have to use jQuery to manipulate DOM
        jQuery(".tve_cpanel_onpage").prependTo("html");
        jQuery("#tve_cpanel").prependTo("html");

        // initialise jQuery draggable
        tve_start_draggable();

        // set up rangy controls
        rangy.init();
        bold_text = rangy.createCssClassApplier("bold_text", {normalize: true});
        italic_text = rangy.createCssClassApplier("italic_text", {normalize: true});
        underline_text = rangy.createCssClassApplier("underline_text", {normalize: true});

        //image alignment controls
        jQuery("#img_left_align").mousedown(function (e) {
            e.stopPropagation();
            element = jQuery(".edit_mode");
            tve_has_caption = check_image_caption(element);
            if (tve_has_caption) {
                caption = jQuery(".edit_mode").parents(".wp-caption").first();
                jQuery(caption).removeClass("alignleft alignright aligncenter").addClass("alignleft");
            }
            jQuery(".edit_mode").removeClass("alignleft alignright aligncenter").addClass("alignleft");
        });
        jQuery("#img_right_align").mousedown(function (e) {
            e.stopPropagation();
            element = jQuery(".edit_mode");
            tve_has_caption = check_image_caption(element);
            if (tve_has_caption) {
                caption = jQuery(".edit_mode").parents(".wp-caption").first();
                jQuery(caption).removeClass("alignleft alignright aligncenter").addClass("alignright");
            }
            jQuery(".edit_mode").removeClass("alignleft alignright aligncenter").addClass("alignright");
        });
        jQuery("#img_center_align").mousedown(function (e) {
            e.stopPropagation();
            element = jQuery(".edit_mode");
            tve_has_caption = check_image_caption(element);
            if (tve_has_caption) {
                caption = jQuery(".edit_mode").parents(".wp-caption").first();
                jQuery(caption).removeClass("alignleft alignright aligncenter").addClass("aligncenter");
            }
            jQuery(".edit_mode").removeClass("alignleft alignright aligncenter").addClass("aligncenter");
        });
        jQuery("#img_no_align").mousedown(function (e) {
            e.stopPropagation();
            element = jQuery(".edit_mode");
            tve_has_caption = check_image_caption(element);
            if (tve_has_caption) {
                caption = jQuery(".edit_mode").parents(".wp-caption").first();
                jQuery(caption).removeClass("alignleft alignright aligncenter");
            }
            jQuery(".edit_mode").removeClass("alignleft alignright aligncenter");
        });

        jQuery("#img_alt_att").change(function (e) {
            e.stopPropagation();
            element = jQuery(".edit_mode");
            jQuery(element).attr("alt", jQuery(this).val());
        });

        jQuery("#img_title_att").change(function (e) {
            e.stopPropagation();
            element = jQuery(".edit_mode");
            jQuery(element).attr("title", jQuery(this).val());
        });

        jQuery("#text_bullet").mousedown(function (e) {
            e.stopPropagation();
            tve_wrap_element_tags(jQuery(".edit_mode"), "<ul><li>", "</li></ul>");
        });
        jQuery("#text_numbered_bullet").mousedown(function (e) {
            e.stopPropagation();
            tve_wrap_element_tags(jQuery(".edit_mode"), "<ol><li class='edit_mode'>", "</ol></ul>");
            convert_element_into_edit_mode(jQuery(".edit_mode"), e);
        });

        // certain text effects are done using inline rangy classes
        jQuery(".tve_rangy_class").mousedown(function (e) {
            e.preventDefault();
            command = jQuery(this).data("command") + "_text";

            //TODO get rid of eval - eval is evil.
            eval(command).toggleSelection();
        });

        jQuery(".text_left_align").mousedown(function (e) {
            jQuery(".edit_mode").removeClass("tve_p_left tve_p_right tve_p_center tvealignjustify").addClass("tve_p_left");
        });
        jQuery(".text_center_align").mousedown(function (e) {
            jQuery(".edit_mode").removeClass("tve_p_left tve_p_right tve_p_center tvealignjustify").addClass("tve_p_center");
        });
        jQuery(".text_right_align").mousedown(function (e) {
            jQuery(".edit_mode").removeClass("tve_p_left tve_p_right tve_p_center tvealignjustify").addClass("tve_p_right");
        });
        jQuery(".text_justify_align").click(function (e) {
            e.stopPropagation();
            jQuery(".edit_mode").removeClass("alignleft alignright aligncenter tvealignjustify").addClass("tvealignjustify");
        });

        // handle block level tag changes for text
        jQuery(".tve_block_change").mousedown(function (e) {
            element = jQuery(".edit_mode");
            replace_with_tag = jQuery(this).data("tag");
            element.replaceWith("<" + replace_with_tag + " class='edit_mode'>" + element.html() + "</" + replace_with_tag + ">");
            e.target = jQuery(".edit_mode");
            convert_element_into_edit_mode(jQuery(".edit_mode"), e);
        });

        jQuery("#more_link").click(function (e) {
            element = jQuery(".edit_mode");
            jQuery(".tve_more_tag").remove();
            more_tag = '<p class="tve_more_tag"><span class="tve_no_edit">More...</span></p>';
            jQuery(element).after(more_tag);
        });

        jQuery("#element_id").change(function (e) {
            var element = jQuery(".edit_mode");
            jQuery(element).attr("id", jQuery(this).val());
        });

        // hyperlink controls
        jQuery("#lb_text_link").mousedown(function (e) {
            window.linkSel = rangy.saveSelection();
        });

        jQuery("#text_unlink").mousedown(function (e) {
            document.execCommand("unlink", true, true);
        });

        jQuery("#image_unlink").mousedown(function (e) {
            element = jQuery(".edit_mode");
            jQuery(element).parents("a").replaceWith(element);
        });

        // button controls sub menu
        // button text
        jQuery("#buttonText").keyup(function () {
            jQuery(".edit_mode a").contents().last()[0].textContent = this.value;
        });

        jQuery("#buttonLink").change(function () {
            jQuery(".edit_mode a").attr("href", this.value);
        });

        // save the post when save button clicked
        jQuery("#tve_save_post").mousedown(function () {
            tve_save_post(true);
        });

        // handle drop down menu
        jQuery(".tve_option_separator").on("mousedown", function (e) {
            if (jQuery(e.target).is(".wp-color-picker")) {
                return true;
            }
            e.preventDefault();
            jQuery(document).off("click.remove_submenu");
            jQuery(".active_sub_menu").not(jQuery(this).find(".active_sub_menu")).hide();
            if (!jQuery(e.target).parents(".active_sub_menu").length != 0) {
                menu = jQuery(this).find(".tve_sub");
                menu.toggle().addClass("active_sub_menu");
                tve_check_submenu_position(menu);
                if (jQuery('#tve_cpanel').hasClass('tve_editor_collapse')) {
                    jQuery('#tve_cpanel').addClass('expand_width');
                }
            }

            // remove drop menu if active
            jQuery(document).on("click.remove_submenu", document, function (e) {
                if (!jQuery(e.target).is(".wp-picker-default, .wp-color-picker") && !jQuery(e.target).is(".tve_option_separator") && !jQuery(e.target).is(".tve_expanded") && !jQuery(e.target).is(".tve_cp") && !jQuery(e.target).is(".tve_collapsed") && !jQuery(e.target).parent().is(".tve_option_separator") && !jQuery(e.target).parents().is('.tve_drop_style') && !jQuery(e.target).is(".wp-picker-holder *") && !jQuery(e.target).parents().is('.tve_drop_flip') && jQuery(".active_sub_menu").length != 0) {
                    hide_sub_menu();
                    jQuery('#tve_cpanel').removeClass('expand_width');
                }
            });
            if (!is_tve_cpanel_submenu_visible()) {
                jQuery('#tve_cpanel').removeClass('expand_width');
            }
        });

        /** set up dynamic jquery selector for all colours that reads from standing data list of colours **/
        tve_shortcode_colours = tve_path_params.shortcode_colours;
        colours_length = tve_shortcode_colours.length;
        colours_class_selector = new Array();

        for (var i = 0; i < colours_length; i++) {
            jQuery(".tve_cpanel_onpage .tve_" + tve_shortcode_colours[i] + ":not(.tve_bullets_menu)").click(function (e) {
                var $this = jQuery(this);
                tve_change_shortcode_colour.call($this, $this.attr("class"), jQuery(".edit_mode"));
                e.preventDefault();
            });
            // transform array into jquery class selector
            colours_class_selector[i] = "tve_" + tve_shortcode_colours[i];
        }

        colours_class_selector = colours_class_selector.join(" ");

        jQuery(".tve_btn_style").click(function () {
            var style = jQuery(this).attr("id");
            tve_change_shortcode_style(style, jQuery(".edit_mode"));
        });

        jQuery(".tve_btn_size").click(function () {
            var size = jQuery(this).attr("id");
            tve_change_shortcode_size(size, jQuery(".edit_mode"));
        });

        jQuery(".tve_open_btn_link").click(function () {
            document.location.href = jQuery(".edit_mode a").attr("href");
        });

        jQuery(".btn_alignment").click(function () {
            alignment = jQuery(this).attr("id");
            tve_change_shortcode_alignment(alignment, jQuery(".edit_mode"));
        });

        jQuery("#btn_link_new_window").change(function () {
            button_link_anchor = jQuery(".edit_mode").find("a.tve_btnLink");
            if (tve_is_checked(jQuery(this))) {
                jQuery(button_link_anchor).attr("target", "_blank");
            } else {
                jQuery(button_link_anchor).attr("target", "_self");
            }
        });

        jQuery("#btn_nofollow").change(function () {
            button_link_anchor = jQuery(".edit_mode").find("a.tve_btnLink");
            if (tve_is_checked(jQuery(this))) {
                jQuery(button_link_anchor).attr("rel", "nofollow");
            } else {
                jQuery(button_link_anchor).attr("rel", "");
            }
        });

        jQuery("#link_no_follow").change(function () {
            button_link_anchor = jQuery(".tve_active_hyperlink");
            if (tve_is_checked(jQuery(this))) {
                jQuery(button_link_anchor).attr("rel", "nofollow");
            } else {
                jQuery(button_link_anchor).removeAttr("rel");
            }
        });

        jQuery('.tve_tab-btn').click(function () {
            jQuery(this).toggleClass('tve_left');
            if (!jQuery(this).hasClass('tve_left')) {
                jQuery('.tve_tab-text').html('Same tab');
            } else {
                jQuery('.tve_tab-text').html('New tab');
            }
        });

        /** bullet style **/
        jQuery(".tve_bullet_style").click(function () {
            bullet_style = jQuery(this).attr("id");
            tve_change_bullet_style(bullet_style, jQuery(".edit_mode"));
        });

        /** handle the changing of style families drop down **/
        jQuery(".tve_btn_style_family").click(function () {
            hide_sub_menu();
            tve_change_style_family(jQuery(this).text());
        });

        /** kitchen sink control **/
        jQuery('.kitchen_sink').on('click', function () {
            jQuery(this).toggleClass('kitchen_sink_close');
            if (jQuery(this).hasClass('kitchen_sink_close')) {
                jQuery('.tve_kitchenContainer').slideDown(100);
            } else {
                jQuery('.tve_kitchenContainer').slideUp(100);
            }
        });

        /** lightbox controls **/
        jQuery(document).on("mousedown", ".tve_lightbox_close", function (e) {
            e.stopPropagation();
            e.preventDefault();
            hide_lightbox();
        });

        /** open lightbox **/
        jQuery(document).on("mousedown", ".tve_lb_open", function (e) {
            e.preventDefault();
            e.stopPropagation();

            // if lightbox activated from control panel menu, then set DOM element to be target
            if (jQuery(this).is(".tve_btn")) {
                tve_add_lightbox_marker(jQuery(".edit_mode"));
            } else {
                // otherwise the target can be the target of the event.
                tve_add_lightbox_marker(jQuery(this));
            }
            tve_open_lightbox(jQuery(this).attr("id"), jQuery(this).hasClass("tve_lb_small"));
        });

        jQuery(document).on("mousedown", ".tve_save_lightbox", function (e) {
            e.preventDefault();
            e.stopPropagation();
            user_options = tve_get_lightbox_options();
            tve_handle_lightbox_options(user_options);
            hide_lightbox();
        });

        jQuery(window).resize(function () {
            //tve_calculate_panel_position();
        });

        // call to action settings
        jQuery("#call_to_action_url").change(function () {
            element = jQuery(".edit_mode");
            url = jQuery(this).val();
            jQuery(element).find(".tve_btnLink").attr("href", url);
            jQuery(".cta_test_link").attr("href", url);
        });

        jQuery("#cta_link_new_window").change(function (e) {
            new_window = jQuery(this).is(":checked");
            if (new_window) {
                jQuery(".edit_mode").find(".tve_btnLink").attr("target", "_blank");
            } else {
                jQuery(".edit_mode").find(".tve_btnLink").attr("target", "_self");
            }
        });

        jQuery("#cta_nofollow").change(function (e) {
            no_follow = jQuery(this).is(":checked");
            if (no_follow) {
                jQuery(".edit_mode").find(".tve_btnLink").attr("rel", "nofollow");
            } else {
                jQuery(".edit_mode").find(".tve_btnLink").attr("rel", "");
            }
        });

        jQuery(".tve_cpanel_onpage").on("mousedown", "#testimonial_image_btn", function (e) {
            e.preventDefault();
            hide_control_panel_menu();
            thrive_open_media(jQuery(this), "load", "testimonial_button", jQuery(".edit_mode"));
        });

        //hyperlink controls
        jQuery("#link_anchor").keyup(function (e) {
            jQuery(".tve_active_hyperlink").text(jQuery(this).val());
        });

        jQuery("#link_url").keyup(function (e) {
            jQuery(".tve_active_hyperlink").attr("href", jQuery(this).val());
        });

        jQuery("#text_unlink").click(function () {
            jQuery(".tve_active_hyperlink").replaceWith(jQuery(".tve_active_hyperlink").html());
            tve_hide_hyperlink_options();
        });

        jQuery("#link_new_window").change(function () {
            if (jQuery("#link_new_window").prop("checked")) {
                jQuery(".tve_active_hyperlink").attr("target", "_blank");
            } else {
                jQuery(".tve_active_hyperlink").attr("target", "_self");
            }
        });

        jQuery("#tve_update_content").click(function () {
            tve_save_post(true);
        });

        jQuery("#tve_cpanel").on("click", ".remove_user_template", function (e) {
            e.preventDefault();
            e.stopPropagation();
            template_name = jQuery.trim(jQuery(this).parent().find(".tve_left").text());
            tve_remove_user_template(template_name, jQuery(this));
        });

        // initialise sliders
        tve_init_sliders();

        //add elements to page on click
        jQuery("#tve_cpanel").on("click", ".cp_draggable", function (e) {
            e.preventDefault();
            e.stopPropagation();
            ui = new Object();
            ui.item = jQuery(this);
            convert_into_element(e, ui, false);
        });

        jQuery(".element_class").change(function () {
            jQuery(".edit_mode").addClass(jQuery(this).val());
        });

        // feature grid controls
        jQuery(".feature_grid_small").click(function () {
            jQuery(".edit_mode").removeClass("tve_gr1 tve_gr2 tve_gr3").addClass("tve_gr1");
        });
        jQuery(".feature_grid_medium").click(function () {
            jQuery(".edit_mode").removeClass("tve_gr1 tve_gr2 tve_gr3").addClass("tve_gr2");
        });
        jQuery(".feature_grid_large").click(function () {
            jQuery(".edit_mode").removeClass("tve_gr1 tve_gr2 tve_gr3").addClass("tve_gr3");
        });

        // credit card icons
        jQuery(".cc_checkbox").change(function () {
            this_cc = jQuery(this).attr("id");
            element = jQuery(".edit_mode");
            if (jQuery(this).prop("checked")) {
                jQuery(element).find(".tve_" + this_cc).css("display", "block");
            } else {
                jQuery(element).find(".tve_" + this_cc).css("display", "none");
            }
            tve_resize_cc_wrapper(element);
        });

        // credit card alignment
        jQuery("#cc_left_align").click(function () {
            element = jQuery(".edit_mode");
            jQuery(element).find(".thrv_cc_wrapper").css("float", "left");
        });
        jQuery("#cc_right_align").click(function () {
            element = jQuery(".edit_mode");
            jQuery(element).find(".thrv_cc_wrapper").css("float", "right");
        });
        jQuery("#cc_center_align").click(function () {
            element = jQuery(".edit_mode");
            jQuery(element).find(".thrv_cc_wrapper").css("float", "");
        });

        // credit card icons for buttons
        jQuery("#button_cc_icons").change(function () {
            element = jQuery(".edit_mode");
            if (jQuery(this).prop("checked")) {
                cc_icons_in_button(element, "load");
            } else {
                cc_icons_in_button(element, "remove");
            }
        });

        // page sections change colour
        jQuery(".tve_section_color_change").click(function () {
            element = jQuery(".edit_mode");
            tve_remove_custom_colours(element);
            color = jQuery(this).find("input").attr("id");
            change_page_section_color(jQuery(element).find(".out").css('background-color', ''), color);
        });

        // font sizes
        jQuery(".tve_font_size").click(function (e) {
            font_size = jQuery(this).data("size");
            element = jQuery(".edit_mode");
            jQuery(element).css("font-size", font_size + "px");
            hide_sub_menu();
        });
        // clear font size
        jQuery(".tve_clear_font_size").click(function () {
            element = jQuery(".edit_mode");
            jQuery(element).css("font-size", "");
            hide_sub_menu();
        });
        jQuery("#tve_undo_manager").click(function () {
            tve_undoManager.undo();
        });
        jQuery("#tve_redo_manager").click(function () {
            tve_undoManager.redo();
        });

        // undo on ctrl+z keyboard press
        jQuery(document).on("keydown", function (e) {
            if (e.which == 90 && e.ctrlKey) {
                tve_undoManager.undo();
            }
        });

        // undo on ctrl+y keyboard press
        jQuery(document).on("keydown", function (e) {
            if (e.which == 89 && e.ctrlKey) {
                tve_undoManager.redo();
            }
        });

        // save on ctrl+s keyboard press
        jQuery(document).on("keydown", function (e) {
            if (e.which == 83 && e.ctrlKey) {
                e.preventDefault();
                tve_save_post(true);
            }
        });

        jQuery("#tve_collapse_editor_btn").click(function () {
            jQuery('#tve_cpanel').toggleClass('tve_editor_collapse');
            jQuery('.tve_wrap_inner').toggleClass('tve_is_collapsed');
            hide_sub_menu();
        });

        jQuery('#tve_flipEditor').click(function () {
            if (jQuery("#tve_cpanel").hasClass("tve_cpanelFlip")) {
                tve_menu_position("right");
            } else {
                tve_menu_position("left");
            }
        });

        jQuery('#tve_flipColor').click(function () {
            if (jQuery("#tve_cpanel").hasClass("tve_is_dark")) {
                tve_menu_color("light");
            } else {
                tve_menu_color("dark");
            }
        });

        jQuery("#tve_highlight_left").click(function () {
            element = jQuery(".edit_mode");
            tve_modify_pricing_highlight(element, "left");
        });

        jQuery("#tve_highlight_right").click(function () {
            element = jQuery(".edit_mode");
            tve_modify_pricing_highlight(element, "right");
        });

        jQuery("#tve_prt_remove_highlight").click(function () {
            element = jQuery(".edit_mode");
            jQuery(element).find(".tve_hgh").removeClass("tve_hgh");
            tve_check_highlight_exists();
        });

        jQuery("#tve_prt_add_highlight").click(function () {
            element = jQuery(".edit_mode");
            jQuery(element).find(".tve_prt_col").eq(0).addClass("tve_hgh");
            tve_check_highlight_exists();
        });

        jQuery(document).on("mousedown", "input.button.button-small.wp-picker-default", function (e) {
            color = jQuery(this).prev().val();
            tve_remember_custom_colour(color);
            jQuery(".wp-color-picker").iris('option', 'palettes', window.tve_custom_colours);
        });

        /** add new toggle to toggle group **/
        jQuery("#add_toggle").click(function () {
            element = jQuery(".edit_mode");
            toggle_code = jQuery(element).find(".tve_faq").first().html();
            jQuery(element).append("<div class='tve_faq active_toggle'></div>");
            jQuery(".active_toggle").html(toggle_code);
            jQuery(".active_toggle").removeClass("active_toggle");
        })

        /** add new tab to tab group **/
        jQuery("#add_tab").click(function (e) {
            element = jQuery(".edit_mode");
            num_tabs = jQuery(element).find(".tve_clearfix li").length;
            tab_header_custom_colour = jQuery(element).find(".tve_clearfix li:not(.tve_tS)").first().attr("data-tve-custom-colour");
            new_tab_index = num_tabs + 1;
            new_tab_header_html = '<li data-tve-custom-colour="' + tab_header_custom_colour + '"><span class="tve_scTC' + new_tab_index + '">New Tab</span></li>';
            jQuery(element).find(".tve_clearfix").append(new_tab_header_html);
            tve_resize_tabs();
            tab_content_custom_colour = jQuery(element).find(".tve_scT div").first().attr("data-tve-custom-colour");
            new_tab_content_html = '<div class="tve_scTC tve_scTC' + new_tab_index + '" style="display: none;" data-tve-custom-colour="' + tab_content_custom_colour + '"><p>Tab ' + new_tab_index + '</p></div>';
            jQuery(element).find(".tve_scT").append(new_tab_content_html);
        });

        tve_text_colour_pickers();

        /** clear highlight (background color) and color effects from selection / text elements **/
        jQuery('.tve_remove_color_formatting > span').click(function (event) {
            tve_remove_color(jQuery(this).parents('.tve_highlight_control').length ? 'background' : 'foreground');
        });
    });

});

/**
 * function to clear edit mode when user clicks off an element that's being edited
 * @param e
 * @returns {boolean}
 */
var clearFocus = function (e) {
    // clear the focus if the click doesn't occur on the currently editable element, anything contained within said element, or any of the secondary level menu items
    if (!jQuery(e.target).is(block_clear_focus)) {

        /** if user is making a text selection and ends up with cursor outside of contenteditable box, then don't refresh **/
        var sel = rangy.getSelection();
        if (sel.toString().length) {
            return false;
        }

        jQuery(".edit_mode").off(".editHyperlinks");
        tve_hide_hyperlink_options();
        e.stopPropagation();
        tve_refresh(true);
        jQuery(document).off(".clearFocus");
    }
}

function tve_edit_hyperlinks(e) {
    jQuery(e.target).addClass("tve_active_hyperlink");
    jQuery(".tve_link_btns").show();
    tve_populate_hyperlink_fields();
}

function tve_populate_hyperlink_fields() {
    active_link = jQuery(".tve_active_hyperlink");
    jQuery("#link_anchor").val(jQuery(active_link).text());
    jQuery("#link_url").val(jQuery(active_link).attr("href"));
    jQuery("#anchor_name").val(jQuery(active_link).attr("name"));

    if (jQuery(active_link).attr("target") == "_blank") {
        jQuery("#link_new_window").prop("checked", true);
    } else {
        jQuery("#link_new_window").prop("checked", false);
    }

    if (jQuery(active_link).attr("rel") == "nofollow") {
        jQuery("#link_no_follow").prop("checked", true);
    } else {
        jQuery("#link_no_follow").prop("checked", false);
    }

}

function tve_hide_hyperlink_options() {
    jQuery(".tve_active_hyperlink").removeClass("tve_active_hyperlink");
    jQuery(".tve_link_btns").hide();
}


/**
 * when editing a box, the sortables should be destroyed to enable text selection.  When user escapes the editing process, the sortables should be enabled again.
 * all events that are disabled when the user is editing text should sit in this function here.
 */
function enable_sortables() {

    if (jQuery("#tve_editor").data("ui-sortable")) {
        jQuery("#tve_editor").sortable("enable");
    }

    // mouse events for popup menu on hover
    jQuery("#tve_editor").on("mouseover", right_menu_hover_items, function (e) {

        // don't show menu items if in edit mode.
        if (check_element_edit_mode(e.target)) {
            return false;
        }

        shortcode_wrapper = tve_get_shortcode_wrapper_hover(e.target);

        // catch elements that you don't want to display the menu for
        if (jQuery(e.target).is(elements_without_delete_button)) {
            // manually bubble up to determine if element is part of a shortcode.  If so, make that the active hover item.
            if (shortcode_wrapper) {
                element_left_position = jQuery(shortcode_wrapper).offset().left;
                element_top_position = jQuery(shortcode_wrapper).offset().top;
                element_width = jQuery(shortcode_wrapper).outerWidth();
                display_element_right_menu(shortcode_wrapper, element_left_position, element_top_position, element_width);
                if (jQuery(shortcode_wrapper).is(left_menu_hover_items)) {
                    if (!jQuery(shortcode_wrapper).is(elements_no_left_menu)) {
                        display_element_left_menu(shortcode_wrapper, element_left_position, element_top_position);
                    }
                }
                return false;
            }
        }


        // calc co-ords
        element_left_position = jQuery(this).offset().left;
        element_top_position = jQuery(this).offset().top;
        element_width = jQuery(this).outerWidth();

        //display left and right menus
        display_element_right_menu(this, element_left_position, element_top_position, element_width);

        if (jQuery(this).is(left_menu_hover_items)) {
            if (!jQuery(shortcode_wrapper).is(elements_no_left_menu)) {
                display_element_left_menu(this, element_left_position, element_top_position);
            }
        }
        e.stopPropagation();
    });

    // remove menu on mouseleave
    jQuery("#tve_editor").on("mouseleave", right_menu_hover_items, function (e) {
        var evt = e || window.event;

        // make sure mouseleave is not triggered by hovering over delete icon
        if (evt.relatedTarget != document.getElementById('delete_icon') && evt.relatedTarget != document.getElementById('duplicate_icon')) {
            hide_element_right_menu();
            hide_element_left_menu();
            e.stopPropagation();
        }
    });

    jQuery(document).on("mouseleave", "#delete_icon", function (e) {
        jQuery(".active_delete").removeClass("active_delete");
        jQuery(this).hide();
        e.stopPropagation();
    });
}

//function to remove containers of nested items if they are empty
function remove_element(element, element_parent) {

    // custom delete handling for tab in tabs shortcode
    if (jQuery(element).is(".thrv_tabs_shortcode li")) {
        remove_shortcode_tab(element);
        return true;
    }

    // if a nested element and no more elements after this deletion, then delete parent element, too.
    if (contains_nested_elements.indexOf(element_parent.get(0).tagName) != -1) {
        if (jQuery(element_parent).find("li").length == 1) {
            if (jQuery(element_parent).parent().is(".thrv_bullets_shortcode")) {
                jQuery(element_parent).parent().remove();
            } else {
                jQuery(element_parent).remove();
            }
        }
    }

    // if image caption, then delete caption wrapper
    if (jQuery(element).is("img") && jQuery(element).parents(".wp-caption").first().length != 0) {
        jQuery(element).parents(".wp-caption").first().remove();
    }

    element.remove();
}

function refresh_sortables() {
    if (jQuery("#tve_editor").data("ui-sortable")) {
        jQuery("#tve_editor").sortable("refresh");
    }
}

// disable any bindings that shouldn't take place when the user is editing text in the contenteditable
function destroy_sortables() {
    if (jQuery("#tve_editor").data("ui-sortable")) {
        jQuery("#tve_editor").sortable("disable");
    }
    jQuery(".hovering").removeClass("hovering");
    jQuery("#tve_editor").off("mouseenter", "p");
}


// function to detect the icon that was dropped and convert into the proper HTML dom code.
// Loads from templating system.
// drag: either true or false, depending on whether the element was dragged or clicked
function convert_into_element(event, ui, drag) {
    var $item = jQuery(ui.item);

    // user templates come from the database, not external files, so must be handled seperately
    if ($item.hasClass("user_template_item")) {
        tve_load_user_template(jQuery.trim($item.find(".tve_left").text()), event.target, drag);
        return true;
    }

    // If added through the API, handle separately
    if ($item.hasClass("tve_api_icon")) {
        var item_id = get_api_item_id(ui.item);

        if (tve_path_params.tve_api_paths[item_id] != null) {
            api_template_path = tve_path_params.tve_api_paths[item_id];

            var data = {
                colour: window.last_colour_used,
                cssdir: tve_path_params.editor_dir,
                custom_data: tve_path_params.tve_api_includes
            }

            jQuery.post(api_template_path, data,function (response) {
                css_class = "tve_api_id_" + item_id;
                response = tve_thrv_wrap(response);
                insert_element_into_page(event, ui, drag, response);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status == 404) {
                        tve_add_notification("ERROR: File: " + api_template_path + " could not be found", true);
                    }
                });
        } else {
            tve_add_notification("ERROR: Element path not specified through API", true);
            return false;
        }
    }

    //loop through template files to find matching shortcode
    var tpl_loop = tve_path_params.shortcodes_array.length;

    var inserted = false;
    for (var i = 0; i < tpl_loop; i++) {
        css_class = tve_path_params.shortcodes_array[i].slice(0, -4);
        if ($item.hasClass(css_class)) {
            inserted = true;
            var data = {
                colour: window.last_colour_used,
                cssdir: tve_path_params.editor_dir
            }

            // if page section then need to also send colour details to template
            if ($item.hasClass("sc_page_section")) {
                page_section_colour = $item.find("input").attr("id");
                data['page_section_colour'] = page_section_colour;
                data['page_section_data'] = tve_path_params.page_section_colours[page_section_colour];
            }

            jQuery.post(tve_path_params.shortcodes_dir + tve_path_params.shortcodes_array[i], data, function (response) {
                insert_element_into_page(event, ui, drag, response);
            });
        }
        if (inserted) {
            break;
        }
    }
}

/**
 * Refresh the screen flow
 * called when user exits edit mode.
 * when build_sortables set to true it will allow the user to sort the elements on screen.
 **/
function tve_refresh(build_sortables) {

    jQuery("p:empty").html("&#x200b");
    jQuery('[contentEditable="true"]').attr("contentEditable", "false");
    jQuery(".edit_mode").removeClass("edit_mode");
    jQuery(".edit_light, .edit_dark").removeClass("edit_light").removeClass("edit_dark");

    if (build_sortables) {
        enable_sortables();
        // hide customised menu because no element is selected at this point.
        hide_control_panel_menu();
    }
    add_elements_after_dragging();
    refresh_dropzones();
}

/**
 * function used to determine the cursor cursor position when user clicks on contenteditable div.
 * get mouse position for setting caret position
 **/
function getMouseEventCaretRange(evt) {
    var range, x = evt.clientX, y = evt.clientY;

    // Try the simple IE way first
    if (document.body.createTextRange) {
        range = document.body.createTextRange();
        range.moveToPoint(x, y);
    }

    else if (typeof document.createRange != "undefined") {
        // Try Mozilla's rangeOffset and rangeParent properties,
        // which are exactly what we want
        if (typeof evt.rangeParent != "undefined") {
            range = document.createRange();
            range.setStart(evt.rangeParent, evt.rangeOffset);
            range.collapse(true);
        }

        // Try the standards-based way next
        else if (document.caretPositionFromPoint) {
            var pos = document.caretPositionFromPoint(x, y);
            range = document.createRange();
            range.setStart(pos.offsetNode, pos.offset);
            range.collapse(true);
        }

        // Next, the WebKit way
        else if (document.caretRangeFromPoint) {
            range = document.caretRangeFromPoint(x, y);
        }
    }
    return range;
}

//set carret position using
function selectRange(range) {
    if (range) {
        if (typeof range.select != "undefined") {
            range.select();
        } else if (typeof window.getSelection != "undefined") {
            var sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
        }
    }
}


// extend jquery to determine darkness of foreground.  Returns true for light, false for dark. Used to determine background colour on highlight.
(function ($) {
    $.fn.lightOrDark = function () {
        var r, b, g, hsp
            , a = this.css('color');
        if (a.match(/^rgb/)) {
            a = a.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/);
            r = a[1];
            g = a[2];
            b = a[3];
        } else {
            a = +("0x" + a.slice(1).replace( // thanks to jed : http://gist.github.com/983661
                a.length < 5 && /./g, '$&$&'
            )
                );
            r = a >> 16;
            g = a >> 8 & 255;
            b = a & 255;
        }
        hsp = Math.sqrt( // HSP equation from http://alienryderflex.com/hsp.html
            0.299 * (r * r) +
                0.587 * (g * g) +
                0.114 * (b * b)
        );
        if (hsp > 127.5) {
            jQuery(this).addClass("edit_dark");
        } else {
            jQuery(this).addClass("edit_light");
        }
    }
})(jQuery);

// button: DOM object of upload media button that has been clicked (there are often more than one on the page so need to know which one was clicked).
// action: "save" = replaces the url of element that has the class .edit_mode
//         "load" = replaces the container of the upload button with the image.
// type = where the upload image has been triggered from ie (image element or testimonial image button etc.)
function thrive_open_media(button, action, type, element_wrapper, new_image) {
    var file_frame;
    if (action == "load") {
        button.parent().addClass("tve_active_image");
    }
    else {
        button.addClass("tve_active_image");
    }

    if (file_frame) {
        file_frame.open();
        return;
    }
    file_frame = wp.media.frames.file_frame = wp.media({
        title: jQuery(this).data('uploader_title'),
        button: {
            text: jQuery(this).data('uploader_button_text'),
        },
        multiple: false  // Set to true to allow multiple files to be selected
    });

    // check if image part of feature grid
    if (jQuery(button).parents().is(".thrv_feature_grid")) {
        var thrv_feature_grid = true;
    }

    file_frame.on('select', function () {
        attachment = file_frame.state().get('selection').first().toJSON();
        if (action == "save") {
            // insert url into input field immediately before in the DOM
            jQuery(".tve_active_image").attr("src", attachment.url);
            jQuery(".tve_active_image").removeClass("tve_active_image");
        } else {
            // handle images in the editor
            // if not feature grid image then send all details including captions
            if (type == "image_element" && !thrv_feature_grid) {
                data = {
                    url: attachment.url,
                    alt_text: attachment.alt,
                    caption: attachment.caption,
                    id: attachment.id,
                    width: attachment.width,
                    height: attachment.height
                }
                jQuery.post(tve_path_params.shortcodes_dir + 'thrv_image_load.php', data, function (response) {
                    jQuery(".tve_active_image").replaceWith(response);
                });
            }
            // ignore captions if feature grid image
            else if (type == "image_element" && thrv_feature_grid) {
                data = {
                    url: attachment.url,
                    alt_text: attachment.alt
                }
                jQuery.post(tve_path_params.shortcodes_dir + 'thrv_image_load_feature_gallery.php', data, function (response) {
                    jQuery(".tve_active_image").replaceWith(response);
                });
            }
            // replace testimonial image
            else if (type == "testimonial_button") {
                jQuery(element_wrapper).find(".tve_ts_o img").attr("src", attachment.url);
            }
        }
    });
    file_frame.open();
}

function convert_element_into_edit_mode(element, e) {
    e.stopPropagation();
    tve_refresh(false);
    hide_sub_menu();
    hide_element_right_menu();
    hide_element_left_menu();

    element.addClass("edit_mode");

    // if Element is a text element:
    if (element.is(text_elements)) {
        destroy_sortables();
        element.attr("contenteditable", "true");
        element.lightOrDark();

        if (e.type == "click" && !(jQuery(e.target).is(selection_loss_tags))) {
            var caretRange = getMouseEventCaretRange(e);
            window.setTimeout(function () {
                selectRange(caretRange);
            }, 10);
        }

        if (element.is(text_elements_inline_only)) {
            load_control_panel_menu(e.target, "text_inline_only");
        } else {
            // in some shortcodes, we shouldn't have formatting menu options display
            if (!element.is(text_element_no_formatting)) {
                load_control_panel_menu(e.target, "text");
            }
        }
    }

    // Perform checks to see what the element is and then load corresponding control panel
    // Else, if element is an image element:
    else if (element.is("img")) {
        e.preventDefault();
        load_control_panel_menu(element, "img");

        // if linked, show unlink buttons
        if (jQuery(element).parent().is("a")) {
            jQuery(".tve_link_btns").show();
        }
    }
    else if (element.children().is(".tve_btn")) {
        e.preventDefault();
        load_control_panel_menu(e.target, "button");
    } else if (element.children().is(".tve_cb")) {
        e.preventDefault();
        load_control_panel_menu(e.target, "contentbox");
    } else if (element.children().is(".tve_fg")) {
        e.preventDefault();
        load_control_panel_menu(e.target, "guarantee");
    } else if (element.children().is(".tve_ca")) {
        e.preventDefault();
        load_control_panel_menu(e.target, "calltoaction");
    } else if (element.children().is(".tve_ts")) {
        e.preventDefault();
        load_control_panel_menu(e.target, "testimonial");
    } else if (element.children().is(".tve_ul")) {
        e.preventDefault();
        load_control_panel_menu(e.target, "bullets");
    } else if (element.children().is(".tve_scT")) {
        e.preventDefault();
        load_control_panel_menu(e.target, "tabs");
    } else if (element.children().is(".tve_faq")) {
        e.preventDefault();
        load_control_panel_menu(e.target, "toggle");
    } else if (element.is(".thrv_custom_html_shortcode")) {
        e.preventDefault();
        load_control_panel_menu(e.target, "custom_html");
    } else if (element.is(".thrv_feature_grid")) {
        e.preventDefault();
        load_control_panel_menu(e.target, "feature_grid");
    } else if (element.is(".thrv_cc_icons, .thrv_cc_wrapper, .tve_cc_logo")) {
        e.preventDefault();
        element = tve_get_shortcode_wrapper_click(e.target);
        load_control_panel_menu(element, "cc_icons")
    } else if (element.is(".thrv_page_section")) {
        e.preventDefault();
        load_control_panel_menu(element, "page_section");
    }
    else if (element.is(".tve_prt")) {
        e.preventDefault();
        load_control_panel_menu(element, "pricing_table");
    } else if (element.is('.thrv_content_container_shortcode')) {
        e.preventDefault();
        load_control_panel_menu(element, 'content_container');
    }

    element.focus();
    return true;
}

function activate_editmode_keyboard_listener() {
    jQuery(document).on("keydown.editmode", function (e) {

        var item_being_edited = jQuery(".edit_mode");

        // don't listen to keyboard if item being edited isn't in focus.
        if (jQuery(':focus').is(item_being_edited)) {

            if (item_being_edited.is(text_elements)) {

                // If enter key but not the shift key pressed
                if (e.which == 13 && !e.shiftKey) {

                    e.preventDefault();
                    block_container = get_parent_block_element(item_being_edited);
                    block_container_tag_name = block_container.get(0).tagName;
                    remove_contenteditable_true();

                    text_after_cursor = get_text_after_cursor();
                    text_after_cursor = jQuery(text_after_cursor).text();

                    switch (block_container_tag_name) {
                        case "P":
                        case "H1":
                        case "H2":
                        case "H3":
                        case "H4":
                        case "H5":
                        case "H6":
                        case "ADDRESS":
                            add_paragraph_to_dom(item_being_edited, text_after_cursor, get_cursor_offset(), e, block_container_tag_name);
                            break;
                        case "UL":
                            add_li_to_dom(item_being_edited, text_after_cursor, "UL", get_cursor_offset(), e);
                            break;
                        case "OL":
                            add_li_to_dom(item_being_edited, text_after_cursor, "OL", get_cursor_offset(), e);
                            break;
                        case "BLOCKQUOTE":
                            add_blockquote_to_dom(item_being_edited, e);
                            break;
                        case "PRE":
                            add_break_to_dom(item_being_edited, e);
                            break;
                    }
                    return true;
                }

                // If Enter and Shift keys are pressed together
                if (e.which == 13 && e.shiftKey) {
                    e.preventDefault();
                    add_break_to_dom(item_being_edited, e);
                    return true;
                }

                // if tab key pressed - show a tab
                if (e.which == 9) {
                    document.execCommand("indent", true, null);
                    if (e.preventDefault) {
                        e.preventDefault();
                    }

                }
            }

            if (item_being_edited.is("img")) {

                // If Delete key is pressed
                if (e.which == 46 || e.which == 8) {
                    item_being_edited.remove();
                }
            }
        }
    });
}

function remove_contenteditable_true() {
    jQuery("[contenteditable]").removeAttr('contenteditable');
}

function deactivate_editmode_keyboard_listener() {
    jQuery(document).off("keydown.editmode");
}

/**
 * Add blockquote to DOM, used when editing a blockquote
 * @param item_being_edited
 * @param e
 */
function add_blockquote_to_dom(item_being_edited, e) {
    item_being_edited.after('<blockquote contenteditable="true"></blockquote>');
    item_being_edited.next('blockquote').focus();
    convert_element_into_edit_mode(item_being_edited.next('blockquote'), e);
}

/**
 * Add LI tag to the dom - used when editing a list
 * @param item_being_edited
 * @param text_after_cursor
 * @param list_type
 * @param cursor_offset
 * @param e
 */
function add_li_to_dom(item_being_edited, text_after_cursor, list_type, cursor_offset, e) {
    if (!cursor_offset) {
        item_being_edited.html("&#x200b");
    }
    if (!check_element_empty(item_being_edited)) {
        item_being_edited.after('<li>' + text_after_cursor + '</li>');
        item_being_edited.next('li').focus();
        convert_element_into_edit_mode(item_being_edited.next('li'), e);
    } else {
        // if current element is empty, and enter pressed, assume the user wants to break out of the bullet list.
        item_being_edited.replaceWith("</" + list_type + ">");
        add_paragraph_to_dom(item_being_edited, text_after_cursor, e);
    }
}


/**
 * Adds a new paragraph to the DOM in a contenteditable div
 * @param item_being_edited
 * @param text_after_cursor
 * @param cursor_offset
 * @param e
 * @param block_container_tag_name
 */
function add_paragraph_to_dom(item_being_edited, text_after_cursor, cursor_offset, e, block_container_tag_name) {
    // add invisible character to empty paragraph otherwise browsers won't show it
    if (!cursor_offset) {
        item_being_edited.html("&#x200b");
    }

    item_being_edited.after('<' + block_container_tag_name + ' contenteditable= "true">' + text_after_cursor + '</' + block_container_tag_name + '>');

    // if currently edited item has custom formatting css, then this should be duplicated to the next element
    tve_copy_style(item_being_edited, item_being_edited.next());

    item_being_edited.next(block_container_tag_name).focus();
    e.target = item_being_edited.next(block_container_tag_name);
    convert_element_into_edit_mode(item_being_edited.next(block_container_tag_name), e);
}

/**
 * Adds a line break to the DOM. A bit of a hack because there is no standard x-browser solution for this.
 * @param item_being_edited
 * @param e
 */
function add_break_to_dom(item_being_edited, e) {

    var sel = rangy.getSelection();
    if (sel.getRangeAt && sel.rangeCount) {
        range = sel.getRangeAt(0);
        var br = document.createElement("br");
        range.insertNode(br);
        sel.collapse(br.parentNode, 1 + tve_get_node_index(br));
        range.setStart(br.parentNode, 1 + tve_get_node_index(br));
        range.setEnd(br.parentNode, 1 + tve_get_node_index(br));

        if (tve_is_empty_line_break(br)) {
            // TODO
            range.insertNode(document.createElement("br"));

            // Compensate for nonstandard implementations of insertNode
            sel.collapse(br.parentNode, 1 + tve_get_node_index(br));
            range.setStart(br.parentNode, 1 + tve_get_node_index(br));
            range.setEnd(br.parentNode, 1 + tve_get_node_index(br));
        }
    }
}

/**
 * Checks to see if an element contains text
 * @param item_being_edited
 * @returns {boolean}
 */
function check_element_empty(item_being_edited) {
    if (item_being_edited.text()) {
        return false;
    } else {
        return true;
    }
}

/**
 * Run through parents to retrieve the first block level element (used for when the user selects
 * something within an element that itself doesn't have controls attached to it, but that might
 * be a child of an element that does.
 * @param element
 * @returns {*}
 */
function get_parent_block_element(element) {
    tag_name = jQuery(element).get(0).tagName;
    if (block_tags.indexOf(tag_name) != -1) {
        return element;
    } else {
        // parent is not a block level element so need to search through dom tree and find first matching block parent
        return element.parents(block_tags).first();
    }
}

/**
 * Get text before the cursor position
 * @param cursorIndex
 * @param item_being_edited
 * @returns {string}
 */
function get_text_before(cursorIndex, item_being_edited) {
    return item_being_edited.html().substring(0, cursorIndex);
}

/**
 * Get text after the cursor position
 * @param cursorIndex
 * @param item_being_edited
 * @returns {string}
 */
function get_text_after(cursorIndex, item_being_edited) {
    return item_being_edited.html().substring(cursorIndex);
}

/**
 * function to return the cursor position within a contenteditable
 * used on first selection of the text.
 * @returns {*}
 */
function get_cursor_position() {

    return rangy.saveSelection();
}

/**
 * There are custom menu items for each group of elements
 * this function runs when edit_mode is activated and loads the appropriate menu for the element
 * @param element - element that's current being edited
 * @param type - type of the element (e.g "text", "image", "bullet" etc.)
 */
function load_control_panel_menu(element, type) {

    if (jQuery(".tve_cpanel_onpage:visible").length != 0) {
        // remove any cp menus that are open
        hide_control_panel_menu();
    }

    /** show menu wrapper and correct menu item **/
    menu_wrapper = jQuery(".tve_cpanel_onpage");
    menu_to_show = jQuery(".tve_secondLayer, #" + type + "_menu, .tve_clear");

    /** calcluate offset and positions needed for display **/
    editor_pos = jQuery("#tve_editor").offset();

    /** menu should sit inside the editor so need to find width and copy across **/
    editor_width = jQuery("#tve_editor").width();
    jQuery(menu_wrapper).css("width", editor_width + "px");

    /** get offset of element to determine correct placement of menu **/
    element_pos = jQuery(".edit_mode").offset();

    /** have to make this synchronous or will return false values, especially on
     * slow machines that might try and calculate the height before elements are visible */
    jQuery.when(jQuery(menu_wrapper).show(), jQuery(menu_to_show).show()).done(function () {
        menu_wrapper_height = jQuery(".tve_cpanel_onpage").height();

        /** display menu above the element being edited and in line with the left edge of the editor div **/
        jQuery(menu_wrapper).css("top", (element_pos.top - menu_wrapper_height - 10) + "px");
        jQuery(menu_wrapper).css("left", editor_pos.left + "px");

        /** set menu options to match values **/
        load_button_values(jQuery(".edit_mode"), type);
    });

    /** dynamically load custom colour controls if appliccable **/
    tve_load_custom_colours(element, type);
}

/**
 * Hides the popup control panel
 */
function hide_control_panel_menu() {
    jQuery(".tve_cpanel_onpage, .tve_secondLayer, .tve_secondLayer>div").hide();
}

/**
 * calculates the position of, and shows, the right hand menu icons
 * position is absolute so need to calculate the position relative to the element being edited
 * @param element
 * @param element_left_position
 * @param element_top_position
 * @param element_width
 */
function display_element_right_menu(element, element_left_position, element_top_position, element_width) {

    // active delete is the class that the delete button looks for on click to know which element to delete.
    jQuery(".active_delete").removeClass("active_delete");
    jQuery(element).addClass("active_delete");

    // calculate position of elements
    var delete_icon = jQuery("#delete_icon");
    delete_icon_width = 20;
    delete_icon_height = 20;

    // set position and display
    delete_icon.css("left", (element_left_position + element_width) - delete_icon_width + "px");
    delete_icon.css("top", (element_top_position) + "px");
    delete_icon.show();
}
/**
 * calculates the position of, and shows, the left hand menu icons.
 * position is absolute so need to calculate the position relative to the element being edited.
 * @param element
 * @param element_left_position
 * @param element_top_position
 */
function display_element_left_menu(element, element_left_position, element_top_position) {
    // active delete is the class that the delete button looks for on click to know which element to delete.
    jQuery(".active_duplicate").removeClass("active_duplicate");
    jQuery(element).addClass("active_duplicate");
    var duplicate_icon = jQuery("#duplicate_icon");
    duplicate_icon_height = 20;
    duplicate_icon.css("left", element_left_position + "px");
    duplicate_icon.css("top", element_top_position + "px");
    duplicate_icon.show();
}
/**
 * Hides the right popup menu (shown in the top right hand corner of the element on hover
 */
function hide_element_right_menu() {
    jQuery('#delete_icon').hide();
    jQuery(".active_delete").removeClass("active_delete");
}
/**
 * Hides the left popup menu (shown in the top left hand corner of the element on hover
 */
function hide_element_left_menu() {
    jQuery('#duplicate_icon').hide();
    jQuery(".active_duplicate").removeClass("active_duplicate");
}

/**
 * Add certain elements to the DOM.  Have to use javascript because no Wordpress hooks exist for these locations
 */
function add_element_menu_icons() {
    jQuery("html").prepend("<div id='delete_icon'></div>");
    jQuery("html").prepend("<div id='duplicate_icon'></div>");

    cpanel_pos_class = "";
    if (tve_path_params.cpanel_display_options['position'] == "left") {
        cpanel_pos_class = "tve_is_flipped";
    }
    jQuery("body").wrapInner("<div class='tve_wrap_inner " + cpanel_pos_class + "'></div>");
}

/**
 * returns the first block level container up the DOM tree
 * @param node
 * @returns {*}
 */
function getBlockContainer(node) {
    while (node) {
        // regex for determinining block element
        if (node.nodeType == 1 && /^(A|P|H[1-6]|DIV|OL|UL|LI|PRE|ADDRESS|BLOCKQUOTE)$/i.test(node.nodeName)) {
            return node;
        }
        node = node.parentNode;
    }
}

/**
 * Returns text after cursor for certain elements for instances where the user creates a new line, for example.
 * @returns {DocumentFragment}
 */
function get_text_after_cursor() {
    var sel = rangy.getSelection();
    if (sel.rangeCount) {
        var selRange = sel.getRangeAt(0);
        var blockEl = getBlockContainer(selRange.endContainer);
        if (blockEl) {
            var range = selRange.cloneRange();
            range.selectNodeContents(blockEl);
            range.setStart(selRange.endContainer, selRange.endOffset);
            return range.extractContents();
        }
    }
}

// function returns location offset of cursor
function get_cursor_offset() {
    sel = rangy.getSelection();
    if (sel.rangeCount) {
        selRange = sel.getRangeAt(0);
        return selRange.startOffset;
    }
}

// function that handles removing a tab from the tabs shortcode
function remove_shortcode_tab(element) {
    target = jQuery(element).find("span").attr("class");
    tabs_wrapper = jQuery(element).parents(".thrv_tabs_shortcode").first();
    jQuery(element).remove();
    jQuery("." + target).remove();
    tve_resize_tabs();
    show_first_tab(tabs_wrapper);
}

// function takes element as input and looks through parents to figure out if the element is a child of a thrive shortcode or not.
// if wrapper found then will return element, else will return false
// if the element selected is the shortcode wrapper, then it returns itself.
// this is the logic that takes place when user hovers over an element where right / left menu is set to display
function tve_get_shortcode_wrapper_hover(element) {
    if (jQuery(element).is(".thrv_wrapper")) {
        return element;
    } else {
        var is_shortcode = jQuery(element).parents(".thrv_wrapper, .tve_faq").first();
        if (is_shortcode.length !== 0) {
            return is_shortcode;
        } else {
            return false;
        }
    }
}

/**
 * Takes element as input and looks through parents to figure out if the element is a child of a thrive shortcode or not.
 * if wrapper found then will return element, else will return false
 * if the element selected is the shortcode wrapper, then it returns itself.
 * this is the logic that takes place when user clicks on an editable element
 */
function tve_get_shortcode_wrapper_click(element) {
    if (jQuery(element).is(".thrv_wrapper")) {
        return element;
    } else {
        var is_shortcode = jQuery(element).parents(".thrv_wrapper").first();
        if (is_shortcode.length !== 0) {
            return is_shortcode;
        } else {
            return false;
        }
    }
}

function refresh_sortable_positions(sortable) {
    jQuery(sortable).sortable('refreshPositions');
}

// instead of using nested sortables that gives a jumpy user interface, we need to find sections of the page that require dropzones added.  This function handles them.
function refresh_dropzones() {
    jQuery(".tve_dropzone").remove();
    jQuery(dropzone_elements).each(function () {
        if (!jQuery.trim(jQuery(this).html()).length) {
            add_dropzone_as_child(this);
        }
        else {
            ensure_editor_droppable(this);
        }
    });
    ensure_editor_droppable(jQuery("#tve_editor"));
    insert_dropzone_between_unsortables(jQuery("#tve_editor"));
}

// there are a few instances where it's not possible to add a new element to the top/bottom of the editor because of how we're not using nested sortables.  This function handles those edge cases.
function ensure_editor_droppable(element) {
    first_element = jQuery(element).children(":first");
    last_element = jQuery(element).children(":last");

    // add to top
    if (first_element.is(prefix_with_dropzone) || jQuery(first_element).length == 0) {
        prepend_dropzone_to_element(element);
    }

    // add to bottom
    if (last_element.is(prefix_with_dropzone)) {
        append_dropzone_to_element(element);
    }
}

// this function searches for two items that can't be sorted and thus can't have items dropped between them and adds a drop zone.
function insert_dropzone_between_unsortables() {
    var prefix_dropzone_array = prefix_with_dropzone.split(',');
    for (i = 0; i < prefix_dropzone_array.length; i++) {
        jQuery("#tve_editor " + prefix_dropzone_array[i] + " + " + prefix_dropzone_array[i]).before("<div class='tve_dropzone'></div>");
    }
}

function add_dropzone_as_child(element) {
    jQuery(element).html("<div class='tve_dropzone'></div>");
}

function prepend_dropzone_to_element(element) {
    jQuery(element).prepend("<div class='tve_dropzone'></div>");
}

function append_dropzone_to_element(element) {
    jQuery(element).append("<div class='tve_dropzone'></div>");
}

/**
 * Clones currently hovered element and places it immediately afterwards in the DOM
 * @param element
 */
function duplicate_element(element) {
    jQuery(element).after(jQuery(element).clone());
    cloned_element = jQuery(element).next();
    tve_clone_custom_styles(element, cloned_element);
}

function hide_menu_icons() {
    jQuery("#delete_icon, #duplicate_icon").hide();
}

// checks to see if the element is currently being edited
function check_element_edit_mode(element) {
    if (jQuery(".edit_mode").length != 0) {
        return true;
    } else {
        return false;
    }
}

// function to display notifications
function tve_add_notification(text, error) {
    error_box = jQuery("#tve_notification_box");
    if (error) {
        jQuery(error_box).addClass("tve_error_box");
    } else {
        jQuery(error_box).removeClass("tve_error_box");
    }
    jQuery(error_box).text(text);
    jQuery(error_box).animate({left: 100}, 700);
    setTimeout(function () {
            jQuery(error_box).animate({left: -950}, 700);
        },
        2000);
}

function tve_editor_init(e, element) {

    var sel = rangy.getSelection();
    /** don't change element if trying to select text **/
    if (sel.toString().length) {
        return false;
    }

    // certain elements shouldn't be editable
    if (jQuery(e.target).is(block_editing_elements)) {
        // if credit card logo, then get wrapper and use that as element
        if (jQuery(e.target).is(".tve_cc_logo")) {
            element = tve_get_shortcode_wrapper_click(e.target);
            e.target = element;
        } else {
            return false;
        }
    }

    e.preventDefault();
    if (!jQuery(e.target).hasClass("edit_mode")) {
        convert_element_into_edit_mode(element, e);
        jQuery(document).on("click.clearFocus", document, clearFocus);
        jQuery(".edit_mode").on("click.editHyperlinks", "a", tve_edit_hyperlinks);
    } else {
        // if user clicks on element that's in edit mode then prevent the event from bubbling up the DOM and deselecting the element.
        if (!jQuery(e.target).is("a")) {
            tve_hide_hyperlink_options();
        }
        e.stopPropagation();
    }
}

// this adds element to page when user clicks instead of drags
// - When an element icon is clicked instead of dragged, this element is added to the bottom of the page.
// - When an element is selected and an element icon is clicked, the new element is added below the selected one.
// - TODO When a container (box, column) is selected, a click on an element icon adds that element at the bottom, within the selected container.
function tve_add_element_on_click(html_to_add) {
    tve_set_window_content_before_action();
    var currently_being_edited = jQuery(".edit_mode");
    if (jQuery(currently_being_edited).length !== 0) {
        jQuery(currently_being_edited).after(html_to_add);
    }
    else {
        jQuery("#tve_editor").append(html_to_add);
    }
    tve_set_window_content_after_action();
    return true;
}

// extracts api item id from class
function get_api_item_id(item) {
    var item_classes = jQuery(item).attr("class").split(" ");
    for (i = 0; i < item_classes.length; i++) {
        if (item_classes[i].indexOf("tve_api_id_") !== -1) {
            api_item_id = item_classes[i].toString();
            api_item_id = api_item_id.replace("tve_api_id_", "");
            return api_item_id;
        }
    }
}

function insert_element_into_page(event, ui, drag, response) {
    var $insertedElement = jQuery(response);
    if (drag) {
        // dragged on so replace at target
        jQuery(event.target).find("." + css_class).replaceWith($insertedElement);
    } else {
        // clicked, so no target set, determine where should be placed
        tve_add_element_on_click($insertedElement);
    }

    tve_insert_element_after($insertedElement, drag);
    tve_resize_tabs();
    add_elements_after_dragging();
    tve_refresh(true);
}

// function to add the thrv_wrapper class to elements
function tve_thrv_wrap(element) {
    element = "<div class='thrv_wrapper'>" + element + "</div>";
    return element;
}

// make sure that the user really wants to leave the page.
window.onbeforeunload = function () {
    return "If you leave this page then any unsaved changes will be lost";
}

// filter pasted text
function tve_filter_paste(textarea) {
    filtered_textarea_html = jQuery.htmlClean(jQuery(textarea).html(), {
        format: true,
        removeTags: ["span", "div"],
        removeAttrs: ["class"]
    });
    return filtered_textarea_html;
}

// Minor hackery.  There currently is no beforestart event for jQuery sortable, so when adding the HTML to the undoManager, there is some messy code
// added by jQuery sortable that should be removed before being added to the Undo Manager.
function tve_set_window_content_before_action() {
    el = document.createElement("div");
    jQuery(el).html(jQuery("#tve_editor").html());
    jQuery(el).find(".tve_placeholder, .tve-ui-helper").remove();
    window.beforeMove = jQuery(el).html();
    jQuery(el).remove();
}

// Add to undo manager
function tve_set_window_content_after_action() {
    tve_add_undo_action("sortable", window.beforeMove, jQuery("#tve_editor").html());
}

function tve_add_undo_action(action, htmlbefore, htmlafter) {
    if (action == "sortable") {
        // make undo-able
        tve_undoManager.add({
            undo: function () {
                tve_restore_editor_content(htmlbefore);
            },
            redo: function () {
                tve_restore_editor_content(htmlafter);
            }
        });
    }
}

function tve_restore_editor_content(html) {
    jQuery("#tve_editor").html(html);
}

// copy inline styles from one element to another
function tve_copy_style(element_copied_from, element_copied_to) {
    element_copied_to.attr("style", element_copied_from.attr("style"));
}

// "A collapsed line break is a br that begins a line box which has nothing
// else in it, and therefore has zero height."
function tve_is_empty_line_break(br) {
    // Add a zwsp after it and see if that changes the height of the nearest
    // non-inline parent.  Note: this is not actually reliable, because the
    // parent might have a fixed height or something.
    var ref = br.parentNode;
    while (jQuery.getComputedStyle(ref).display == "inline") {
        ref = ref.parentNode;
    }

    var origStyle = {
        height: ref.style.height,
        maxHeight: ref.style.maxHeight,
        minHeight: ref.style.minHeight
    };

    ref.style.height = 'auto';
    ref.style.maxHeight = 'none';
    if (!(jQuery.browser.msie && jQuery.browser.version < 8)) {
        ref.style.minHeight = '0';
    }
    var space = document.createTextNode('\u200b');
    var origHeight = ref.offsetHeight;
    if (origHeight == 0) {
        throw 'isCollapsedLineBreak: original height is zero, bug?';
    }
    br.parentNode.insertBefore(space, br.nextSibling);
    var finalHeight = ref.offsetHeight;
    space.parentNode.removeChild(space);

    ref.style.height = origStyle.height;
    ref.style.maxHeight = origStyle.maxHeight;
    if (!(jQuery.browser.msie && jQuery.browser.version < 8)) {
        ref.style.minHeight = origStyle.minHeight;
    }

    /** Allow some leeway in case the zwsp didn't create a whole new line, but
     * only made an existing line slightly higher.  Firefox 6.0a2 shows this
     * behavior when the first line is bold.*/
    return origHeight < finalHeight - 5;
}

// determine where the current node type lies with respect to siblings of the same type
function tve_get_node_index(node) {
    var i = 0;
    while ((node = node.previousSibling)) {
        i++;
    }
    return i;
}

jQuery.getComputedStyle = function (node, style) {
    if (window.getComputedStyle) {
        return window.getComputedStyle(node, style);
    }
    if (node.currentStyle) {
        return node.currentStyle;
    }
    return null;
};


var tve_stylesheet = (function () {
    var style = jQuery(".tve_custom_style")[0];

    // WebKit hack :(
    style.appendChild(document.createTextNode(""));

    // Add the <style> element to the page
    return style.sheet;
})();

function tve_add_css_rule(selector, rules, index) {
    try {
        if (tve_stylesheet.insertRule) {
            tve_stylesheet.insertRule(selector + "{" + rules + "}", index);
        }
        else {
            tve_stylesheet.addRule(selector, rules, index);
        }
    } catch (err) {
        txt = "There was an error on this page.\n\n";
        txt += "Error description: " + err.message + "\n\n";
        txt += "Click OK to continue.\n\n";
        alert(txt);
    }
}

function tve_direct_css_insert(css_rule) {
    if (tve_stylesheet.insertRule) {
        tve_stylesheet.insertRule(css_rule, 0);
    }
    else {
        tve_stylesheet.addRule(css_rule, 0);
    }
}

/**
 * Copies the custom styles from the original element to the new element by creating a new unique id
 * @param element - original element that's being copied
 * @param cloned_element - the newly cloned element added to the DOM
 */
function tve_clone_custom_styles(element, cloned_element) {
    var already_cloned_id = new Array();
    jQuery(cloned_element).find("[data-tve-custom-colour]").each(function () {
        css_id = jQuery(this).attr("data-tve-custom-colour");
        if (already_cloned_id.indexOf(css_id) == -1) {
            css_rules = new Array();
            css_rules = tve_return_css_rule(css_id);
            new_unique_id = tve_get_unique_css_identifier(jQuery(this), false);
            for (var i = 0; i < css_rules.length; i++) {
                new_css_rule = css_rules[i].replace(css_id, new_unique_id);
                tve_direct_css_insert(new_css_rule);
            }
            /** replace all instances of the old id with the new id **/
            jQuery(cloned_element).find("[data-tve-custom-colour='" + css_id + "']").each(function () {
                if (already_cloned_id.indexOf(jQuery(this).attr("data-tve-custom-colour")) == -1) {
                    jQuery(this).attr("data-tve-custom-colour", new_unique_id);
                }
            });
            already_cloned_id.push(css_id);
        }
    });
}

/**
 * function to delete css rules in custom stylesheet
 * @param selector - delete all rules that match selector
 */
function tve_remove_css_rule(selector) {
    var myrules = tve_stylesheet.cssRules ? tve_stylesheet.cssRules : tve_stylesheet.rules;
    tve_stylesheet.crossdelete = tve_stylesheet.deleteRule ? tve_stylesheet.deleteRule : tve_stylesheet.removeRule;
    for (i = 0; i < myrules.length; i++) {
        if (myrules[i].selectorText.toLowerCase().indexOf(selector) != -1) {
            tve_stylesheet.crossdelete(i);
            i--;
        }
    }
}

/**
 * function to return the css rule of a matching selector
 * @param selector - css selection text
 * @returns {string} - first matching css style
 */
function tve_return_css_rule(css_id) {
    selector = '[data-tve-custom-colour="' + css_id + '"]';
    var myrules = tve_stylesheet.cssRules ? tve_stylesheet.cssRules : tve_stylesheet.rules;
    tve_stylesheet.crossdelete = tve_stylesheet.deleteRule ? tve_stylesheet.deleteRule : tve_stylesheet.removeRule;
    matching_rules = new Array();
    for (i = 0; i < myrules.length; i++) {
        if (myrules[i].selectorText.toLowerCase().indexOf(css_id) != -1) {
            matching_rules.push(myrules[i].cssText);
        }
    }
    return matching_rules;
}

/**
 * function to extract the custom inline rules from the document
 * @returns {string} - string of css to be saved in db
 */
function tve_extract_stylesheet_rules() {
    var myrules = tve_stylesheet.cssRules ? tve_stylesheet.cssRules : tve_stylesheet.rules;
    var inline_rules = "";
    for (var i = 0; i < myrules.length; i++) {
        inline_rules = inline_rules + myrules[i].cssText + "\n";
    }
    return inline_rules;
}

/**
 * returns the style type of the element (for example "1")
 * @param thrv_wrapper element
 * @returns {data|*|data|data|data|data}
 */
function tve_get_element_style(element) {
    return jQuery(element).data("tve-style");
}

/**
 * gets colour picker data and initialises the colour pickers one by one
 */
function tve_load_colour_pickers(colour_picker_array, type) {

    jQuery("#tve_colour_pickers").remove();

    var colour_picker_wrapper = document.createElement("div");
    colour_picker_wrapper.id = "tve_colour_pickers";

    var i = 0;
    for (var prop in colour_picker_array) {

        /** determine how many colours make up the CSS property.
         * For instance, a gradient background has two colours instead of one, so need two colour pickers **/
        css_property = colour_picker_array[prop]['property'];
        css_modified_property = css_property;
        if (css_property == "border" || css_property == "border-color") {
            css_modified_property = "border-bottom-color";
        }

        /** error catching so that if the custom colour mapping selector can't be found, the mapping is ignored **/
        if (jQuery(".edit_mode " + colour_picker_array[prop]['selector']).length != 0) {
            colour_picker_array[prop]['ignore'] = false;
        } else {
            console.log("Content Builder Error: can't find en element for this custom colour mapping function: " + ".edit_mode " + colour_picker_array[prop]['selector']);
            colour_picker_array[prop]['ignore'] = true;
        }

        if (!colour_picker_array[prop]['ignore']) {
            current_colour_text = jQuery(".edit_mode " + colour_picker_array[prop]['selector']).css(css_modified_property);
            current_colour = new Array();
            current_colour = tve_find_colours(current_colour_text);

            /** if no colours found, then default to white**/
            if (typeof current_colour === "undefined" || !current_colour) {
                current_colour = new Array();
                current_colour[0] = "#FFFFFF";
            }

            /** Sometimes we can have 3 colours returned from a gradient that includes a background colour.
             * In this instance, we only want the last two colours */
            if (current_colour && typeof current_colour != "undefined" && current_colour.length == 3) {
                current_colour = current_colour.slice(1, 3);
            }

            colour_picker_array[prop]['num_colours'] = current_colour.length;
            colour_picker_array[prop]['colours'] = current_colour;

            for (var j = 0; j < current_colour.length; j++) {
                tve_build_colour_picker_fragment(colour_picker_wrapper, "tve_colour_picker" + i + "-" + j);
            }
        }
        i++;
    }

    /** add colour pickers to DOM **/
    colour_picker_div = jQuery("#" + type + "_menu" + " .tve_color_picker");
    jQuery(colour_picker_wrapper).appendTo(colour_picker_div);


    // 2. initialise them
    var i = 0;
    for (var prop in colour_picker_array) {
        if (!colour_picker_array[prop]['ignore']) {
            for (var x = 0; x < colour_picker_array[prop]['num_colours']; x++) {
                tve_init_colour_pickers(colour_picker_array, "#tve_colour_picker" + i + "-" + x, colour_picker_array[prop]['label'], colour_picker_array[prop]['selector'], colour_picker_array[prop]['property'], colour_picker_array[prop]['value'], colour_picker_array[prop]['colours'][x], colour_picker_array[prop]['colours'], i);
            }
        }
        i++;
    }
}

/**
 * builds the colour picker controls dynamically
 * @param label - label to be added to colour picker (e.g "Text Colour")
 * @param selector - jquery selector of element to apply to (e.g "tcb_cb1")
 * @param css_property - css property to apply (e.g. "background-color")
 */
function tve_build_colour_picker_fragment(colour_picker_wrapper, id, label, selector, css_property) {
    var this_colour_picker = document.createElement("input");
    this_colour_picker.type = "text";
    this_colour_picker.setAttribute("data-default-color", "#effeff");
    this_colour_picker.id = id;

    var new_line = document.createElement("div");
    new_line.className = "tve_clear";

    colour_picker_wrapper.appendChild(this_colour_picker);
    colour_picker_wrapper.appendChild(new_line);
    this_colour_picker.onclick = function () {
        this_colour_picker.select();
    }
}

/**
 * initialises the colour pickers for custom colours
 * @param colour_picker_array - array of data for the loaded colour pickers
 * @param id - automatically generated id for the colour picker
 * @param label - appears next to the colour picker (e.g "Border color")
 * @param selector - jquery element to which the colour will be applied
 * @param css_property - e.g "text-shadow"
 * @param value_template - e.g "0 2px [color]" where [color] is replaced by the hex value read form the colour picker
 */
function tve_init_colour_pickers(colour_picker_array, id, label, selector, css_property, value_template, existing_colour, colours_array, colour_picker_array_id) {
    wpColorPickerL10n = new Array();
    wpColorPickerL10n['clear'] = "Clear";
    wpColorPickerL10n['defaultString'] = "Save as Favourite Colour";
    wpColorPickerL10n['pick'] = label;
    jQuery(id).val(existing_colour);


    var myOptions = {
        change: function (event, ui) {
            var unique_selector = tve_get_unique_css_identifier(modify_selector_edit_mode(selector), true);
            color = ui.color.toString();
            css_text = tve_get_custom_colour_css(colour_picker_array, selector, css_property, value_template, color, id, colours_array, colour_picker_array_id);
            css_text = css_text + tve_append_additional_styles_for_selector(id, colour_picker_array, selector, css_text, colour_picker_array_id);
            jQuery.when(tve_remove_css_rule(unique_selector)).then(
                tve_add_css_rule("[data-tve-custom-colour='" + unique_selector + "']", css_text, 0)
            );

            //console.log(tve_extract_stylesheet_rules());
        },
        hide: true,
        palettes: window.tve_custom_colours
    };

    // load colour pickers
    jQuery(id).wpColorPicker(myOptions);

    // unbind default button click event
    jQuery("input.button.button-small.wp-picker-default").unbind("click");
}

/**
 * Generates a unique id that can be used for attaching CSS styles
 * @param selector - the jquery element to generate unique css id for
 * @param return_current - boolean.  If true then will return the current id if exists, if false then will generate a completely new id.
 * @returns either existing id or newly generated id
 */
function tve_get_unique_css_identifier(selector, return_current) {
    if (jQuery(selector).attr("data-tve-custom-colour") && return_current) {
        return jQuery(selector).attr("data-tve-custom-colour");
    } else {
        rand_id = Math.floor((Math.random() * 100000000) + 1);
        jQuery(selector).attr("data-tve-custom-colour", rand_id);
        return rand_id;
    }
}

/**
 * Removes any custom colours that have been applied to an element
 */
function tve_remove_custom_colours(element) {
    jQuery(element).find("[data-tve-custom-colour]").removeAttr("data-tve-custom-colour");
}

/**
 * saves last used custom colour into global variable and enforces max memory limit
 * @param color - the last colour selected by the user in the color palette
 */
function tve_remember_custom_colour(color) {
    if (window.tve_custom_colours.indexOf(color) != -1) {
        tve_add_notification("Favourite colour " + color + " already exists!", true);
    } else {
        window.tve_custom_colours.push(color);
        window.tve_custom_colours = window.tve_custom_colours.slice(Math.max(window.tve_custom_colours.length - window.max_colour_history, 0));
    }
}

/**
 * Extracts the hex values / rgb values from a complex string of CSS.  For example - gradients.
 * @param text
 * @returns {Array|{index: number, input: string}}
 */
function tve_find_colours(text) {
    text = escapeHTML(text);
    var reRGB = /RGB\s*\(\s*-?[0-9]{1,3}%?\s*,\s*-?[0-9]{1,3}%?\s*,\s*-?[0-9]{1,3}%?\)/ig;
    var reRGBA = /RGBA\s*\(\s*[0-9]{1,3}%?\s*,\s*[0-9]{1,3}%?\s*,\s*[0-9]{1,3}%?\s*,\s*[0-9]*\.?[0-9]*\s*\)/ig;
    var reHex = /#[0-9a-f]{3}([0-9a-f]{3})?\b/ig;
    var reHSL = /HSL\s*\(\s*-?[0-9]{1,3}\s*,\s*[0-9]{1,3}%\s*,\s*[0-9]{1,3}%\)/ig;
    var reHSLA = /HSLA\s*\(\s*-?[0-9]{1,3}\s*,\s*[0-9]{1,3}%\s*,\s*[0-9]{1,3}%\s*,\s*[0-9]*\.?[0-9]*\s*\)/ig;
    var reNamed = /\bAliceBlue\b|\bAntiqueWhite\b|\bAqua\b|\bAquamarine\b|\bAzure\b|\bBeige\b|\bBisque\b|\bBlack\b|\bBlanchedAlmond\b|\bBlue\b|\bBlueViolet\b|\bBrown\b|\bBurlyWood\b|\bCadetBlue\b|\bChartreuse\b|\bChocolate\b|\bCoral\b|\bCornflowerBlue\b|\bCornsilk\b|\bCrimson\b|\bCyan\b|\bDarkBlue\b|\bDarkCyan\b|\bDarkGoldenRod\b|\bDarkGray\b|\bDarkGreen\b|\bDarkKhaki\b|\bDarkMagenta\b|\bDarkOliveGreen\b|\bDarkorange\b|\bDarkOrchid\b|\bDarkRed\b|\bDarkSalmon\b|\bDarkSeaGreen\b|\bDarkSlateBlue\b|\bDarkSlateGray\b|\bDarkTurquoise\b|\bDarkViolet\b|\bDeepPink\b|\bDeepSkyBlue\b|\bDimGray\b|\bDodgerBlue\b|\bFireBrick\b|\bFloralWhite\b|\bForestGreen\b|\bFuchsia\b|\bGainsboro\b|\bGhostWhite\b|\bGold\b|\bGoldenRod\b|\bGray\b|\bGreen\b|\bGreenYellow\b|\bHoneyDew\b|\bHotPink\b|\bIndianRed\b|\bIndigo\b|\bIvory\b|\bKhaki\b|\bLavender\b|\bLavenderBlush\b|\bLawnGreen\b|\bLemonChiffon\b|\bLightBlue\b|\bLightCoral\b|\bLightCyan\b|\bLightGoldenRodYellow\b|\bLightGrey\b|\bLightGreen\b|\bLightPink\b|\bLightSalmon\b|\bLightSeaGreen\b|\bLightSkyBlue\b|\bLightSlateGray\b|\bLightSteelBlue\b|\bLightYellow\b|\bLime\b|\bLimeGreen\b|\bLinen\b|\bMagenta\b|\bMaroon\b|\bMediumAquaMarine\b|\bMediumBlue\b|\bMediumOrchid\b|\bMediumPurple\b|\bMediumSeaGreen\b|\bMediumSlateBlue\b|\bMediumSpringGreen\b|\bMediumTurquoise\b|\bMediumVioletRed\b|\bMidnightBlue\b|\bMintCream\b|\bMistyRose\b|\bMoccasin\b|\bNavajoWhite\b|\bNavy\b|\bOldLace\b|\bOlive\b|\bOliveDrab\b|\bOrange\b|\bOrangeRed\b|\bOrchid\b|\bPaleGoldenRod\b|\bPaleGreen\b|\bPaleTurquoise\b|\bPaleVioletRed\b|\bPapayaWhip\b|\bPeachPuff\b|\bPeru\b|\bPink\b|\bPlum\b|\bPowderBlue\b|\bPurple\b|\bRed\b|\bRosyBrown\b|\bRoyalBlue\b|\bSaddleBrown\b|\bSalmon\b|\bSandyBrown\b|\bSeaGreen\b|\bSeaShell\b|\bSienna\b|\bSilver\b|\bSkyBlue\b|\bSlateBlue\b|\bSlateGray\b|\bSnow\b|\bSpringGreen\b|\bSteelBlue\b|\bTan\b|\bTeal\b|\bThistle\b|\bTomato\b|\bTurquoise\b|\bViolet\b|\bWheat\b|\bWhite\b|\bWhiteSmoke\b|\bYellow\b|\bYellowGreen\b/ig;
    var reSelected = new RegExp(reRGB.source + "|" + reRGBA.source + "|" + reHex.source + "|" + reHSL.source + "|" + reHSLA.source + "|" + reNamed.source, "ig")
    var foundColors = text.match(reSelected);
    return foundColors;
}

/**
 * Escapes the regex for colour extraction
 * @param s
 * @returns {XML|string}
 */
function escapeHTML(s) {
    return s.replace(/&/g, "&amp;").replace(/"/g, "&quot;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
}

/**
 * Takes the selector from the custom_colour mapping file and appends .edit mode to each comma separated jquery selector
 * @param selector - jquery selector of element to which a custom colour is being applied
 * @returns {string} - modified jquery selector
 */
function modify_selector_edit_mode(selector) {
    var selector_array = selector.split(',');
    for (var i = 0; i < selector_array.length; i++) {
        selector_array[i] = selector_array[i].replace(/^\s*|\s*$/g, '');
        selector_array[i] = ".edit_mode " + selector_array[i];
    }
    return selector_array.join(",");
}

/**
 * Creates the CSS for changing element custom colours
 * @param colour_picker_array - array of data for currently displayed colour pickers
 * @param css_property - ie. "background-image" or "border-color"
 * @param css_value_template - ie. "1px solid [color]" where colour picked is substituted
 * @param colour - rgb of the colour picked e.g rgb(20,20,20)
 * @param id - id of the active colour picker
 * @param colours_array (array) - holds the colour(s) of the css property prior to the change
 * @returns {string}
 */
function tve_get_custom_colour_css(colour_picker_array, selector, css_property, css_value_template, colour, id, colours_array, colour_picker_array_id) {
    switch (css_value_template) {
        case 'gradient':
            var css_text =
                "background-image: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, [color0]), color-stop(100%, [color1])) !important;" +
                    "background-image: -webkit-linear-gradient([color0] 0%, [color1] 100%) !important;" +
                    "background-image: -moz-linear-gradient([color0] 0%, [color1] 100%) !important;" +
                    "background-image: -o-linear-gradient([color0] 0%, [color1] 100%) !important;" +
                    "background-image: linear-gradient([color0] 0%, [color1] 100%) !important;";
            colour_number = id.slice(-1);
            colour_string = "[color" + colour_number + "]";
            css_text = replaceAll(colour_string, colour, css_text);

            /** when modifying gradient, need to find active
             * colour from colour picker to update display **/
            if (colour_number == "0") {
                alternate_colourpicker = id.slice(0, -1) + "1";
                existing_colour = jQuery(alternate_colourpicker).val();
                css_text = replaceAll("[color1]", existing_colour, css_text);
            } else {
                alternate_colourpicker = id.slice(0, -1) + "0";
                existing_colour = jQuery(alternate_colourpicker).val();
                css_text = replaceAll("[color0]", existing_colour, css_text);
            }
            return css_text;
            break;
        case 'box-shadow':
            var css_text =
                "-webkit-box-shadow: 10px 10px 5px 0px [color];" +
                    "-moz-box-shadow: 10px 10px 5px 0px [color];" +
                    "box-shadow: 10px 10px 5px 0px [color];";
            return css_text;
            break;
        default:
            css_value = css_value_template.replace('[color]', colour);
            css_text = css_property + ":" + css_value + " !important;";
            return css_text;
    }
}

/**
 * Looks for additioanl colour pickers that are being applied to the same selector in order to build a full rule set for selector
 * @param id
 * @param colour_picker_array
 * @param selector
 * @param css_text
 * @param colour_picker_array_id
 * @returns {string|*}
 */
function tve_append_additional_styles_for_selector(id, colour_picker_array, selector, css_text, colour_picker_array_id) {
    additional_css_text = "";
    for (j = 0; j < colour_picker_array.length; j++) {
        if (!colour_picker_array[j]['ignore']) {
            if (colour_picker_array_id != j) {
                if (colour_picker_array[j]['selector'] == selector) {
                    for (var x = 0; x < colour_picker_array[j]['num_colours']; x++) {
                        additional_css_text = additional_css_text + tve_get_custom_colour_css(
                            colour_picker_array,
                            colour_picker_array[j]['selector'],
                            colour_picker_array[j]['property'],
                            colour_picker_array[j]['value'],
                            jQuery("#tve_colour_picker" + j + "-" + x).val(),
                            "#tve_colour_picker" + j + "-" + x,
                            colour_picker_array[j]['colours'],
                            j);
                    }
                }
            }
        }
    }
    return additional_css_text;
}

/**
 * global string replacement
 * @param find
 * @param replace
 * @param str
 * @returns {*|XML|replace|string|void}
 */
function replaceAll(find, replace, str) {
    return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

/**
 * Escapes characters that throw off the Reg Ex string
 * @param str - String to escape
 * @returns {*|XML|replace|string|void} - escaped string
 */
function escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

/**
 * Checks whether the currently selected element has any custom colour mappings, and if it does then it calls a function to dynamically load the controls
 * @param element - element that is currently selected
 * @param type - type of element ie. "contentbox"
 */
function tve_load_custom_colours(element, type) {
    if (typeof tve_path_params.tve_colour_mapping[type] !== "undefined") {
        element_wrapper = tve_get_shortcode_wrapper_click(element);
        if (element_wrapper) {
            element_style = tve_get_element_style(element_wrapper);
            if (typeof tve_path_params.tve_colour_mapping[type][element_style] !== "undefined") {
                colour_controls = tve_path_params.tve_colour_mapping[type][element_style][tve_path_params.tve_loaded_stylesheet];
                tve_load_colour_pickers(colour_controls, type);
            } else {
                console.log("can't find colour controls for tve_path_params.tve_colour_mapping[" + type + "][" + element_style + "][" + tve_path_params.tve_loaded_stylesheet + "]")
            }
        }
    }
}

/**
 * Splits a CSS string into selectors and rules ready to be added to the inline stylesheet
 * @param css_text
 */
function tve_convert_css_into_array(css_text) {
    var css_code = css_text.match(/[^{}]+(?=\})/g);
    var css_selectors = css_text.match(/\[(.*?)\]/g);
    for (var i = 0; i < css_selectors.length; i++) {
        tve_add_css_rule(css_selectors[i], css_code[i], 0);
    }
}

/** Old controls.js file below here **/

function tve_change_bullet_style(bullet_style, element) {
    bullet_list = jQuery(element).find(".tve_ul");
    bullet_list.removeClass("tve_ul1 tve_ul2 tve_ul3 tve_ul4 tve_ul5").addClass(bullet_style);
}

function tve_change_shortcode_alignment(alignment, element) {
    element_type = get_shortcode_type_from_edit_mode(element);
    switch (element_type) {
        case "button":
            element.removeClass("tve_leftBtn tve_centerBtn tve_rightBtn tve_fullwidthBtn").addClass(alignment)
            break;
        case 'content_container':
            element.find('.tve_content_inner').removeClass('tve_left tve_right tve_center').addClass(alignment.replace(/CC/, ''));
            break;
    }
}

function tve_change_shortcode_size(size, element) {
    element_type = get_shortcode_type_from_edit_mode(element);
    switch (element_type) {
        case "button":
            button = get_element_from_wrapper(element, element_type);
            button.removeClass("tve_smallBtn tve_normalBtn tve_bigBtn tve_hugeBtn").addClass(size);
    }
}

//changes the style of any shortcode element
function tve_change_shortcode_style(style, element) {
    element_type = get_shortcode_type_from_edit_mode(element);
    switch (element_type) {
        case "button" :
            button = get_element_from_wrapper(element, element_type);
            button.removeClass("tve_btn1 tve_btn2 tve_btn3 tve_btn4 tve_btn5 tve_btn6 tve_btn7 tve_btn8").addClass(style);
    }
}

/**
 * changes the colour of any shortcode element
 * also, it can be used to change the background color (highlights) of selections / text elements
 *
 * it's called in the current <li>'s context (this = jQuery(li.tve_{color}))
 */
function tve_change_shortcode_colour(colour, element) {
    element_type = get_shortcode_type_from_edit_mode(element);
    if (element_type == "text") {
        var _mode = this.parents('.tve_highlight_control').length ? 'background' : 'foreground';
        tve_change_colour(this, _mode);
    } else {
        shortcode = get_element_from_wrapper(element, element_type);
        shortcode.removeClass(colours_class_selector).addClass(colour);
        tve_remove_custom_colours(element);
    }
    window.last_colour_used = colour;
}

// returns the element held within the edit_mode wrapper used to add and remove classes for styling
function get_element_from_wrapper(element, element_type) {
    switch (element_type) {
        case "button" :
            return jQuery(element).find(".tve_btn");
            break;
        case "contentbox" :
            return jQuery(element).find(".tve_cb");
            break;
        case "guarantee" :
            return jQuery(element).find(".tve_fg");
            break;
        case "calltoaction" :
            return jQuery(element).find(".tve_ca, .tve_btn");
            break;
        case "testimonial" :
            return jQuery(element).find(".tve_ts");
            break;
        case "bullets" :
            return jQuery(element).find(".tve_ul");
            break;
        case "tabs" :
            return jQuery(element).find(".tve_scT");
            break;
        case "toggle" :
            return element;
            break;
        case "pricingtable" :
            return element;
            break;
    }
}

// determines the type of shortcode by looking at the DOM
function get_shortcode_type_from_edit_mode(element) {
    if (element.hasClass("thrv_button_shortcode")) {
        return "button";
    } else if (element.hasClass("thrv_contentbox_shortcode")) {
        return "contentbox";
    } else if (element.hasClass("thrv_guarantee_shortcode")) {
        return "guarantee";
    } else if (element.hasClass("thrv_calltoaction_shortcode")) {
        return "calltoaction";
    } else if (element.hasClass("thrv_testimonial_shortcode")) {
        return "testimonial";
    } else if (element.hasClass("thrv_bullets_shortcode")) {
        return "bullets";
    } else if (element.hasClass("thrv_tabs_shortcode")) {
        return "tabs";
    } else if (element.hasClass("thrv_toggle_shortcode")) {
        return "toggle";
    } else if (element.hasClass("tve_prt")) {
        return "pricingtable";
    } else if (element.hasClass('thrv_content_container_shortcode')) {
        return 'content_container';
    } else if (element.is(text_elements)) {
        return "text";
    }
}

function copy_attributes_to_new_element(new_element, attributes) {
    jQuery.each(attributes, function () {
        new_element.attr(this.name, this.value);
    });
}

function tve_wrap_element_tags(element, open_tag, close_tag) {
    content = element.html();
    content = open_tag + content + close_tag;
    element.replaceWith(content);
}

// function to send ajax request to server in order to save the post content
function tve_save_post(update) {
    var tve_content = filter_content_pre_save();
    var tve_content_more = tve_transform_more_tags();
    var custom_css_rules = tve_extract_stylesheet_rules();
    var saved_custom_colours = window.tve_custom_colours;
    var tve_custom_css = tve_get_custom_css();
    remove_save_content();

    var data = {
        action: 'tve_save_post',
        tve_content: tve_content,
        tve_content_more: tve_content_more,
        post_id: tve_path_params.post_id,
        security: tve_path_params.tve_ajax_nonce,
        update: update,
        inline_rules: custom_css_rules,
        custom_colours: saved_custom_colours,
        tve_custom_css: tve_custom_css
    }
    console.log(tve_path_params.ajax_url);
    jQuery.post(tve_path_params.ajax_url, data, function (response) {
        if (update) {
            tve_add_notification("All changes saved!");
        }
    });
    tve_undoManager.clear();
}

// function to send ajax request to server in order change the style family of the current page
function tve_change_style_family(style_family) {

    tve_save_post(false);

    var data = {
        action: 'tve_change_style_family',
        style_family: style_family,
        post_id: tve_path_params.post_id,
        security: tve_path_params.tve_ajax_nonce
    }
    jQuery.post(tve_path_params.ajax_url, data, function (response) {
        jQuery("#tve_style_family-css").attr("href", tve_path_params.style_families[style_family]);
        tve_path_params.tve_loaded_stylesheet = style_family;
    });
}

// this loads the existing values into the input fields when the secondary menu is triggered
function load_button_values(element, type) {
    switch (type) {
        case "button" :
            jQuery("#buttonText").val(jQuery.trim(jQuery(element).find("a").text()));
            jQuery("#buttonLink").val(jQuery(element).find("a").attr("href"));
            if (jQuery(element).find(".tve_btnLink").attr("target") == "_blank") {
                jQuery("#btn_link_new_window").prop("checked", true);
            } else {
                jQuery("#btn_link_new_window").prop("checked", false);
            }
            if (jQuery(element).find(".tve_btnLink").attr("rel") == "nofollow") {
                jQuery("#btn_nofollow").prop("checked", true);
            } else {
                jQuery("#btn_nofollow").prop("checked", false);
            }
            if (jQuery(element).find(".thrv_cc_icons").length != 0) {
                jQuery("#button_cc_icons").prop("checked", true);
            } else {
                jQuery("#button_cc_icons").prop("checked", false);
            }
            handle_custom_classes(element);
            break;
        case "calltoaction" :
            // call to action button location
            jQuery("#call_to_action_url").val(jQuery(element).find("a.tve_btnLink").attr("href"));
            // open in new window checkbox?
            if (jQuery(element).find("a.tve_btnLink").attr("target") == "_blank") {
                jQuery("#cta_link_new_window").prop("checked", true);
            } else {
                jQuery("#cta_link_new_window").prop("checked", false);
            }

            if (jQuery(element).find("a.tve_btnLink").attr("rel") == "nofollow") {
                jQuery("#cta_nofollow").prop("checked", true);
            } else {
                jQuery("#cta_nofollow").prop("checked", false);
            }
            break;
        case "img" :
            element = jQuery(".edit_mode");
            var width = jQuery(element).css("width");
            if (width) {
                jQuery("#image_width_input").val(width);
                jQuery("#tve_img_size_slider").slider("value", parseInt(width));
            }
            jQuery("#img_alt_att").val(jQuery(element).attr("alt"));
            jQuery("#img_title_att").val(jQuery(element).attr("title"));
            handle_custom_classes(element);
            break;
        case "text" :
            jQuery("#element_id").val(jQuery(".edit_mode").attr("id"));
            handle_custom_classes(element);
            break;
        case "cc_icons" :
            jQuery(".cc_checkbox").prop("checked", false);
            jQuery(element).find(".tve_cc_logo:visible").each(function () {
                cc_type = jQuery(this).attr("class");
                cc_type_array = cc_type.split(" ");
                for (i = 0; i < cc_type_array.length; i++) {
                    if (cc_type_array[i] != "tve_cc_logo" && cc_type_array[i] != "tve_no_edit") {
                        card_type = cc_type_array[i].substring(4);
                        jQuery("#" + card_type).prop("checked", true);
                    }
                }
            });
            break;
        case "pricing_table" :
            tve_check_highlight_exists();
            break;
    }
}

// function to populate editor input field with custom classes (excluding those that are reserved for the editor)
function handle_custom_classes(element) {
    class_list = jQuery(element).attr("class");
    class_list_array = class_list.split(" ");
    var custom_class_list = new Array();
    for (i = 0; i < class_list_array.length; i++) {
        if (class_list_array[i].indexOf("thrv_") == -1 && class_list_array[i].indexOf("tve_") == -1 && thrv_custom_classes.indexOf(class_list_array[i]) == -1) {
            custom_class_list.push(class_list_array[i]);
        }
    }
    jQuery(".element_class").val(custom_class_list.join(" "));
}

function remove_save_content() {
    jQuery("#pre_save_filter_wrapper").html("");
}

function tve_transform_more_tags() {
    jQuery("#pre_save_filter_wrapper .tve_more_tag").replaceWith("<!--tvemore-->");
    return jQuery("#pre_save_filter_wrapper").html();
}

// clean up any code that was used for editing purposes on save
function filter_content_pre_save() {
    pre_save_wrapper = jQuery("#pre_save_filter_wrapper");
    pre_save_wrapper.html(jQuery("#tve_editor").html());
    jQuery("#pre_save_filter_wrapper .tve_faqC").css("display", "none");
    jQuery("#pre_save_filter_wrapper .tve_oFaq").removeClass("tve_oFaq");
    jQuery("#pre_save_filter_wrapper .tve_dropzone").remove();
    jQuery("#pre_save_filter_wrapper [contenteditable='true']").attr("contenteditable", "false");
    pre_save_wrapper.html(tve_multilineTrim(pre_save_wrapper.html()));
    return pre_save_wrapper.html();
}

// hack to try and counteract the flickering that occurs when using nested sortables
// remove wrapper class while dragging.
function remove_elements_while_dragging() {
    jQuery(".thrv_wrapper").addClass("ignore_during_draggable").removeClass("thrv_wrapper");
    refresh_sortables();
}

// hack to try and counteract the flickering that occurs when using nested sortables
// add wrapper class after finished dragging
function add_elements_after_dragging() {
    jQuery(".ignore_during_draggable").removeClass("ignore_during_draggable").addClass("thrv_wrapper");
    refresh_sortables();
}

function hide_sub_menu() {
    jQuery(".active_sub_menu").parent().css("top", "");
    jQuery(".active_sub_menu").hide();
    tve_remove_text_boundaries();
}

function tve_open_lightbox(file, small) {
    jQuery.post(tve_path_params.cpanel_dir + '/' + file + '.php',
        function (response) {
            load_lightbox_content(response, small);
            tve_populate_lightbox(file);
            show_lightbox();
        });
}

function load_lightbox_content(content, small) {
    jQuery(".tve_lightbox_content").html(content);
    // if small lightbox, then apply relevant class
    if (small) {
        jQuery(".tve_lightbox_frame, .tve_lightbox_overlay").addClass("tve_link_lightbox");
    } else {
        jQuery(".tve_lightbox_frame, .tve_lightbox_overlay").removeClass("tve_link_lightbox");
    }
}

function show_lightbox() {
    jQuery(".tve_lightbox_overlay").show();
    jQuery(".tve_lightbox_frame").show();
}

function hide_lightbox() {
    jQuery(".tve_lightbox_overlay").hide();
    jQuery(".tve_lightbox_frame").hide();
    jQuery(".tve_active_lightbox").removeClass("tve_active_lightbox");
}

// function to get all the options that the user has entered into the lightbox
function tve_get_lightbox_options() {
    user_options = new Object();
    jQuery(".tve_lightbox_content input, .tve_lightbox_content textarea").each(function () {
        if (jQuery(this).attr("type") == "checkbox") {
            checked = tve_is_checked(jQuery(this));
            user_options[jQuery(this).attr("name")] = checked;
        } else {
            user_options[jQuery(this).attr("name")] = jQuery(this).val();
        }
    });
    return user_options;
}


// function to add a class in the DOM that corresponds to the element tha triggered the lightbox
function tve_add_lightbox_marker(element) {
    if (jQuery(element).is(".thrv_wrapper")) {
        jQuery(element).addClass("tve_active_lightbox");
    } else {
        jQuery(element).parents(".thrv_wrapper").first().addClass("tve_active_lightbox");
    }
}

function tve_populate_lightbox(lightbox) {
    switch (lightbox) {
        case "lb_full_html" :
            page_content = filter_content_pre_save();
            jQuery("textarea[name='tve_full_html']").val(page_content);
            break;
        case "lb_image_link" :
            element = jQuery(".edit_mode");
            if (jQuery(element).parent().is("a")) {
                jQuery("#lb_link_location").val(jQuery(element).parent().attr("href"));
                if (jQuery(element).parent().attr("target") == "_blank") {
                    jQuery("#lb_link_target").prop("checked", true);
                }
                if (jQuery(element).parent().attr("rel") == "nofollow") {
                    jQuery("#lb_no_follow").prop("checked", true);
                }
            }
            break;
        case "lb_custom_html" :
            element = jQuery(".edit_mode");
            element.addClass("tve_active_lightbox");
            jQuery("#tve_custom_html").val(element.html());
            break;
        case 'lb_custom_css':
            jQuery('textarea[name=tve_custom_css]').val(tve_get_custom_css(true));
            break;
    }
}

// handle lightbox options depending on form that was displayed
function tve_handle_lightbox_options(user_options) {
    switch (user_options.tve_lb_type) {
        case "custom_html":
            target = jQuery(".tve_active_lightbox");
            jQuery(target).html(user_options.tve_custom_html);
            jQuery(target).removeClass("code_placeholder").addClass("thrv_custom_html_shortcode");
            break;
        case "tve_full_html":
            jQuery("#tve_editor").html(user_options.tve_full_html);
            break;
        case "tve_google_map":
            google_map_code = user_options.tve_embed_code;
            jQuery(".tve_active_lightbox").replaceWith("<div class='tve-flexible-container thrv_wrapper'>" + user_options.tve_embed_code + "</div>");
            break;
        case "text_link":
            tve_add_hyperlink(user_options.lb_link, user_options.lb_link_target, user_options.lb_link_name, user_options.lb_no_follow);
            break;
        case "image_link" :
            tve_add_image_hyperlink(user_options.lb_link, user_options.lb_link_target, user_options.lb_no_follow);
            break;
        case "user_template" :
            tve_add_user_template(jQuery("#template_name").val());
            break;
        case 'tve_custom_css':
            tve_add_custom_css(user_options.tve_custom_css);
            break;
    }
}

function tve_calculate_panel_position() {
    width = jQuery(window).width();
    console_width = 1050;
    jQuery("#tve_cpanel").css("left", (width - console_width) / 2 + "px");
}

// remove empty lines from html string
function tve_multilineTrim(htmlString) {
    if (htmlString == "undefined") {
        return false;
    }
    return htmlString.split("\n").map(jQuery.trim).filter(function (line) {
        return line != ""
    }).join("\n");
}

// function to get the starting node within a click
function getSelectionStartNode() {
    var node, selection;
    if (window.getSelection) { // FF3.6, Safari4, Chrome5 (DOM Standards)
        selection = getSelection();
        node = selection.anchorNode;
    }
    if (!node && document.selection) { // IE
        selection = document.selection
        var range = selection.getRangeAt ? selection.getRangeAt(0) : selection.createRange();
        node = range.commonAncestorContainer ? range.commonAncestorContainer :
            range.parentElement ? range.parentElement() : range.item(0);
    }
    if (node) {
        return (node.nodeName == "#text" ? node.parentNode : node);
    }
}

// adds / modifies hyperlinks on images
function tve_add_image_hyperlink(link, target, nofollow) {

    // get image
    element = jQuery(".edit_mode");

    if (target) {
        link_target = "_blank";
    } else {
        link_target = "_self";
    }

    if (nofollow) {
        no_follow = true;
    } else {
        no_follow = false;
    }

    // if image already linked
    if (jQuery(element).parent().is("a")) {
        jQuery(element).parent().attr("href", link);
        jQuery(element).parent().attr("target", link_target);
        if (nofollow) {
            jQuery(element).parent().attr("rel", "nofollow");
        } else {
            jQuery(element).parent().attr("rel", "");
        }
    }

    else {
        var anchor = document.createElement("a");
        anchor.href = link;
        if (target) {
            anchor.target = link_target;
        }
        if (nofollow) {
            jQuery(element).parent().attr("rel", "nofollow");
        } else {
            jQuery(element).parent().attr("rel", "");
        }
        jQuery(element).wrap(anchor);
    }
    jQuery(".tve_link_btns").show();
}

function tve_add_hyperlink(link, target, anchor_name, no_follow) {
    rangy.restoreSelection(window.linkSel);
    var anchor = document.createElement("a");
    anchor.href = link;
    if (target) {
        anchor.target = "_blank";
    }
    if (anchor_name) {
        anchor.name = anchor_name;
    }
    if (no_follow) {
        anchor.setAttribute("rel", "nofollow");
    }
    var sel = rangy.getSelection();
    // if nothing selected - insert placeholder anchor text
    if (!sel.toString().length) {
        anchor.innerHTML = "Click to replace anchor text";
    }
    tve_surroundSelectedText(anchor);
    rangy.removeMarkers(window.linkSel);
}

//function used to create hyperlinks
function tve_surroundSelectedText(element) {
    var sel, range;
    if (window.getSelection) {
        // IE9 and non-IE
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            element.appendChild(document.createTextNode(range.toString()));
            range.deleteContents();
            range.insertNode(element);

            // Preserve the selection
            range = range.cloneRange();
            range.setStartAfter(element);
            range.collapse(true);
            sel.removeAllRanges();
            sel.addRange(range);
        }
    } else if (document.selection && document.selection.type != "Control") {
        // IE < 9
        var selRange = document.selection.createRange();
        element.appendChild(document.createTextNode(selRange.text));
        selRange.pasteHTML(element.outerHTML);
    }
}

function tve_add_user_template(template_name) {

    var template_content = jQuery("#tve_editor").html();
    var custom_css_rules = tve_extract_stylesheet_rules();

    template_name = encodeURIComponent(template_name);

    var data = {
        action: 'tve_save_user_template',
        template_name: template_name,
        template_content: template_content,
        dataType: "json",
        security: tve_path_params.tve_ajax_nonce,
        custom_css: custom_css_rules
    }

    jQuery.post(tve_path_params.ajax_url, data, function (response) {
        if (!response.success) {
            tve_add_notification(response.error);
        } else {
            new_template_list_item = '<li id="tve_user_template" class="cp_draggable user_template_item tve_clearfix ui-draggable">' +
                '<span class="tve_left">' +
                decodeURIComponent(response.new_template_added) +
                '</span>' +
                '<span class="tve_icon tve_icon-51 remove_user_template tve_right"></span></li>';
            jQuery("#user_template_list").append(new_template_list_item);
            tve_add_notification("New template added!");
            tve_start_draggable();
        }
    }, "json");
}

function tve_load_user_template(template_name, target, drag) {

    template_name = encodeURIComponent(template_name);

    var data = {
        action: 'tve_load_user_template',
        template_name: template_name,
        security: tve_path_params.tve_ajax_nonce,
        dataType: "json"
    }

    jQuery.post(tve_path_params.ajax_url, data, function (response) {
        response = JSON.parse(response);
        if (drag) {
            jQuery(target).find(".user_template_item").replaceWith(response.html_code);
        } else {
            tve_add_element_on_click(response.html_code);
        }

        tve_convert_css_into_array(response.css_code);

        //jQuery.when(jQuery(".tve_custom_style").append(response.css_code)).then(function () {
        //    tve_clean_css_code();
        //});
    });
}

/**
 * Sends ajax request to remove user template from database and remove item from templates drop down list
 * @param template_name - name of the template to be removed
 * @param target - target of the click in the menu
 */
function tve_remove_user_template(template_name, target) {

    template_name = encodeURIComponent(template_name);

    var data = {
        action: 'tve_remove_user_template',
        template_name: template_name,
        security: tve_path_params.tve_ajax_nonce
    }

    jQuery.post(tve_path_params.ajax_url, data, function (response) {
        jQuery(target).parent().remove();
        tve_add_notification("User template deleted!");
    });
}

/**
 * Initialises the draggable script
 */
function tve_start_draggable() {
    jQuery(".cp_draggable").draggable({
        connectToSortable: "#tve_editor",
        helper: "clone",
        revert: "false",
        cancel: ".thrv_custom_html_shortcode",
        opacity: 0,
        scroll: true,
        start: function (event, ui) {
            jQuery(ui.helper[0]).text("");
            jQuery(ui.helper).hide();
            jQuery(ui.item).addClass("active_draggable");
            hide_sub_menu();
        }
    });
}

/**
 * Generic function to return if a checkbox is checked or not
 * @param checkbox - the selected checkbox
 * @returns {boolean}
 */
function tve_is_checked(checkbox) {
    if (jQuery(checkbox).prop("checked")) {
        return true;
    } else {
        return false;
    }
}

/**
 * sets the width of the credit cards wrapper to allow center positioning of credit cards
 * @param element - the selected credit card wrapper
 */
function tve_resize_cc_wrapper(element) {
    visible_cards = jQuery(element).find(".tve_cc_logo:visible").length;
    jQuery(element).find(".thrv_cc_wrapper").css("width", (visible_cards * 56) + "px");
}

/**
 * adds / removes credit card images to buttons
 * @param element - the selected button
 * @param action - "load" to add buttons, "remove" to remove the buttons
 */
function cc_icons_in_button(element, action) {
    if (action == "load") {
        var data = {
            colour: window.last_colour_used,
            cssdir: tve_path_params.editor_dir
        }
        jQuery.post(tve_path_params.shortcodes_dir + 'sc_cc_icons.php', data, function (response) {
            jQuery(element).append(response);
        });
    } else if (action == "remove") {
        jQuery(element).find(".thrv_cc_icons").remove();
    }
}

/**
 * changes the colour of a page section
 * @param element - the selected page section element
 * @param color - the color the classname of the color that it should be changed to
 */
function change_page_section_color(element, color) {
    for (var section_colour in tve_path_params.page_section_colours) {
        jQuery(element).removeClass(section_colour);
    }
    jQuery(element).addClass(color);
}

/**
 * function to see if the image that is currently selected contains a caption or not
 * @param element - the selected image
 * @returns {boolean}
 */
function check_image_caption(element) {
    if (jQuery(element).parents(".wp-caption").length != 0) {
        return true;
    } else {
        return false;
    }
}

/**
 * function to check if sidebar visible or not
 * @returns {boolean}
 */
function is_tve_cpanel_submenu_visible() {
    if (jQuery("#tve_cpanel").find(".tve_sub :visible").length != 0) {
        return true;
    } else {
        return false;
    }
}

/**
 * Change colour of cpanel sidebar called on click
 * @param color - "light" or "dark"
 */
function tve_menu_color(color) {
    if (color == "light") {
        jQuery('#tve_cpanel').removeClass('tve_is_dark');
        jQuery('.tve_cpanel_onpage').removeClass('tve_is_dark');
    } else {
        jQuery('#tve_cpanel').addClass('tve_is_dark');
        jQuery('.tve_cpanel_onpage').addClass('tve_is_dark');
    }
    hide_sub_menu();
    tve_save_cp_config("color", color);
}

/**
 * change side of control panel position
 * @param side "left" or "right"
 */
function tve_menu_position(side) {
    if (side == "right") {
        jQuery("#tve_cpanel").removeClass("tve_cpanelFlip");
        jQuery('.tve_wrap_inner').removeClass('tve_is_flipped');
    } else {
        jQuery("#tve_cpanel").addClass("tve_cpanelFlip");
        jQuery('.tve_wrap_inner').addClass('tve_is_flipped');
    }
    hide_sub_menu();
    tve_save_cp_config("position", side);
}

/**
 * saves control panel display configuration so that the UI is remember after each page load
 * @param attribute - the attribute to save.  E.g "position"
 * @param value - value of the attribute   E.g "left"
 */
function tve_save_cp_config(attribute, value) {
    var data = {
        action: 'tve_editor_display_config',
        attribute: attribute,
        value: value,
        security: tve_path_params.tve_ajax_nonce,
    }

    jQuery.post(tve_path_params.ajax_url, data, function (response) {
    });
}
/**
 * modifies the highlight position on the pricing table
 * @param element - the pricing table element selected with jQuery
 * @param string - whether to move the highlight to the left of the right depending on which button was clicked
 */
function tve_modify_pricing_highlight(element, direction) {

    /* calculate number of columns and position of current highlight */
    var num_of_cols = 0;
    var current_highlight_position = 0;
    jQuery(element).find(".tve_prt_col").each(function () {
        if (jQuery(this).is(".tve_hgh")) {
            current_highlight_position = num_of_cols;
        }
        num_of_cols++;
    });

    /* can't move a column that is at the end of the grid */
    if ((num_of_cols == current_highlight_position + 1 && direction == "right") ||
        current_highlight_position == 0 && direction == "left") {
        return false;
    }

    /* remove existing highlight */
    jQuery(element).find(".tve_hgh").removeClass("tve_hgh");

    /* get new column position.  Includes adjustment for eq selector starting at 0 instead of 1 */
    if (direction == "left") {
        var new_position = current_highlight_position - 1;
    } else {
        var new_position = current_highlight_position + 1;
    }

    /* add new highlight */
    jQuery(element).find(".tve_prt_col").eq(new_position).addClass("tve_hgh");
    return true;
}

/**
 * Simple function to check whether there is a highlight column in a pricing table and display the relevant control panel buttons
 * @returns {boolean}
 */
function tve_check_highlight_exists() {
    element = jQuery(".edit_mode");
    if (jQuery(element).find(".tve_hgh").length !== 0) {
        jQuery(".tve_remove_highlight").show();
        jQuery(".tve_add_highlight").hide();
    } else {
        jQuery(".tve_remove_highlight").hide();
        jQuery(".tve_add_highlight").show();
    }
}

/**
 * Simple function to change the colour or background colour of selected text to one of the default colours
 *
 * @param $color_li_element jQuery wrapper over the <li> containing the color
 * @param string mode if set and equal to 'background', sets the background colour, else, the foreground colour
 */
function tve_change_colour($color_li_element, mode) {
    if (typeof mode === 'undefined') {
        mode = 'foreground';
    }

    var rgb_colour = $color_li_element.find('a').css("background-color"),
        sel = rangy.getSelection(),
        _css = mode == 'background' ? 'backgroundColor' : 'color',
        _cmd = mode == 'background' ? 'BackColor' : 'ForeColor';

    if (!sel.toString().length) {
        jQuery(".edit_mode font").contents().unwrap();
        jQuery(".edit_mode").css(_css, rgbToHex(rgb_colour));
    } else {
        document.execCommand(_cmd, false, rgbToHex(rgb_colour));
        sel.removeAllRanges();
    }
}

/**
 * remove color formatting from text elements : either text color or background color
 *
 * @param string mode foreground or background
 */
function tve_remove_color(mode) {
    if (typeof mode === 'undefined') {
        mode = 'foreground';
    }

    var sel = rangy.getSelection(),
        _css = mode == 'background' ? 'backgroundColor' : 'color',
        _cmd = mode == 'background' ? 'BackColor' : 'ForeColor';

    if (!sel.toString().length) {
        jQuery(".edit_mode font").contents().unwrap();
        jQuery(".edit_mode").css(_css, "");
    } else {
        document.execCommand('removeFormat', false, _cmd);
        sel.removeAllRanges();
    }
}

/**
 * Convert RGB to hex
 * @param c
 * @returns {string}
 */
function rgbToHex(color) {
    if (color.substr(0, 1) === "#") {
        return color;
    }
    var nums = /(.*?)rgb\((\d+),\s*(\d+),\s*(\d+)\)/i.exec(color),
        r = parseInt(nums[2], 10).toString(16),
        g = parseInt(nums[3], 10).toString(16),
        b = parseInt(nums[4], 10).toString(16);
    return "#" + (
        (r.length == 1 ? "0" + r : r) +
            (g.length == 1 ? "0" + g : g) +
            (b.length == 1 ? "0" + b : b)
        );
}

function tve_text_colour_pickers() {
    wpColorPickerL10n = new Array();
    wpColorPickerL10n['clear'] = "Clear";
    wpColorPickerL10n['defaultString'] = "Save as Favourite Colour";
    wpColorPickerL10n['pick'] = "Text Colour";

    var text_colourpicker_options = {
        change: function (event, ui) {
            if (!window.textselection) {
                window.textselection = rangy.saveSelection();
            }
            rangy.restoreSelection(window.textselection);
            window.textselection = rangy.saveSelection();
            color = ui.color.toString();

            /* if the current event target is a child of .tve_highlight_control -> the property changed is the background color, not the color */
            var sel = rangy.getSelection(),
                isHighlightControl = jQuery(event.target).parents('.tve_highlight_control').length,
                _cmd = isHighlightControl ? 'BackColor' : 'ForeColor',
                _css = isHighlightControl ? 'backgroundColor' : 'color';

            if (sel.toString().length) {
                document.execCommand(_cmd, false, color);
                sel.removeAllRanges();
            } else {
                jQuery(".edit_mode").css(_css, color);
            }
        },
        hide: true,
        palettes: window.tve_custom_colours
    };

    jQuery(".text_colour_picker").each(function () {
        var $this = jQuery(this);
        wpColorPickerL10n['pick'] = $this.parents('.tve_highlight_control').length ? 'Highlight Colour' : 'Text Colour';
        $this.wpColorPicker(text_colourpicker_options)
    });
}

/**
 * Remove rangy text selection boundaries after colour picker
 */
function tve_remove_text_boundaries() {
    if (window.textselection) {
        rangy.removeMarkers(window.textselection);
        window.textselection = null;
    }
}

/**
 * Check that sub menu doesn't disappear off the top of the screen.  If it does, then reposition so that it's a drop down menu instead.
 * @param menu
 * @returns {boolean}
 */
function tve_check_submenu_position(menu) {
    menu_pos = jQuery(menu).offset();
    if (menu_pos == undefined) {
        return false;
    }
    if (menu_pos.top < 30) {
        jQuery(menu).parent().css("top", "0px");
    }
}

/**
 * initialize all sliders required in the controls section
 * slider's min, max, target input values are taken directly from the html attributes of each slider node (.tve_slider_config)
 */
function tve_init_sliders() {
    jQuery('.tve_slider_config').each (function() {
        var $this = jQuery(this),
            $slider = $this.find('.tve_slider_element'),
            _min = parseInt($this.attr('data-min-value')),
            _val = $this.attr('data-value'),
            _max = $this.attr('data-max-value'),
            _cssProp = $this.attr('data-property') ? $this.attr('data-property') : 'width',
            _child = $this.attr('data-selector') ? $this.attr('data-selector') : false,
            $targetInput = jQuery('#' + $this.attr('data-related-input-id'));

        if (_max === 'available') { //take the maximum available width (parent / container width)
            _max = jQuery('#tve_editor').width();
        }

        $slider.slider({
            value: _val,
            min: _min,
            max: _max,
            slide: function(event, ui) {
                var width = ui.value + "px",
                    element = (_child ? jQuery(".edit_mode").find(_child) : jQuery('.edit_mode')).css(_cssProp, width);

                $targetInput.val(width);
                if (check_image_caption(element)) {//just in case this is for the image element
                    element.parents(".wp-caption").first().css(_cssProp, width);
                }
            }
        });

        $targetInput.change(function() {
            var width = $targetInput.val(),
                element = (_child ? jQuery(".edit_mode").find(_child) : jQuery('.edit_mode')).css(_cssProp, width);

            if (check_image_caption(element)) {//just in case this is for the image element
                element.parents(".wp-caption").first().css(_cssProp, width);
            }

            $slider.slider("value", parseInt(width))
        }).change();
    });
}

/**
 * triggered after the an element has been inserted to DOM (either by drag or by click)
 *
 * @param $insertedElement jQuery wrapper over the inserted HTML
 * @param drag whether or not the element was inserted via drag & drop operation
 */
function tve_insert_element_after($insertedElement, drag) {
    if ($insertedElement.hasClass('thrv_content_container_shortcode')) {
        $insertedElement.parent().addClass('tve_clearfix');
    }
}

/**
 * append a custom css style to the current page. if it aleardy exists, replace it
 * @param string cssString
 */
function tve_add_custom_css(cssString) {
    var $_style = jQuery('style#tve_head_custom_css');
    if ($_style.length === 0) {
        $_style = jQuery('<style type="text/css" id="tve_head_custom_css"></style>').appendTo(jQuery('head'));
    }
    $_style.text(cssString);
}

/**
 * get the current custom css string, if any
 *
 * @param bool returnHint if set, it will return a hint for the user
 */
function tve_get_custom_css(returnHint) {
    var $_style = jQuery('style#tve_head_custom_css');
    if ($_style.length === 0) {
        return (typeof returnHint !== 'undefined' && returnHint) ? '/** Insert your custom CSS rules here. **/' : '';
    }
    return $_style.text();
}