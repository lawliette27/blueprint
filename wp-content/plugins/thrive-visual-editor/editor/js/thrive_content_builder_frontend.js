jQuery(document).ready(function () {
    jQuery("#tve_editor").on("click", ".tve_scT ul li", function (e) {
        tabs_wrapper = jQuery(this).parents(".thrv_tabs_shortcode").first();
        target_tab = jQuery(this).find("span").attr("class");
        active_tab = jQuery(tabs_wrapper).find(".tve_tS");
        active_tab_custom_colour = jQuery(active_tab).attr("data-tve-custom-colour");
        target_tab_custom_colour = jQuery(this).attr("data-tve-custom-colour");
        /** custom colour exists for currently active tab **/
        if (typeof active_tab_custom_colour !== 'undefined' && active_tab_custom_colour !== false) {
            jQuery(this).attr("data-tve-custom-colour", active_tab_custom_colour);
        } else {
            jQuery(this).attr("data-tve-custom-colour", "");
        }
        if (typeof target_tab_custom_colour !== 'undefined' && target_tab_custom_colour !== false) {
            jQuery(active_tab).attr("data-tve-custom-colour", target_tab_custom_colour);
        } else {
            jQuery(active_tab).attr("data-tve-custom-colour", "");
        }
        jQuery(tabs_wrapper).find(".tve_tS").removeClass("tve_tS");
        jQuery(this).addClass('tve_tS');
        jQuery(tabs_wrapper).find(".tve_scTC").hide().filter('.' + target_tab).show();
        e.preventDefault();
    });
    jQuery("#tve_editor").on("click", ".tve_faqB h4, .tve_faqB span", function () {
        toggle_wrap = jQuery(this).parents(".tve_faqB");
        toggle_content = jQuery(toggle_wrap).siblings(".tve_faqC");
        toggle_indicator = jQuery(toggle_wrap).find(".tve_toggle");

        if (jQuery(toggle_content).is(":visible")) {
            jQuery(toggle_content).slideUp("fast");
            jQuery(toggle_indicator).removeClass("tve_oFaq");
        } else {
            jQuery(toggle_content).slideDown("fast");
            jQuery(toggle_indicator).addClass("tve_oFaq");
        }

    });
});

function tve_resize_tabs() {
    jQuery('.tve_scT ul').each(function () {
        var noOfLi = jQuery(this).children('li').length;
        jQuery(this).children('li').css('width', 100 / noOfLi + '%');
    });
}

function show_first_tab(tabs_wrapper) {
    target_tab = jQuery(tabs_wrapper).find("span").first().attr("class");
    jQuery(tabs_wrapper).find(".tve_tS").removeClass("tve_tS");
    jQuery(tabs_wrapper).find("ul li").first().addClass('tve_tS');
    jQuery(tabs_wrapper).find(".tve_scTC").hide();
    jQuery(tabs_wrapper).find(".tve_scTC").first().show();
}

