<?php if (isset($_POST['is_thrive_theme']) && $_POST['is_thrive_theme'] == 1): $is_thrive_theme = true;
else: $is_thrive_theme = false; endif; ?>
<div class="tve_wrapper
    <?php echo ($_POST['display_options']['position'] == 'left') ? "tve_cpanelFlip" : ""; ?>
    <?php echo ($_POST['display_options']['color'] == 'dark') ? "tve_is_dark" : ""; ?>"
     id="tve_cpanel">
<div class="tve_editor">
<div class="tve_cpanel_sec tve_control_btns">
    <div class="tve_btn_success tve_left" title="Save">
        <div class="tve_update" title="Save" id="tve_update_content">
            <span class="tve_expanded">Save Changes</span>
            <span class="tve_collapsed tve_icon tve_icon-52"></span>
        </div>
    </div>
    <div class="tve_btn_default tve_expanded tve_left" title="Publish">
        <a class="tve_preview" title="Publish" id="tve_preview_content" target="_blank"
           href="<?php echo $_POST['preview_url']; ?>">
            <span class="">Preview</span>
        </a>
    </div>
    <div class="tve_clear"></div>
</div>
<div class="tve_cpanel_sec">
    <?php /*
    echo (isset($_POST['tve_api_data']['dropdown_lists']['main_left'])) ? $_POST['tve_api_data']['dropdown_lists']['main_left'] : "";
    echo (isset($_POST['tve_api_data']['menu_items']['main_left'])) ? $_POST['tve_api_data']['menu_items']['main_left'] : "";
    */
    ?>
    <div class="tve_ed_btn tve_btn_icon tve_left">
        <div class="tve_icon tve_icon-25" id="tve_undo_manager" title="Undo last action"></div>
    </div>
    <div class="tve_ed_btn tve_btn_icon tve_left tve_expanded">
        <div class="tve_icon tve_icon-26" id="tve_redo_manager" title="Redo last action"></div>
    </div>
    <div class="tve_ed_btn tve_btn_icon tve_left" title="HTML">
        <div class="tve_icon tve_icon-20 tve_lb_open" id="lb_full_html" title="HTML"></div>
    </div>
    <div class="tve_option_separator tve_dropdown_submenu tve_drop_temp tve_left">
        <div class="tve_ed_btn tve_btn_text" title="Templates">
            <span class="tve_expanded">Templates</span>
            <span class="tve_collapsed tve_icon tve_icon-50"></span>
        </div>
        <div class="tve_sub_btn">
            <div class="tve_sub active_sub_menu">
                <ul>
                    <li>
                        <div class="tve_lb_open tve_lb_small" id="lb_save_user_template"
                             title="Save as Template">Save as Template
                        </div>
                    </li>
                    <li class="tve_list_separator"></li>
                    <li class="tve_ld_tml">
                        <?php if (isset($_POST['user_templates']) && is_array($_POST['user_templates'])): ?>
                            <span>Load Template</span>
                        <?php endif; ?>
                        <ul id="user_template_list">
                            <?php if (isset($_POST['user_templates']) && is_array($_POST['user_templates'])): ?>
                                <?php foreach ($_POST['user_templates'] as $user_template_id => $template_name): ?>
                                    <li id="tve_user_template" class="cp_draggable user_template_item tve_clearfix">
                                        <span
                                            class="tve_left"><?php echo urldecode(stripslashes($template_name)); ?></span>
                                        <span class="tve_icon tve_icon-51 remove_user_template tve_right"></span>
                                    </li>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <!--<li class="tve_list_separator"></li>
                    <li>Thrive Templates</li>-->
                </ul>
            </div>
        </div>
    </div>
    <!--"bottom_right" api position - load API data if exists-->
    <?php /*
    echo (isset($_POST['tve_api_data']['dropdown_lists']['main_extended_right'])) ? $_POST['tve_api_data']['dropdown_lists']['main_extended_right'] : "";
    echo (isset($_POST['tve_api_data']['menu_items']['main_extended_right'])) ? $_POST['tve_api_data']['menu_items']['main_extended_right'] : "";
    */
    ?>
    <div class="tve_clear"></div>
</div>
<div class="tve_cpanel_options">
<div class="tve_cpanel_sec tve_cpanel_sep">
    <span class="tve_cpanel_head tve_expanded">Simple Content Elements</span>
</div>
<div class="tve_cpanel_list">
    <div class="cp_draggable paragraph tve_option_separator tve_clearfix" title="Text">
        <div class="tve_icon tve_icon-1 tve_left" title="Text"></div>
        <span class="tve_expanded tve_left">Paragraph/Text Element</span>
    </div>
    <div class="cp_draggable thrv_image tve_option_separator tve_clearfix" title="Image">
        <div class="tve_icon tve_icon-3 tve_left" title="Image"></div>
        <span class="tve_expanded tve_left">Image</span>
    </div>
    <div class="cp_draggable sc_buttons1_classy tve_option_separator tve_clearfix" title="Buttons">
        <div class="tve_icon tve_icon-4 tve_left" title="Button"></div>
        <span class="tve_expanded tve_left">Button</span>
    </div>
    <div class="cp_draggable sc_cc_icons tve_option_separator tve_clearfix" title="Add Credit Card Icons">
        <div class="tve_icon tve_icon-27 tve_left" id="cc_icons" title="Add Credit Card Icons"></div>
        <span class="tve_expanded tve_left">Credit Card Icons</span>
    </div>
    <div class="cp_draggable sc_custom_html tve_option_separator tve_clearfix" title="Custom HTML">
        <div class="tve_icon tve_icon-28 tve_code tve_left" id=""></div>
        <span class="tve_expanded tve_left">Custom HTML</span>
    </div>
    <div class="tve_lb_open sc_custom_css tve_option_separator tve_clearfix" title="Custom CSS" id="lb_custom_css">
        <div class="tve_icon tve_icon-60 tve_code tve_left" id=""></div>
        <span class="tve_expanded tve_left">Custom CSS</span>
    </div>
    <div class="cp_draggable sc_content_container tve_clearfix" title="Custom HTML">
        <div class="tve_icon tve_icon-59 tve_left" id=""></div>
        <span class="tve_expanded tve_left">Content Container</span>
    </div>
</div>
<div class="tve_cpanel_sec tve_cpanel_sep">
    <span class="tve_cpanel_head tve_expanded">Multi-Style Elements</span>
</div>
<div class="tve_cpanel_list">
<div class="tve_option_separator tve_clearfix" title="Columns">
    <div class="tve_icon tve_icon-2 tve_left"></div>
    <span class="tve_expanded tve_left">Column Layout</span>
        <span class="tve_caret_top tve_right tve_sub_btn tve_sub_btn_caret tve_expanded"
              id="sub_02"></span>

    <div class="tve_clear"></div>
    <div class="tve_sub_btn" title="Columns">
        <div class="tve_sub">
            <ul class="tve_columns_menu">
                <li class="cp_draggable standard_halfs tve_nohighlight">
                    <div class="tve_colm tve_twc"><p>1/2</p></div>
                    <div class="tve_colm tve_twc tve_lst"><p>1/2</p></div>
                    <div class="clear"></div>
                </li>
                <li class="cp_draggable standard_thirds">
                    <div class="tve_colm tve_oth"><p>1/3</p></div>
                    <div class="tve_colm tve_oth"><p>1/3</p></div>
                    <div class="tve_colm tve_thc tve_lst"><p>1/3</p></div>
                    <div class="clear"></div>
                </li>
                <li class="cp_draggable standard_fourths">
                    <div class="tve_colm tve_foc"><p>1/4</p></div>
                    <div class="tve_colm tve_foc"><p>1/4</p></div>
                    <div class="tve_colm tve_foc"><p>1/4</p></div>
                    <div class="tve_colm tve_foc tve_lst"><p>1/4</p></div>
                    <div class="clear"></div>
                </li>
                <li class="cp_draggable standard_fifths">
                    <div class="tve_colm tve_fic"><p>1/5</p></div>
                    <div class="tve_colm tve_fic"><p>1/5</p></div>
                    <div class="tve_colm tve_fic"><p>1/5</p></div>
                    <div class="tve_colm tve_fic"><p>1/5</p></div>
                    <div class="tve_colm tve_fic tve_lst"><p>1/5</p></div>
                    <div class="clear"></div>
                </li>
                <li class="cp_draggable standard_thirds_one_two">
                    <div class="tve_colm tve_oth"><p>1/3</p></div>
                    <div class="tve_colm tve_tth tve_lst"><p>2/3</p></div>
                    <div class="clear"></div>
                </li>
                <li class="cp_draggable standard_thirds_two_one">
                    <div class="tve_colm tve_tth"><p>2/3</p></div>
                    <div class="tve_colm tve_oth tve_lst"><p>1/3</p></div>
                    <div class="clear"></div>
                </li>
                <li class="cp_draggable standard_fourths_one_three">
                    <div class="tve_colm tve_foc"><p>1/4</p></div>
                    <div class="tve_colm tve_tfo tve_lst"><p>3/4</p></div>
                    <div class="clear"></div>
                </li>
                <li class="cp_draggable standard_fourths_three_one">
                    <div class="tve_colm tve_tfo"><p>3/4</p></div>
                    <div class="tve_colm  tve_foc tve_lst"><p>1/4</p></div>
                    <div class="clear"></div>
                </li>
                <li class="cp_draggable standard_two_fourths_half">
                    <div class="tve_colm tve_foc"><p>1/4</p></div>
                    <div class="tve_colm tve_foc"><p>1/4</p></div>
                    <div class="tve_colm tve_twc tve_lst"><p>1/2</p></div>
                    <div class="clear"></div>
                </li>
                <li class="cp_draggable standard_fourth_half_fourth">
                    <div class="tve_colm tve_foc"><p>1/4</p></div>
                    <div class="tve_colm tve_twc"><p>1/2</p></div>
                    <div class="tve_colm tve_foc tve_lst"><p>1/4</p></div>
                    <div class="clear"></div>
                </li>
                <li class="cp_draggable standard_half_fourth_fourth">
                    <div class="tve_colm tve_twc"><p>1/2</p></div>
                    <div class="tve_colm tve_foc"><p>1/4</p></div>
                    <div class="tve_colm tve_foc tve_lst"><p>1/4</p></div>
                    <div class="clear"></div>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="tve_option_separator tve_clearfix" title="Content Box">
    <div class="tve_icon tve_icon-5 tve_left"></div>
    <span class="tve_expanded tve_left">Content Box</span>
    <span class="tve_caret_top tve_right tve_sub_btn tve_sub_btn_caret tve_expanded"
          id="sub_02"></span>

    <div class="tve_clear"></div>
    <div class="tve_sub_btn" title="Content Box">
        <div class="tve_sub">
            <ul>
                <li class="tve_sub_heading">With Headline</li>
                <li class="cp_draggable sc_contentbox1"><span class="tve_icon tve_icon-24"></span>Style 1</li>
                <li class="cp_draggable sc_contentbox2"><span class="tve_icon tve_icon-24"></span>Style 2</li>
                <li class="cp_draggable sc_contentbox3"><span class="tve_icon tve_icon-24"></span>Style 3</li>
                <li class="tve_sub_heading">Without Headline</li>
                <li class="cp_draggable sc_contentbox4"><span class="tve_icon tve_icon-24"></span>Style 4</li>
                <li class="cp_draggable sc_contentbox5"><span class="tve_icon tve_icon-24"></span>Style 5</li>
                <li class="cp_draggable sc_contentbox6"><span class="tve_icon tve_icon-24"></span>Style 6</li>
            </ul>
        </div>
    </div>
</div>
<div class="tve_option_separator tve_clearfix" title="Bullet Points">
    <div class="tve_icon tve_icon-10 tve_left"></div>
    <span class="tve_expanded tve_left">Styled List</span>
    <span class="tve_caret_top tve_right tve_sub_btn tve_sub_btn_caret tve_expanded"
          id="sub_02"></span>

    <div class="tve_clear"></div>
    <div class="tve_sub_btn" title="Bullet Points">
        <div class="tve_sub">
            <ul>
                <li class="cp_draggable sc_bullets1 tve_clearfix">
                    <span class="tve_icon tve_icon-24 tve_left"></span>
                    <span class="tve_left">Style 1</span>
                    <span class="tve_icon tve_icon-44 tve_icon-list tve_left"></span>
                </li>
                <li class="cp_draggable sc_bullets2 tve_clearfix">
                    <span class="tve_icon tve_icon-24 tve_left"></span>
                    <span class="tve_left">Style 2</span>
                    <span class="tve_icon tve_icon-45 tve_icon-list tve_left"></span>
                </li>
                <li class="cp_draggable sc_bullets3 tve_clearfix">
                    <span class="tve_icon tve_icon-24 tve_left"></span>
                    <span class="tve_left">Style 3</span>
                    <span class="tve_icon tve_icon-46 tve_icon-list tve_left"></span>
                </li>
                <li class="cp_draggable sc_bullets4 tve_clearfix">
                    <span class="tve_icon tve_icon-24 tve_left"></span>
                    <span class="tve_left">Style 4</span>
                    <span class="tve_icon tve_icon-47 tve_icon-list tve_left"></span>
                </li>
                <li class="cp_draggable sc_bullets5 tve_clearfix">
                    <span class="tve_icon tve_icon-24 tve_left"></span>
                    <span class="tve_left">Style 5</span>
                    <span class="tve_icon tve_icon-48 tve_icon-list tve_left"></span>
                </li>
                <li class="cp_draggable sc_bullets6 tve_clearfix">
                    <span class="tve_icon tve_icon-24 tve_left"></span>
                    <span class="tve_left">Style 6</span>
                    <span class="tve_icon tve_icon-49 tve_icon-list tve_left"></span>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="tve_option_separator tve_clearfix" title="Divider">
    <div class="tve_icon tve_icon-9 tve_left"></div>
    <span class="tve_expanded tve_left">Divider</span>
    <span class="tve_caret_top tve_right tve_sub_btn tve_sub_btn_caret tve_expanded"
          id="sub_02"></span>

    <div class="tve_clear"></div>
    <div class="tve_sub_btn" title="Divider">
        <div class="tve_sub">
            <ul>
                <li class="cp_draggable sc_divider1">
                    <span class="tve_icon tve_icon-24"></span>Style 1
                </li>
                <li class="cp_draggable sc_divider2">
                    <span class="tve_icon tve_icon-24"></span>Style 2
                </li>
                <li class="cp_draggable sc_divider3">
                    <span class="tve_icon tve_icon-24"></span>Style 3
                </li>
                <li class="cp_draggable sc_divider4">
                    <span class="tve_icon tve_icon-24"></span>Style 4
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="tve_option_separator tve_clearfix" title="Testimonials">
    <div class="tve_icon tve_icon-6 tve_left"></div>
    <span class="tve_expanded tve_left">Testimonial</span>
    <span class="tve_caret_top tve_right tve_sub_btn tve_sub_btn_caret tve_expanded"
          id="sub_02"></span>

    <div class="tve_clear"></div>
    <div class="tve_sub_btn" title="Testimonials">
        <div class="tve_sub">
            <ul>
                <li class="tve_sub_heading">Templates With Picture</li>
                <li class="cp_draggable sc_testimonial1">
                    <span class="tve_icon tve_icon-24"></span>Style 1
                </li>
                <li class="cp_draggable sc_testimonial2">
                    <span class="tve_icon tve_icon-24"></span>Style 2
                </li>
                <li class="cp_draggable sc_testimonial3">
                    <span class="tve_icon tve_icon-24"></span>Style 3
                </li>
                <li class="cp_draggable sc_testimonial4">
                    <span class="tve_icon tve_icon-24"></span>Style 4
                </li>
                <li class="cp_draggable sc_testimonial9">
                    <span class="tve_icon tve_icon-24"></span>Style 5
                </li>
                <li class="tve_sub_heading">Templates Without Picture</li>
                <li class="cp_draggable sc_testimonial5">
                    <span class="tve_icon tve_icon-24"></span>Style 6
                </li>
                <li class="cp_draggable sc_testimonial6">
                    <span class="tve_icon tve_icon-24"></span>Style 7
                </li>
                <li class="cp_draggable sc_testimonial7">
                    <span class="tve_icon tve_icon-24"></span>Style 8
                </li>
                <li class="cp_draggable sc_testimonial8">
                    <span class="tve_icon tve_icon-24"></span>Style 9
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="tve_option_separator tve_clearfix" title="Call to Action">
    <div class="tve_icon tve_icon-8 tve_left"></div>
    <span class="tve_expanded tve_left">Call to Action</span>
    <span class="tve_caret_top tve_right tve_sub_btn tve_sub_btn_caret tve_expanded"
          id="sub_02"></span>

    <div class="tve_clear"></div>
    <div class="tve_sub_btn" title="Call to Action">
        <div class="tve_sub">
            <ul>
                <li class="cp_draggable sc_calltoaction1">
                    <span class="tve_icon tve_icon-24"></span>Style 1
                </li>
                <li class="cp_draggable sc_calltoaction2">
                    <span class="tve_icon tve_icon-24"></span>Style 2
                </li>
                <li class="cp_draggable sc_calltoaction3">
                    <span class="tve_icon tve_icon-24"></span>Style 3
                </li>
                <li class="cp_draggable sc_calltoaction4">
                    <span class="tve_icon tve_icon-24"></span>Style 4
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="tve_option_separator tve_clearfix" title="Guarantee">
    <div class="tve_icon tve_icon-7 tve_left"></div>
    <span class="tve_expanded tve_left">Guarantee Box</span>
    <span class="tve_caret_top tve_right tve_sub_btn tve_sub_btn_caret tve_expanded"
          id="sub_02"></span>

    <div class="tve_clear"></div>
    <div class="tve_sub_btn" title="Guarantee">
        <div class="tve_sub">
            <ul>
                <li class="cp_draggable sc_guarantee1">
                    <span class="tve_icon tve_icon-24"></span>Style 1
                </li>
                <li class="cp_draggable sc_guarantee2">
                    <span class="tve_icon tve_icon-24"></span>Style 2
                </li>
                <li class="cp_draggable sc_guarantee3">
                    <span class="tve_icon tve_icon-24"></span>Style 3
                </li>
                <li class="cp_draggable sc_guarantee4">
                    <span class="tve_icon tve_icon-24"></span>Style 4
                </li>
            </ul>
        </div>
    </div>
</div>
</div>
<div class="tve_cpanel_sec tve_cpanel_sep">
    <span class="tve_cpanel_head tve_expanded">Advanced Elements</span>
</div>
<div class="tve_cpanel_list">
    <div class="tve_grid tve_option_separator tve_clearfix" title="Pricing Table">
        <span class="tve_icon tve_icon-55 tve_left"></span>
        <span class="tve_expanded tve_left">Pricing Table</span>
        <span class="tve_caret_top tve_sub_btn tve_sub_btn_caret tve_right tve_expanded" id="sub_02"></span>

        <div class="tve_clear"></div>
        <div class="tve_sub_btn" title="Pricing Table">
            <div class="tve_sub">
                <ul>
                    <li class="cp_draggable sc_pricing_table_1col" title="1 Columns">
                        <span class="tve_icon tve_icon-24"></span>1 Column
                    </li>
                    <li class="cp_draggable sc_pricing_table_2col" title="2 Columns">
                        <span class="tve_icon tve_icon-24"></span>2 Columns
                    </li>
                    <li class="cp_draggable sc_pricing_table_3col" title="3 Columns">
                        <span class="tve_icon tve_icon-24"></span>3 Columns
                    </li>
                    <li class="cp_draggable sc_pricing_table_4col" title="4 Columns">
                        <span class="tve_icon tve_icon-24"></span>4 Columns
                    </li>
                    <li class="cp_draggable sc_pricing_table_5col" title="5 Columns">
                        <span class="tve_icon tve_icon-24"></span>5 Columns
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="cp_draggable sc_tabs tve_option_separator tve_clearfix" title="Tabs">
        <div class="tve_icon tve_icon-11 tve_left" title="Tabs"></div>
        <span class="tve_expanded tve_left">Tabbed Content</span>
    </div>
    <?php /*
        echo (isset($_POST['tve_api_data']['dropdown_lists']['main_right'])) ? $_POST['tve_api_data']['dropdown_lists']['main_right'] : "";
        echo (isset($_POST['tve_api_data']['menu_items']['main_right'])) ? $_POST['tve_api_data']['menu_items']['main_right'] : "";
    */
    ?>
    <div class="tve_grid tve_option_separator tve_clearfix" title="Feature Grids">
        <span class="tve_icon tve_icon-29 tve_left"></span>
        <span class="tve_expanded tve_left">Feature Grid</span>
        <span class="tve_caret_top tve_sub_btn tve_sub_btn_caret tve_right tve_expanded" id="sub_02"></span>

        <div class="tve_clear"></div>
        <div class="tve_sub_btn" title="Feature Grids">
            <div class="tve_sub">
                <ul>
                    <li class="cp_draggable sc_feature_grid_2_column"
                        title="2 Column Feature Grid">
                        <span class="tve_icon tve_icon-24"></span>2 Column Feature Grid
                    </li>
                    <li class="cp_draggable sc_feature_grid_3_column"
                        title="3 Column Feature Grid">
                        <span class="tve_icon tve_icon-24"></span>3 Column Feature Grid
                    </li>
                    <li class="cp_draggable sc_feature_grid_4_column"
                        title="4 Column Feature Grid">
                        <span class="tve_icon tve_icon-24"></span>4 Column Feature Grid
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <?php /*
        echo (isset($_POST['tve_api_data']['dropdown_lists']['main_extended_left'])) ? $_POST['tve_api_data']['dropdown_lists']['main_extended_left'] : "";
        echo (isset($_POST['tve_api_data']['menu_items']['main_extended_left'])) ? $_POST['tve_api_data']['menu_items']['main_extended_left'] : "";
    */
    ?>
    <div class="cp_draggable sc_toggle tve_option_separator tve_clearfix" title="Toggle">
        <div class="tve_icon tve_icon-13 tve_left" title="Toggle"></div>
        <span class="tve_expanded tve_left">Content Toggle</span>
    </div>
    <div class="cp_draggable sc_gmap tve_option_separator tve_clearfix" title="Google Map">
        <div class="tve_icon tve_icon-12 tve_left" title="Google Map"></div>
        <span class="tve_expanded tve_left">Google Map Embed</span>
    </div>
</div>
<div class="tve_cpanel_sec tve_cpanel_sep">
    <span class="tve_cpanel_head tve_expanded">
        Thrive Theme Elements
    </span>
</div>
<div class="tve_cpanel_list">
    <?php if ($is_thrive_theme): ?>
        <div class="tve_option_separator tve_clearfix" title="Feature Grids">
            <div class="tve_icon tve_icon-14 tve_left"></div>
            <span class="tve_expanded tve_left">Borderless Content</span>
        <span class="tve_caret_top tve_right tve_sub_btn tve_sub_btn_caret tve_expanded"
              id="sub_02"></span>

            <div class="tve_clear"></div>
            <div class="tve_sub_btn" title="Borderless Content">
                <div class="tve_sub active_sub_menu">
                    <ul>
                        <li class="cp_draggable sc_borderless_image ui-draggable" title="Borderless Image">
                            <span class="tve_icon tve_icon-24"></span>Borderless Image
                        </li>
                        <li class="cp_draggable sc_borderless_html ui-draggable" title="Borderless Video Embed">
                            <span class="tve_icon tve_icon-24"></span>Borderless Video Embed
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($is_thrive_theme && isset($_POST['page_section_colours']) && !empty($_POST['page_section_colours'])): ?>
        <div class="tve_option_separator tve_clearfix" title="Page Sections">
            <div class="tve_icon tve_icon-23 tve_left"></div>
            <span class="tve_expanded tve_left">Page Section</span>
        <span class="tve_caret_top tve_right tve_sub_btn tve_sub_btn_caret tve_expanded"
              id="sub_02"></span>

            <div class="tve_clear"></div>
            <div class="tve_sub_btn" title="Page Sections">
                <div class="tve_sub active_sub_menu">
                    <ul class="tve_default_colors tve_left">
                        <?php foreach ($_POST['page_section_colours'] as $colour_name => $colour_details): ?>
                            <li class="cp_draggable sc_page_section ui-draggable tve_clearfix" title="Page Section">
                                <span class="tve_icon tve_left tve_icon-24"></span>
                                <input type="hidden" id="<?php echo $colour_name; ?>"/>
                        <span class="tve_section_colour tve_left"
                              style="background:<?php echo $colour_details['color']; ?>"></span>
                                <span class="tve_left"><?php echo $colour_name; ?></span>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
</div>
<div class="tve_cpanel_sec">
    <div class="tve_option_separator tve_dropdown_submenu tve_drop_style tve_left">
        <div class="tve_ed_btn tve_btn_text">
            <div class="tve_icon tve_icon-31 tve_left tve_sub_btn" id="sub_02"></div>
            <span class="tve_left tve_expanded">Style Family</span>

            <div class="tve_clear"></div>
        </div>
        <div class="tve_sub_btn">
            <div class="tve_sub active_sub_menu">
                <ul>
                    <?php foreach ($_POST['style_families'] as $style_family_name => $style_family_location): ?>
                        <li id="tve_style_family_<?php echo strtolower($style_family_name); ?>"
                            class="tve_btn_style_family"><?php echo $style_family_name; ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="tve_option_separator tve_dropdown_submenu tve_drop_flip tve_left">
        <div class="tve_ed_btn tve_btn_icon">
            <div class="tve_icon tve_icon-53"></div>
            <div class="tve_sub_btn">
                <div class="tve_sub active_sub_menu">
                    <ul>
                        <li id="tve_flipEditor">Switch Editor Side</li>
                        <li id="tve_flipColor">Change Editor Color</li>
                    </ul>
                    <div class="tve_clear"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="tve_clear"></div>
</div>
<div class="tve_cpanel_sec tve_btn_collapse">
    <div class="tve_cpanel_head tve_clearfix" id="tve_collapse_editor_btn">
        <div class="tve_icon tve_icon-32 tve_left tve_expanded "></div>
        <div class="tve_icon tve_icon-33 tve_left tve_collapsed"></div>
        <span class="tve_left tve_expanded">Collapse Editor</span>
    </div>
    <a href="" class="tve_logo">
        <span class="tve_cpanel_logo" alt=""/></span>
    </a>

    <div class="tve_clear"></div>
</div>

</div>

</div>
<div class="tve_cpanel_onpage <?php echo ($_POST['display_options']['color'] == 'dark') ? "tve_is_dark" : ""; ?>">
<div class="tve_secondLayer">
<div id="text_menu">
    <span class="tve_options_headline">Element options</span>
    <ul class="tve_menu">
        <?php
        echo (isset($_POST['tve_api_data']['dropdown_lists']['text_menu_left'])) ? $_POST['tve_api_data']['dropdown_lists']['text_menu_left'] : "";
        echo (isset($_POST['tve_api_data']['menu_items']['text_menu_left'])) ? $_POST['tve_api_data']['menu_items']['text_menu_left'] : "";
        ?>
        <!--<li class="tve_btn"><span class="tve_awesome" id="tve_undo" title="undo last action">&#xf0e2;</span></li>
        <li class="tve_btn"><span class="tve_awesome" id="tve_redo" title="redo last action">&#xf01e;</span></li>-->
        <li class="tve_ed_btn tve_btn_text">
            <div class="tve_option_separator tve_save_selection">
                <i class="tve_icon tve_icon-15 tve_left"></i><span
                    class="tve_caret tve_left" id="sub_01"></span>

                <div class="tve_clear"></div>
                <div class="tve_sub_btn">
                    <div class="tve_sub active_sub_menu color_selector" id="tve_sub_01_s">
                        <ul class="tve_default_colors tve_left">
                            <li class="tve_color_title"><span class="tve_options_headline">Default Colors</span></li>
                            <li class="tve_clear"></li>
                            <li class="tve_black"><a href="#"></a></li>
                            <li class="tve_blue"><a href="#"></a></li>
                            <li class="tve_green"><a href="#"></a></li>
                            <li class="tve_orange"><a href="#"></a></li>
                            <li class="tve_clear"></li>
                            <li class="tve_purple"><a href="#"></a></li>
                            <li class="tve_red"><a href="#"></a></li>
                            <li class="tve_teal"><a href="#"></a></li>
                            <li class="tve_white"><a href="#"></a></li>
                        </ul>
                        <div class="tve_color_picker tve_left">
                            <span class="tve_options_headline tve_color_title">
                                Custom Colors
                            </span>

                            <div class="tve_colour_pickers">
                                <input type="text" class="text_colour_picker" data-default-color="#000000">
                            </div>
                            <div class="tve_clear"></div>
                            <div class="tve_remove_color_formatting tve_left">
                                <span class="tve_left tve_options_headline"><i class="tve_left tve_icon tve_icon-58"></i> Clear text colour</span>
                            </div>
                            <div class="tve_clear"></div>
                        </div>
                        <div class="tve_clear"></div>
                    </div>
                </div>
            </div>
        </li>
        <?php /* colour picker for background color (highlight of current selection */ ?>
        <li class="tve_ed_btn tve_btn_text">
            <div class="tve_option_separator tve_save_selection">
                <i class="tve_icon tve_icon-57 tve_left"></i><span
                    class="tve_caret tve_left" id="sub_01"></span>

                <div class="tve_clear"></div>
                <div class="tve_sub_btn tve_highlight_control">
                    <div class="tve_sub active_sub_menu color_selector tve_sub_generic" id="tve_sub_01_s">
                        <ul class="tve_default_colors tve_left">
                            <li class="tve_color_title"><span class="tve_options_headline">Default Colors</span></li>
                            <li class="tve_clear"></li>
                            <li class="tve_black"><a href="#"></a></li>
                            <li class="tve_blue"><a href="#"></a></li>
                            <li class="tve_green"><a href="#"></a></li>
                            <li class="tve_orange"><a href="#"></a></li>
                            <li class="tve_clear"></li>
                            <li class="tve_purple"><a href="#"></a></li>
                            <li class="tve_red"><a href="#"></a></li>
                            <li class="tve_teal"><a href="#"></a></li>
                            <li class="tve_white"><a href="#"></a></li>
                        </ul>
                        <div class="tve_color_picker tve_left">
                            <span class="tve_options_headline tve_color_title">
                                Custom Colors
                            </span>

                            <div class="tve_colour_pickers">
                                <input type="text" class="text_colour_picker" data-default-color="#000000">
                            </div>
                            <div class="tve_clear"></div>

                            <div class="tve_remove_color_formatting tve_left">
                                <span class="tve_left tve_options_headline"><i class="tve_left tve_icon tve_icon-58"></i> Clear highlight</span>
                            </div>
                            <div class="tve_clear"></div>
                        </div>
                        <div class="tve_clear"></div>
                    </div>
                </div>
            </div>
        </li>
        <li class="tve_ed_btn tve_btn_icon">
            <div class="tve_icon tve_icon-34 tve_rangy_class" data-command="bold" title="Bold"></div>
        </li>
        <li class="tve_ed_btn tve_btn_icon">
            <div class="tve_icon tve_icon-35 tve_rangy_class" data-command="italic" title="Italic"></div>
        </li>
        <li class="tve_ed_btn tve_btn_icon">
            <div class="tve_icon tve_icon-36 tve_rangy_class" data-command="underline" title="Underline"></div>
        </li>
        <li class="tve_ed_btn tve_btn_icon">
            <div class="tve_icon tve_icon-37" id="text_bullet" title="Unordered List"></div>
        </li>
        <li class="tve_ed_btn tve_btn_icon">
            <div class="tve_icon tve_icon-38" id="text_numbered_bullet" title="Numbered List"></div>
        </li>
        <li class="tve_ed_btn tve_btn_icon">
            <div class="tve_icon tve_icon-30 text_left_align" title="Text align left"></div>
        </li>
        <li class="tve_ed_btn tve_btn_icon">
            <div class="tve_icon tve_icon-39 text_center_align" title="Text align center"></div>
        </li>
        <li class="tve_ed_btn tve_btn_icon">
            <div class="tve_icon tve_icon-40 text_right_align" title="Text align right"></div>
        </li>
        <li class="tve_ed_btn tve_btn_icon">
            <div class="tve_icon tve_icon-41 text_justify_align" title="Text align justify"></div>
        </li>
        <li class="tve_ed_btn tve_btn_icon">
            <span class="tve_lb_open tve_lb_small tve_icon tve_icon-42" id="lb_text_link" title="Create link"></span>
        </li>
        <li class="tve_ed_btn tve_btn_icon">
            <span class="tve_icon tve_icon-54" id="more_link" title="Insert more link"></span>
        </li>
        <li class="tve_ed_btn tve_btn_text">
            <div class="tve_option_separator">
                <span class="tve_ind tve_left">Formatting</span>
                <span class="tve_caret tve_left" id="sub_02"></span>

                <div class="tve_clear"></div>
                <div class="tve_sub_btn">
                    <div class="tve_sub active_sub_menu">
                        <ul>
                            <li class="tve_block_change" data-tag="p">Paragraph</li>
                            <li class="tve_block_change" data-tag="address">Address</li>
                            <li class="tve_block_change" data-tag="pre">Preformatted</li>
                            <li class="tve_block_change" data-tag="blockquote">Blockquote</li>
                            <li class="tve_block_change" data-tag="h1">Heading 1</li>
                            <li class="tve_block_change" data-tag="h2">Heading 2</li>
                            <li class="tve_block_change" data-tag="h3">Heading 3</li>
                            <li class="tve_block_change" data-tag="h4">Heading 4</li>
                            <li class="tve_block_change" data-tag="h5">Heading 5</li>
                            <li class="tve_block_change" data-tag="h6">Heading 6</li>
                        </ul>
                    </div>
                </div>
            </div>
        </li>
        <li class="tve_ed_btn tve_btn_text">
            <div class="tve_option_separator">
                <span class="tve_ind tve_left">Font Size</span>
                <span class="tve_caret tve_left" id="sub_02"></span>

                <div class="tve_clear"></div>
                <div class="tve_sub_btn">
                    <div class="tve_sub active_sub_menu">
                        <ul>
                            <li data-size="10" class="tve_font_size">10px</li>
                            <li data-size="12" class="tve_font_size">12px</li>
                            <li data-size="14" class="tve_font_size">14px</li>
                            <li data-size="16" class="tve_font_size">16px</li>
                            <li data-size="18" class="tve_font_size">18px</li>
                            <li data-size="20" class="tve_font_size">20px</li>
                            <li data-size="22" class="tve_font_size">22px</li>
                            <li data-size="30" class="tve_font_size">30px</li>
                            <li data-size="36" class="tve_font_size">26px</li>
                            <li data-size="42" class="tve_font_size">42px</li>
                            <li data-size="50" class="tve_font_size">50px</li>
                            <li data-size="62" class="tve_font_size">62px</li>
                            <li data-size="70" class="tve_font_size">70px</li>
                            <li class="tve_clear_font_size">Clear Sizes in this block</li>
                        </ul>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <input type="text" id="element_id" class="tve_text" placeholder="ID">
        </li>
        <?php /*
        echo (isset($_POST['tve_api_data']['dropdown_lists']['text_menu_right'])) ? $_POST['tve_api_data']['dropdown_lists']['text_menu_right'] : "";
        echo (isset($_POST['tve_api_data']['menu_items']['text_menu_right'])) ? $_POST['tve_api_data']['menu_items']['text_menu_right'] : "";
        */
        ?>
        <li><input type="text" class="element_class tve_text" placeholder="Custom class"></li>

        <!-- this only shows when the user clicks on a hyperlink -->
        <li class="tve_link_btns tve_firstOnRow" style="clear:both"><span class="" id="text_h6"><input type="text"
                                                                                                       id="link_anchor"
                                                                                                       placeholder="Anchor Text"/></span>
        </li>
        <li class="tve_link_btns"><span class="" id="text_h6"><input type="text" id="link_url"
                                                                     placeholder="URL"/></span></li>
        <li class="tve_link_btns"><span class="" id="text_h6"><input type="text" id="anchor_name"
                                                                     placeholder="Anchor name"/></span></li>
        <li class="tve_text tve_link_btns">
            <input type="checkbox" id="link_new_window">
            <label for="link_new_window">Open link in new window?</label>
        </li>
        <li class="tve_text tve_link_btns">
            <input type="checkbox" id="link_no_follow">
            <label for="link_no_follow">Make Link no follow?</label>
        </li>
        <li class="tve_ed_btn tve_link_btns">
            <span class="tve_icon tve_icon-43" id="text_unlink"></span>
        </li>

    </ul>
</div>
<div id="text_inline_only_menu">
    <span class="tve_options_headline">Element options</span>
    <ul class="tve_menu">
        <?php
        echo (isset($_POST['tve_api_data']['dropdown_lists']['text_menu_left'])) ? $_POST['tve_api_data']['dropdown_lists']['text_menu_left'] : "";
        echo (isset($_POST['tve_api_data']['menu_items']['text_menu_left'])) ? $_POST['tve_api_data']['menu_items']['text_menu_left'] : "";
        ?>
        <li class="tve_ed_btn tve_btn_text">
            <div class="tve_option_separator">
                <i class="tve_icon tve_icon-15 tve_left"></i><span
                    class="tve_caret tve_left" id="sub_01"></span>

                <div class="tve_clear"></div>
                <div class="tve_sub_btn">
                    <div class="tve_sub active_sub_menu color_selector" id="tve_sub_01_s">
                        <ul class="tve_default_colors tve_left">
                            <li class="tve_color_title"><span class="tve_options_headline">Default Colors</span></li>
                            <li class="tve_clear"></li>
                            <li class="tve_black"><a href="#"></a></li>
                            <li class="tve_blue"><a href="#"></a></li>
                            <li class="tve_green"><a href="#"></a></li>
                            <li class="tve_orange"><a href="#"></a></li>
                            <li class="tve_clear"></li>
                            <li class="tve_purple"><a href="#"></a></li>
                            <li class="tve_red"><a href="#"></a></li>
                            <li class="tve_teal"><a href="#"></a></li>
                            <li class="tve_white"><a href="#"></a></li>
                        </ul>
                        <div class="tve_color_picker tve_left">
                            <span class="tve_options_headline tve_color_title">
                                Custom Colors
                            </span>

                            <div class="tve_colour_pickers">
                                <input type="text" class="text_colour_picker" data-default-color="#000000">
                            </div>
                        </div>
                        <div class="tve_clear"></div>
                    </div>
                </div>
            </div>
        </li>
        <li class="tve_ed_btn tve_btn_icon">
            <div class="tve_icon tve_icon-34 tve_rangy_class" data-command="bold" title="Bold"></div>
        </li>
        <li class="tve_ed_btn tve_btn_icon">
            <div class="tve_icon tve_icon-35 tve_rangy_class" data-command="italic" title="Italic"></div>
        </li>
        <li class="tve_ed_btn tve_btn_icon">
            <div class="tve_icon tve_icon-36 tve_rangy_class" data-command="underline" title="Underline"></div>
        </li>
        <li class="tve_ed_btn tve_btn_icon">
            <div class="tve_icon tve_icon-30 text_left_align" title="Text align left"></div>
        </li>
        <li class="tve_ed_btn tve_btn_icon">
            <div class="tve_icon tve_icon-39 text_center_align" title="Text align center"></div>
        </li>
        <li class="tve_ed_btn tve_btn_icon">
            <div class="tve_icon tve_icon-40 text_right_align" title="Text align right"></div>
        </li>
        <li class="tve_ed_btn tve_btn_icon">
            <div class="tve_icon tve_icon-41 text_justify_align" title="Text align justify"></div>
        </li>
        <li class="tve_ed_btn tve_btn_text">
            <div class="tve_option_separator">
                <span class="tve_ind tve_left">Font Size</span>
                <span class="tve_caret tve_left" id="sub_02"></span>

                <div class="tve_clear"></div>
                <div class="tve_sub_btn">
                    <div class="tve_sub active_sub_menu">
                        <ul>
                            <li data-size="10" class="tve_font_size">10px</li>
                            <li data-size="12" class="tve_font_size">12px</li>
                            <li data-size="14" class="tve_font_size">14px</li>
                            <li data-size="16" class="tve_font_size">16px</li>
                            <li data-size="18" class="tve_font_size">18px</li>
                            <li data-size="20" class="tve_font_size">20px</li>
                            <li data-size="22" class="tve_font_size">22px</li>
                            <li data-size="30" class="tve_font_size">30px</li>
                            <li data-size="36" class="tve_font_size">26px</li>
                            <li data-size="42" class="tve_font_size">42px</li>
                            <li data-size="50" class="tve_font_size">50px</li>
                            <li data-size="62" class="tve_font_size">62px</li>
                            <li data-size="70" class="tve_font_size">70px</li>
                            <li class="tve_clear_font_size">Clear Sizes in this block</li>
                        </ul>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <input type="text" id="element_id" class="tve_text" placeholder="ID">
        </li>
        <?php /*
        echo (isset($_POST['tve_api_data']['dropdown_lists']['text_menu_right'])) ? $_POST['tve_api_data']['dropdown_lists']['text_menu_right'] : "";
        echo (isset($_POST['tve_api_data']['menu_items']['text_menu_right'])) ? $_POST['tve_api_data']['menu_items']['text_menu_right'] : "";
        */
        ?>
        <li><input type="text" class="element_class tve_text" placeholder="Custom class"></li>

        <!-- this only shows when the user clicks on a hyperlink -->
        <li class="tve_link_btns tve_firstOnRow" style="clear:both"><span class="" id="text_h6"><input type="text"
                                                                                                       id="link_anchor"
                                                                                                       placeholder="Anchor Text"/></span>
        </li>
        <li class="tve_link_btns"><span class="" id="text_h6"><input type="text" id="link_url"
                                                                     placeholder="URL"/></span></li>
        <li class="tve_link_btns"><span class="" id="text_h6"><input type="text" id="anchor_name"
                                                                     placeholder="Anchor name"/></span></li>
        <li class="tve_text tve_link_btns">
            <input type="checkbox" id="link_new_window">
            <label for="link_new_window">Open link in new window?</label>
        </li>
        <li class="tve_ed_btn tve_link_btns">
            <span class="tve_icon tve_icon-43" id="text_unlink"></span>
        </li>

    </ul>
</div>
<div id="img_menu">
    <span class="tve_options_headline">Element options</span>
    <ul class="tve_menu">
        <?php /*
        echo (isset($_POST['tve_api_data']['dropdown_lists']['image_menu_left'])) ? $_POST['tve_api_data']['dropdown_lists']['image_menu_left'] : "";
        echo (isset($_POST['tve_api_data']['menu_items']['image_menu_left'])) ? $_POST['tve_api_data']['menu_items']['image_menu_left'] : "";
        */
        ?>
        <li class="tve_ed_btn tve_btn_icon">
            <span class="tve_icon tve_icon-30" id="img_left_align"></span>
        </li>
        <li class="tve_ed_btn tve_btn_icon">
            <span class="tve_icon tve_icon-39" id="img_center_align"></span>
        </li>
        <li class="tve_ed_btn tve_btn_icon">
            <span class="tve_icon tve_icon-40" id="img_right_align"></span>
        </li>
        <li id="img_no_align" class="tve_ed_btn tve_btn_text tve_center btn_alignment">None</li>
        <li class="tve_ed_btn">
            <span class="tve_lb_open tve_lb_small tve_icon tve_icon-42" id="lb_image_link"></span>
        </li>
        <!-- this only shows when the user clicks on a hyperlink -->
        <li class="tve_ed_btn tve_link_btns">
            <span class="tve_icon tve_icon-43" id="image_unlink"></span>
        </li>
        <li class="tve_text tve_slider_config" data-value="300" data-min-value="0" data-max-value="1800" data-related-input-id="image_width_input">
            <label for="image_width_input" class="tve_left">&nbsp;Image size</label>

            <div class="tve_slider tve_left">
                <div class="tve_slider_element" id="tve_img_size_slider"></div>
            </div>
            <input class="tve_left" type="text" id="image_width_input" value="20px">

            <div class="clear"></div>
        </li>
        <li class="tve_clear"></li>
    </ul>
    <div class="clear"></div>
    <ul class="tve_menu">
        <li id="change_image"
            class="tve_ed_btn tve_center tve_btn_text btn_alignment upload_image_cpanel">Change Image
        </li>
        <li class="">
            <label for="img_alt_att">Alt text</label>
            <input type="text" id="img_alt_att">
        </li>
        <li class="">
            <label for="img_title_att">Title text</label>
            <input type="text" id="img_title_att">
        </li>
        <?php /*
        echo (isset($_POST['tve_api_data']['dropdown_lists']['image_menu_right'])) ? $_POST['tve_api_data']['dropdown_lists']['image_menu_right'] : "";
        echo (isset($_POST['tve_api_data']['menu_items']['image_menu_right'])) ? $_POST['tve_api_data']['menu_items']['image_menu_right'] : "";
        */
        ?>
        <li class=""><input type="text" class="element_class" placeholder="Custom class"></li>
        <li class="tve_clear"></li>
    </ul>
</div>
<div id="button_menu">
    <span class="tve_options_headline">Element options</span>

    <div class="tve_menu">
        <?php /*
        echo (isset($_POST['tve_api_data']['dropdown_lists']['button_menu_left'])) ? $_POST['tve_api_data']['dropdown_lists']['button_menu_left'] : "";
        echo (isset($_POST['tve_api_data']['menu_items']['button_menu_left'])) ? $_POST['tve_api_data']['menu_items']['button_menu_left'] : "";
        */
        ?>
        <li class="tve_ed_btn tve_btn_text">
            <div class="tve_option_separator">
                <i class="tve_icon tve_icon-15 tve_left"></i>
                <span class="tve_caret tve_left" id="sub_01"></span>

                <div class="tve_clear"></div>
                <div class="tve_sub_btn">
                    <div class="tve_sub active_sub_menu color_selector" id="tve_sub_01_s">
                        <ul class="tve_default_colors tve_left">
                            <li class="tve_color_title"><span class="tve_options_headline">Default Colors</span></li>
                            <li class="tve_clear"></li>
                            <li class="tve_black"><a href="#"></a></li>
                            <li class="tve_blue"><a href="#"></a></li>
                            <li class="tve_green"><a href="#"></a></li>
                            <li class="tve_orange"><a href="#"></a></li>
                            <li class="tve_clear"></li>
                            <li class="tve_purple"><a href="#"></a></li>
                            <li class="tve_red"><a href="#"></a></li>
                            <li class="tve_teal"><a href="#"></a></li>
                            <li class="tve_white"><a href="#"></a></li>
                        </ul>
                        <div class="tve_color_picker tve_left">
                            <span class="tve_options_headline tve_color_title">
                                Custom Colors
                            </span>
                        </div>
                        <div class="tve_clear"></div>
                    </div>
                </div>
            </div>
        </li>
        <li class="tve_ed_btn tve_btn_text">
            <div class="tve_option_separator">
                <span class="tve_ind tve_left">Style 1</span><span
                    class="tve_caret tve_left" id="sub_02"></span>

                <div class="tve_clear"></div>
                <div class="tve_sub_btn">
                    <div class="tve_sub active_sub_menu">
                        <ul>
                            <li id="tve_btn1" class="tve_btn_style">Style 1</li>
                            <li id="tve_btn3" class="tve_btn_style">Style 2</li>
                            <li id="tve_btn5" class="tve_btn_style">Style 3</li>
                            <li id="tve_btn6" class="tve_btn_style">Style 4</li>
                            <li id="tve_btn7" class="tve_btn_style">Style 5</li>
                            <li id="tve_btn8" class="tve_btn_style">Style 6</li>
                        </ul>
                    </div>
                </div>
            </div>
        </li>
        <li class="tve_ed_btn tve_btn_text">
            <div class="tve_option_separator">
                <span class="tve_ind tve_left">Small</span>
                <span class="tve_caret tve_left"></span>

                <div class="tve_clear"></div>
                <div class="tve_sub_btn">
                    <div class="tve_sub active_sub_menu">
                        <ul>
                            <li id="tve_smallBtn" class="tve_btn_size">Small</li>
                            <li id="tve_normalBtn" class="tve_btn_size">Normal</li>
                            <li id="tve_bigBtn" class="tve_btn_size">Big</li>
                            <li id="tve_hugeBtn" class="tve_btn_size">Huge</li>
                        </ul>
                    </div>
                </div>
            </div>
        </li>
        <li><input type="text" id="buttonText" placeholder="Button text"/></li>
        <li><input type="text" id="buttonLink" placeholder="http://"/></li>
        <li class="tve_text">
            <input type="checkbox" id="btn_link_new_window">
            <label for="btn_link_new_window">New window?</label>
        </li>
        <li class="tve_text">
            <input type="checkbox" id="btn_nofollow">
            <label for="btn_nofollow">Nofollow?</label>
        </li>
        <li class=""><input type="text" class="element_class" placeholder="Custom class"></li>
        <li class="tve_clear"></li>
        <li class="tve_text tve_firstOnRow">
            Align:
        </li>
        <li id="tve_leftBtn" class="btn_alignment tve_alignment_left">
            Left
        </li>
        <li id="tve_centerBtn" class="btn_alignment tve_alignment_center">
            Center
        </li>
        <li id="tve_rightBtn" class="btn_alignment tve_alignment_right">
            Right
        </li>
        <li>
            <div id="tve_fullwidthBtn" class="tve_ed_btn tve_btn_text tve_center btn_alignment tve_left">Full Width
            </div>
            <div id="tve_defaultBtn" class="tve_ed_btn tve_btn_text tve_center btn_alignment tve_left">Default</div>
            <?php /*
                echo (isset($_POST['tve_api_data']['dropdown_lists']['button_menu_right'])) ? $_POST['tve_api_data']['dropdown_lists']['button_menu_right'] : "";
                echo (isset($_POST['tve_api_data']['menu_items']['button_menu_right'])) ? $_POST['tve_api_data']['menu_items']['button_menu_right'] : "";
            */
            ?>

            <div class="tve_text tve_left">
                <input type="checkbox" id="button_cc_icons">
                <label for="button_cc_icons">Show Credit Card Icons?</label>
            </div>
            <div class="tve_clear"></div>
        </li>
        <li class="tve_clear"></li>
    </div>
</div>
<div id="contentbox_menu">
    <span class="tve_options_headline">Element options</span>
    <ul class="tve_menu">
        <?php /*
            echo (isset($_POST['tve_api_data']['dropdown_lists']['content_box_menu_left'])) ? $_POST['tve_api_data']['dropdown_lists']['content_box_menu_left'] : "";
            echo (isset($_POST['tve_api_data']['menu_items']['content_box_menu_left'])) ? $_POST['tve_api_data']['menu_items']['content_box_menu_left'] : "";
        */
        ?>
        <li class="tve_ed_btn tve_btn_text">
            <div class="tve_option_separator">
                <i class="tve_icon tve_icon-15 tve_left"></i><span
                    class="tve_caret tve_left" id="sub_01"></span>

                <div class="tve_clear"></div>
                <div class="tve_sub_btn">
                    <div class="tve_sub active_sub_menu color_selector" id="tve_sub_01_s">
                        <ul class="tve_default_colors tve_left">
                            <li class="tve_color_title"><span class="tve_options_headline">Default Colors</span></li>
                            <li class="tve_clear"></li>
                            <li class="tve_black"><a href="#"></a></li>
                            <li class="tve_blue"><a href="#"></a></li>
                            <li class="tve_green"><a href="#"></a></li>
                            <li class="tve_orange"><a href="#"></a></li>
                            <li class="tve_clear"></li>
                            <li class="tve_purple"><a href="#"></a></li>
                            <li class="tve_red"><a href="#"></a></li>
                            <li class="tve_teal"><a href="#"></a></li>
                            <li class="tve_white"><a href="#"></a></li>
                        </ul>
                        <div class="tve_color_picker tve_left">
                            <span class="tve_options_headline tve_color_title">
                                Custom Colors
                            </span>
                        </div>
                        <div class="tve_clear"></div>
                    </div>
                </div>
            </div>
        </li>
        <?php /*
        echo (isset($_POST['tve_api_data']['dropdown_lists']['content_box_menu_right'])) ? $_POST['tve_api_data']['dropdown_lists']['content_box_menu_right'] : "";
        echo (isset($_POST['tve_api_data']['menu_items']['content_box_menu_right'])) ? $_POST['tve_api_data']['menu_items']['content_box_menu_right'] : "";
        */
        ?>
    </ul>
</div>
<div id="guarantee_menu">
    <span class="tve_options_headline">Element options</span>
    <ul class="tve_menu">
        <?php /*
        echo (isset($_POST['tve_api_data']['dropdown_lists']['guarantee_menu_left'])) ? $_POST['tve_api_data']['dropdown_lists']['guarantee_menu_left'] : "";
        echo (isset($_POST['tve_api_data']['menu_items']['guarantee_menu_left'])) ? $_POST['tve_api_data']['menu_items']['guarantee_menu_left'] : "";
        */
        ?>
        <li class="tve_ed_btn tve_btn_text">
            <div class="tve_option_separator">
                <i class="tve_icon tve_icon-15 tve_left"></i><span
                    class="tve_caret tve_left" id="sub_01"></span>

                <div class="tve_clear"></div>
                <div class="tve_sub_btn">
                    <div class="tve_sub active_sub_menu color_selector" id="tve_sub_01_s">
                        <ul class="tve_default_colors tve_left">
                            <li class="tve_color_title"><span class="tve_options_headline">Default Colors</span></li>
                            <li class="tve_clear"></li>
                            <li class="tve_black"><a href="#"></a></li>
                            <li class="tve_blue"><a href="#"></a></li>
                            <li class="tve_green"><a href="#"></a></li>
                            <li class="tve_orange"><a href="#"></a></li>
                            <li class="tve_clear"></li>
                            <li class="tve_purple"><a href="#"></a></li>
                            <li class="tve_red"><a href="#"></a></li>
                            <li class="tve_teal"><a href="#"></a></li>
                            <li class="tve_white"><a href="#"></a></li>
                        </ul>
                        <div class="tve_color_picker tve_left">
                            <span class="tve_options_headline tve_color_title">
                                Custom Colors
                            </span>
                        </div>
                        <div class="tve_clear"></div>
                    </div>
                </div>
            </div>
        </li>
        <?php /*
        echo (isset($_POST['tve_api_data']['dropdown_lists']['guarantee_menu_right'])) ? $_POST['tve_api_data']['dropdown_lists']['guarantee_menu_right'] : "";
        echo (isset($_POST['tve_api_data']['menu_items']['guarantee_menu_right'])) ? $_POST['tve_api_data']['menu_items']['guarantee_menu_right'] : "";
        */
        ?>
    </ul>
</div>
<div id="calltoaction_menu">
    <span class="tve_options_headline">Element options</span>
    <ul class="tve_menu">
        <?php /*
        echo (isset($_POST['tve_api_data']['dropdown_lists']['calltoaction_menu_left'])) ? $_POST['tve_api_data']['dropdown_lists']['calltoaction_menu_left'] : "";
        echo (isset($_POST['tve_api_data']['menu_items']['calltoaction_menu_left'])) ? $_POST['tve_api_data']['menu_items']['calltoaction_menu_left'] : "";
        */
        ?>
        <li class="tve_ed_btn tve_btn_text">
            <div class="tve_option_separator">
                <i class="tve_icon tve_icon-15 tve_left"></i><span
                    class="tve_caret tve_left" id="sub_01"></span>

                <div class="tve_clear"></div>
                <div class="tve_sub_btn">
                    <div class="tve_sub active_sub_menu color_selector" id="tve_sub_01_s">
                        <ul class="tve_default_colors tve_left">
                            <li class="tve_color_title"><span class="tve_options_headline">Default Colors</span></li>
                            <li class="tve_clear"></li>
                            <li class="tve_black"><a href="#"></a></li>
                            <li class="tve_blue"><a href="#"></a></li>
                            <li class="tve_green"><a href="#"></a></li>
                            <li class="tve_orange"><a href="#"></a></li>
                            <li class="tve_clear"></li>
                            <li class="tve_purple"><a href="#"></a></li>
                            <li class="tve_red"><a href="#"></a></li>
                            <li class="tve_teal"><a href="#"></a></li>
                            <li class="tve_white"><a href="#"></a></li>
                        </ul>
                        <div class="tve_color_picker tve_left">
                            <span class="tve_options_headline tve_color_title">
                                Custom Colors
                            </span>
                        </div>
                        <div class="tve_clear"></div>
                    </div>
                </div>
            </div>
        </li>
        <li class="tve_btn_text">
            <label for="call_to_action_url">URL:</label>
            <input type="text" id="call_to_action_url"/>
        </li>
        <li class="tve_ed_btn tve_btn_text tve_center">
            <a href="#" class="cta_test_link" target="_blank">Test Link</a>
        </li>
        <li class="tve_text">
            <input type="checkbox" id="cta_link_new_window">
            <label for="cta_link_new_window">Open link in new window?</label>
        </li>
        <li class="tve_text">
            <input type="checkbox" id="cta_nofollow">
            <label for="cta_nofollow">Nofollow</label>
        </li>
        <?php /*
        echo (isset($_POST['tve_api_data']['dropdown_lists']['calltoaction_menu_right'])) ? $_POST['tve_api_data']['dropdown_lists']['calltoaction_menu_right'] : "";
        echo (isset($_POST['tve_api_data']['menu_items']['calltoaction_menu_right'])) ? $_POST['tve_api_data']['menu_items']['calltoaction_menu_right'] : "";
        */
        ?>
    </ul>
</div>
<div id="testimonial_menu">
    <span class="tve_options_headline">Element options</span>
    <ul class="tve_menu">
        <?php /*
            echo (isset($_POST['tve_api_data']['dropdown_lists']['testimonial_menu_left'])) ? $_POST['tve_api_data']['dropdown_lists']['testimonial_menu_left'] : "";
            echo (isset($_POST['tve_api_data']['menu_items']['testimonial_menu_left'])) ? $_POST['tve_api_data']['menu_items']['testimonial_menu_left'] : "";
        */
        ?>
        <li class="tve_ed_btn tve_btn_text">
            <div class="tve_option_separator">
                <i class="tve_icon tve_icon-15 tve_left"></i><span
                    class="tve_caret tve_left" id="sub_01"></span>

                <div class="tve_clear"></div>
                <div class="tve_sub_btn ">
                    <div class="tve_sub active_sub_menu color_selector" id="tve_sub_01_s">
                        <ul class="tve_default_colors tve_left">
                            <li class="tve_color_title"><span class="tve_options_headline">Default Colors</span></li>
                            <li class="tve_clear"></li>
                            <li class="tve_black"><a href="#"></a></li>
                            <li class="tve_blue"><a href="#"></a></li>
                            <li class="tve_green"><a href="#"></a></li>
                            <li class="tve_orange"><a href="#"></a></li>
                            <li class="tve_clear"></li>
                            <li class="tve_purple"><a href="#"></a></li>
                            <li class="tve_red"><a href="#"></a></li>
                            <li class="tve_teal"><a href="#"></a></li>
                            <li class="tve_white"><a href="#"></a></li>
                        </ul>
                        <div class="tve_color_picker tve_left">
                            <span class="tve_options_headline tve_color_title">
                                Custom Colors
                            </span>
                        </div>
                        <div class="tve_clear"></div>
                    </div>
                </div>
            </div>
        </li>
        <li class="tve_btn_text tve_ed_btn tve_center" id="testimonial_image_btn">
            Choose Testimonial Image
        </li>
        <?php /*
        echo (isset($_POST['tve_api_data']['dropdown_lists']['testimonial_menu_right'])) ? $_POST['tve_api_data']['dropdown_lists']['testimonial_menu_right'] : "";
        echo (isset($_POST['tve_api_data']['menu_items']['testimonial_menu_right'])) ? $_POST['tve_api_data']['menu_items']['testimonial_menu_right'] : "";
        */
        ?>
    </ul>
</div>
<div id="bullets_menu">
    <span class="tve_options_headline">Element options</span>
    <ul class="tve_menu">
        <?php /*
        echo (isset($_POST['tve_api_data']['dropdown_lists']['bullets_menu_left'])) ? $_POST['tve_api_data']['dropdown_lists']['bullets_menu_left'] : "";
        echo (isset($_POST['tve_api_data']['menu_items']['bullets_menu_left'])) ? $_POST['tve_api_data']['menu_items']['bullets_menu_left'] : "";
        */
        ?>
        <li class="tve_ed_btn tve_btn_text">
            <div class="tve_option_separator">
                <i class="tve_icon tve_icon-15 tve_left"></i><span
                    class="tve_caret tve_left" id="sub_01"></span>

                <div class="tve_clear"></div>
                <div class="tve_sub_btn ">
                    <div class="tve_sub active_sub_menu color_selector tve_clearfix" id="tve_sub_01_s">
                        <ul class="tve_default_colors tve_left">
                            <li class="tve_color_title"><span class="tve_options_headline">Default Colors</span></li>
                            <li class="tve_clear"></li>
                            <li class="tve_black"><a href="#"></a></li>
                            <li class="tve_blue"><a href="#"></a></li>
                            <li class="tve_green"><a href="#"></a></li>
                            <li class="tve_orange"><a href="#"></a></li>
                            <li class="tve_clear"></li>
                            <li class="tve_purple"><a href="#"></a></li>
                            <li class="tve_red"><a href="#"></a></li>
                            <li class="tve_teal"><a href="#"></a></li>
                            <li class="tve_white"><a href="#"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </li>
        <li class="tve_ed_btn tve_btn_text">
            <div class="tve_option_separator">
                <span class="tve_ind tve_left">Style 1</span><span
                    class="tve_caret tve_left" id="sub_02"></span>

                <div class="tve_clear"></div>
                <div class="tve_sub_btn">
                    <div class="tve_sub active_sub_menu">
                        <ul>
                            <li id="tve_ul1" class="tve_bullet_style">Style 1</li>
                            <li id="tve_ul2" class="tve_bullet_style">Style 2</li>
                            <li id="tve_ul3" class="tve_bullet_style">Style 3</li>
                            <li id="tve_ul4" class="tve_bullet_style">Style 4</li>
                            <li id="tve_ul5" class="tve_bullet_style">Style 5</li>
                        </ul>
                    </div>
                </div>
            </div>
        </li>
        <?php /*
        echo (isset($_POST['tve_api_data']['dropdown_lists']['bullets_menu_right'])) ? $_POST['tve_api_data']['dropdown_lists']['bullets_menu_right'] : "";
        echo (isset($_POST['tve_api_data']['menu_items']['bullets_menu_right'])) ? $_POST['tve_api_data']['menu_items']['bullets_menu_right'] : "";
        */
        ?>
    </ul>
</div>
<div id="tabs_menu">
    <span class="tve_options_headline">Element options</span>
    <ul class="tve_menu">
        <?php /*
            echo (isset($_POST['tve_api_data']['dropdown_lists']['tabs_menu_left'])) ? $_POST['tve_api_data']['dropdown_lists']['tabs_menu_left'] : "";
            echo (isset($_POST['tve_api_data']['menu_items']['tabs_menu_left'])) ? $_POST['tve_api_data']['menu_items']['tabs_menu_left'] : "";
        */
        ?>
        <li class="tve_ed_btn tve_btn_text">
            <div class="tve_option_separator">
                <i class="tve_icon tve_icon-15 tve_left"></i><span
                    class="tve_caret tve_left" id="sub_01"></span>

                <div class="tve_clear"></div>
                <div class="tve_sub_btn">
                    <div class="tve_sub active_sub_menu color_selector tve_clearfix" id="tve_sub_01_s">
                        <ul class="tve_default_colors tve_left">
                            <li class="tve_color_title"><span class="tve_options_headline">Default Colors</span></li>
                            <li class="tve_clear"></li>
                            <li class="tve_black"><a href="#"></a></li>
                            <li class="tve_blue"><a href="#"></a></li>
                            <li class="tve_green"><a href="#"></a></li>
                            <li class="tve_orange"><a href="#"></a></li>
                            <li class="tve_clear"></li>
                            <li class="tve_purple"><a href="#"></a></li>
                            <li class="tve_red"><a href="#"></a></li>
                            <li class="tve_teal"><a href="#"></a></li>
                            <li class="tve_white"><a href="#"></a></li>
                        </ul>
                        <div class="tve_color_picker tve_left">
                            <span class="tve_options_headline tve_color_title">
                                Custom Colors
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li class="tve_add_highlight">
            <div id="add_tab" class="tve_ed_btn tve_btn_text tve_center btn_alignment tve_left">Add New Tab
            </div>
        </li>
    </ul>
</div>
<div id="toggle_menu">
    <span class="tve_options_headline">Element options</span>
    <ul class="tve_menu">
        <li class="tve_ed_btn tve_btn_text">
            <div class="tve_option_separator">
                <i class="tve_icon tve_icon-15 tve_left"></i><span
                    class="tve_caret tve_left" id="sub_01"></span>

                <div class="tve_clear"></div>
                <div class="tve_sub_btn">
                    <div class="tve_sub active_sub_menu color_selector tve_clearfix" id="tve_sub_01_s">
                        <ul class="tve_default_colors tve_left">
                            <li class="tve_color_title"><span class="tve_options_headline">Default Colors</span></li>
                            <li class="tve_clear"></li>
                            <li class="tve_black"><a href="#"></a></li>
                            <li class="tve_blue"><a href="#"></a></li>
                            <li class="tve_green"><a href="#"></a></li>
                            <li class="tve_orange"><a href="#"></a></li>
                            <li class="tve_clear"></li>
                            <li class="tve_purple"><a href="#"></a></li>
                            <li class="tve_red"><a href="#"></a></li>
                            <li class="tve_teal"><a href="#"></a></li>
                            <li class="tve_white"><a href="#"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </li>
        <li class="tve_add_highlight">
            <div id="add_toggle" class="tve_ed_btn tve_btn_text tve_center btn_alignment tve_left">Add New Toggle
            </div>
        </li>
        <?php /*
        echo (isset($_POST['tve_api_data']['dropdown_lists']['tabs_menu_right'])) ? $_POST['tve_api_data']['dropdown_lists']['tabs_menu_right'] : "";
        echo (isset($_POST['tve_api_data']['menu_items']['tabs_menu_right'])) ? $_POST['tve_api_data']['menu_items']['tabs_menu_right'] : "";
        */
        ?>
    </ul>
</div>
<div id="custom_html_menu">
    <span class="tve_options_headline">Element options</span>
    <ul class="tve_menu">
        <?php /*
        echo (isset($_POST['tve_api_data']['dropdown_lists']['custom_html_menu_left'])) ? $_POST['tve_api_data']['dropdown_lists']['custom_html_menu_left'] : "";
        echo (isset($_POST['tve_api_data']['menu_items']['custom_html_menu_left'])) ? $_POST['tve_api_data']['menu_items']['custom_html_menu_left'] : "";
        */
        ?>
        <li id="lb_custom_html"
            class="tve_ed_btn tve_btn_text tve_lb_open tve_center btn_alignment">Edit Custom HTML Code
        </li>
        <?php /*
        echo (isset($_POST['tve_api_data']['dropdown_lists']['custom_html_menu_right'])) ? $_POST['tve_api_data']['dropdown_lists']['custom_html_menu_right'] : "";
        echo (isset($_POST['tve_api_data']['menu_items']['custom_html_menu_right'])) ? $_POST['tve_api_data']['menu_items']['custom_html_menu_right'] : "";
        */
        ?>
    </ul>
</div>

<div id="feature_grid_menu">
    <span class="tve_options_headline">Element options</span>
    <ul class="tve_menu">
        <li class="tve_ed_btn tve_btn_text">
            <div class="tve_option_separator">
                <span class="tve_ind tve_left">Image Size</span><span
                    class="tve_caret tve_left" id="sub_02"></span>

                <div class="tve_clear"></div>
                <div class="tve_sub_btn">
                    <div class="tve_sub active_sub_menu">
                        <ul>
                            <li class="feature_grid_large">Large</li>
                            <li class="feature_grid_medium">Medium</li>
                            <li class="feature_grid_small">Small</li>
                        </ul>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

<div id="cc_icons_menu">
    <span class="tve_options_headline">Element options</span>
    <ul class="tve_menu">
        <li class="tve_ed_btn">
            <span class="tve_icon tve_icon-30" id="cc_left_align"></span>
        </li>
        <li class="tve_ed_btn">
            <span class="tve_icon tve_icon-39" id="cc_center_align"></span>
        </li>
        <li class="tve_ed_btn">
            <span class="tve_icon tve_icon-40" id="cc_right_align"></span>
        </li>
        <li class="tve_text">
            <input type="checkbox" class="cc_checkbox" id="cc_amex">
            <label for="cc_amex">Amex</label>
        </li>
        <li class="tve_text">
            <input type="checkbox" class="cc_checkbox" id="cc_discover">
            <label for="cc_discover">Discover</label>
        </li>
        <li class="tve_text">
            <input type="checkbox" class="cc_checkbox" id="cc_mc">
            <label for="cc_mc">Mastercard</label>
        </li>
        <li class="tve_text">
            <input type="checkbox" class="cc_checkbox" id="cc_visa">
            <label for="cc_visa">Visa</label>
        </li>
        <li class="tve_text">
            <input type="checkbox" class="cc_checkbox" id="cc_paypal">
            <label for="cc_paypal">Paypal</label>
        </li>
    </ul>
    <div class="tve_clear"></div>
</div>
<div id="pricing_table_menu">
    <span class="tve_options_headline">Element options</span>
    <ul class="tve_menu">
        <li class="tve_ed_btn tve_btn_text">
            <div class="tve_option_separator">
                <i class="tve_icon tve_icon-15 tve_left"></i><span
                    class="tve_caret tve_left" id="sub_01"></span>

                <div class="tve_clear"></div>
                <div class="tve_sub_btn">
                    <div class="tve_sub active_sub_menu color_selector" id="tve_sub_01_s">
                        <ul class="tve_default_colors tve_left">
                            <li class="tve_color_title"><span class="tve_options_headline">Default Colors</span></li>
                            <li class="tve_clear"></li>
                            <li class="tve_black"><a href="#"></a></li>
                            <li class="tve_blue"><a href="#"></a></li>
                            <li class="tve_green"><a href="#"></a></li>
                            <li class="tve_orange"><a href="#"></a></li>
                            <li class="tve_clear"></li>
                            <li class="tve_purple"><a href="#"></a></li>
                            <li class="tve_red"><a href="#"></a></li>
                            <li class="tve_teal"><a href="#"></a></li>
                            <li class="tve_white"><a href="#"></a></li>
                        </ul>
                        <div class="tve_color_picker tve_left">
                            <span class="tve_options_headline tve_color_title">
                                Custom Colors
                            </span>
                        </div>
                        <div class="tve_clear"></div>
                    </div>
                </div>
            </div>
        </li>
        <li class="tve_text">
            Move highlighted column:
        </li>
        <li id="tve_highlight_left" class="btn_alignment tve_alignment_left tve_">
            Left
        </li>
        <li id="tve_highlight_right" class="btn_alignment tve_alignment_right">
            Right
        </li>
        <li class="tve_add_highlight">
            <div id="tve_prt_add_highlight" class="tve_ed_btn tve_btn_text tve_center btn_alignment tve_left">Add
                highlighted column
            </div>
        </li>
        <li class="tve_remove_highlight">
            <div id="tve_prt_remove_highlight" class="tve_ed_btn tve_btn_text tve_center btn_alignment tve_left">Remove
                highlighted column
            </div>
        </li>
        <li><input type="text" class="element_class tve_text" placeholder="Custom class"></li>
    </ul>
    <div class="tve_clear"></div>
</div>
<div id="content_container_menu">
    <span class="tve_options_headline">Element options</span>
    <ul class="tve_menu">
        <li class="tve_text tve_firstOnRow">
            Align:
        </li>
        <li id="tve_leftCC" class="btn_alignment tve_alignment_left">
            Left
        </li>
        <li id="tve_centerCC" class="btn_alignment tve_alignment_center">
            Center
        </li>
        <li id="tve_rightCC" class="btn_alignment tve_alignment_right">
            Right
        </li>
        <li class="tve_text tve_slider_config" data-value="300" data-min-value="50"
            data-max-value="available"
            data-related-input-id="content_container_width_input"
            data-property="max-width"
            data-selector=".tve_content_inner">
            <label for="content_container_width_input" class="tve_left">&nbsp;Max-width</label>

            <div class="tve_slider tve_left">
                <div class="tve_slider_element"></div>
            </div>
            <input class="tve_left width50" type="text" id="content_container_width_input" value="300px">

            <div class="clear"></div>
        </li>
        <li class="tve_clear"></li>
    </ul>
</div>
<?php if ($is_thrive_theme && isset($_POST['page_section_colours']) && !empty($_POST['page_section_colours'])): ?>
    <div id="page_section_menu">
        <span class="tve_options_headline">Element options</span>
        <ul class="tve_menu">
            <li class="tve_ed_btn tve_btn_text">
                <div class="tve_option_separator">
                    <i class="tve_icon tve_icon-15 tve_left"></i><span
                        class="tve_caret tve_left" id="sub_01"></span>

                    <div class="tve_clear"></div>
                    <div class="tve_sub_btn">
                        <div class="tve_sub active_sub_menu color_selector" id="tve_sub_01_s">

                            <div class="tve_color_picker tve_left">
                            <span class="tve_options_headline tve_color_title">
                                Custom Colors
                            </span>
                            </div>
                            <div class="tve_clear"></div>
                        </div>
                    </div>
                </div>
            </li>
            <?php foreach ($_POST['page_section_colours'] as $colour_name => $colour_details): ?>
                <li class="tve_ed_btn tve_btn_text tve_center tve_section_color_change" title="Page Section">
                    <span class="tve_section_colour" style="background:<?php echo $colour_details['color']; ?>"></span>
                    <?php echo $colour_name; ?>
                    <input type="hidden" id="<?php echo $colour_name; ?>"/>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<div class="tve_clear"></div>
</div>
</div>
<!--lightbox stuff-->
<div class="tve_lightbox_overlay"></div>
<div class="tve_lightbox_frame">
    <a class="tve-lightbox-close" href="#" title="Close"><span class="tve_lightbox_close"></span></a>

    <div class="tve_lightbox_content"></div>
    <div class="tve_lightbox_buttons">
        <div class="tve_menu">
            <input type="button" class="tve_save_lightbox tve_btn_green" value="Save">
        </div>
    </div>
</div>