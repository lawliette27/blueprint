<?php
$tve_colour_mapping = array(
    "contentbox" => array(
        "1" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Headline Background",
                    "selector" => ".tve_hd",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Border",
                    "selector" => ".tve_cb1",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
                2 => array(
                    "label" => "Headline text shadow",
                    "selector" => ".tve_hd h1, .tve_hd h2, .tve_hd h3, .tve_hd h4, .tve_hd h5, .tve_hd h6",
                    "property" => "text-shadow",
                    "value" => "0 1px 0 [color]"
                ),
                3 => array(
                    "label" => "Background",
                    "selector" => ".tve_cb1",
                    "property" => "background-color",
                    "value" => "[color]"
                )
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Headline Gradient",
                    "selector" => ".tve_hd",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                1 => array(
                    "label" => "Headline Shadow",
                    "selector" => ".tve_hd h1, .tve_hd h2, .tve_hd h3, .tve_hd h4, .tve_hd h5, .tve_hd h6",
                    "property" => "text-shadow",
                    "value" => "0 1px 0 [color]"
                ),
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_cb1",
                    "property" => "background-color",
                    "value" => "[color]"
                )
            )
        ),
        "2" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_cb2",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Horizontal Line",
                    "selector" => ".tve_cb2 hr",
                    "property" => "background-color",
                    "value" => "[color]"
                )
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Background Gradient",
                    "selector" => ".tve_cb2",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                1 => array(
                    "label" => "Headline Shadow",
                    "selector" => ".tve_cb2 h1, .tve_cb2 h2, .tve_cb2 h3, .tve_cb2 h4, .tve_cb2 h5, .tve_cb2 h6",
                    "property" => "text-shadow",
                    "value" => "0 1px 0 [color]"
                ),
                2 => array(
                    "label" => "Horizontal Line",
                    "selector" => ".tve_cb2 hr",
                    "property" => "background-color",
                    "value" => "[color]"
                )
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Border",
                    "selector" => ".tve_cb_cnt:last",
                    "property" => "border",
                    "value" => "2px solid [color]"
                )
            ),
        ),
        "3" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Headline Background",
                    "selector" => ".tve_hd",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Box Background",
                    "selector" => ".tve_cb3",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Border Colour",
                    "selector" => ".tve_cb3",
                    "property" => "border",
                    "value" => "2px solid [color]"
                )
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Background Gradient",
                    "selector" => ".tve_cb3",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                1 => array(
                    "label" => "Divider",
                    "selector" => "hr",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Headline Background",
                    "selector" => ".tve_hd",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Border Colour",
                    "selector" => ".tve_cb3",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
                2 => array(
                    "label" => "Background Colour",
                    "selector" => ".tve_cb3",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
            ),
        ),
        "4" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Border",
                    "selector" => ".tve_cb4",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Headline Gradient",
                    "selector" => ".tve_hd",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                1 => array(
                    "label" => "Background Gradient",
                    "selector" => ".tve_cb4",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Background Colour",
                    "selector" => ".tve_cb4",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Border Colour",
                    "selector" => ".tve_cb4",
                    "property" => "border",
                    "value" => "1px solid [color]"
                ),
            ),
        ),
        "5" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_cb5",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_cb5",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                1 => array(
                    "label" => "Border",
                    "selector" => ".tve_cb5",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Border",
                    "selector" => ".tve_cb5",
                    "property" => "border",
                    "value" => "1px solid [color]"
                ),
            )
        ),
        "6" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_cb6",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Border",
                    "selector" => ".tve_cb6",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_cb6",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_cb6",
                    "property" => "background-color",
                    "value" => "[color]"
                )
            )
        )
    ),
    "testimonial" => array(
        "1" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Border",
                    "selector" => ".tve_ts1",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
                1 => array(
                    "label" => "Background",
                    "selector" => ".tve_ts1",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Picture Background",
                    "selector" => ".tve_ts_o",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                3 => array(
                    "label" => "Picture Border",
                    "selector" => ".tve_ts_o img",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Main Background",
                    "selector" => ".tve_ts.tve_ts1",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                1 => array(
                    "label" => "Button Background",
                    "selector" => ".tve_ts_o:first",
                    "property" => "background-image",
                    "value" => "gradient"
                )
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Box Border",
                    "selector" => ".tve_ts.tve_ts1",
                    "property" => "border",
                    "value" => "3px solid [color]"
                ),
                1 => array(
                    "label" => "Button Background",
                    "selector" => ".tve_ts.tve_red .tve_ts_o img",
                    "property" => "border",
                    "value" => "3px solid [color]"
                )
            )
        ),
        "2" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_ts_cn",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Image Border",
                    "selector" => ".tve_ts_imc",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
                2 => array(
                    "label" => "Divider",
                    "selector" => ".tve_ts_o",
                    "property" => "border-bottom-color",
                    "value" => "[color]"
                ),
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_ts2",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                1 => array(
                    "label" => "Name Background",
                    "selector" => ".tve_ts_o > span",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Picture Border",
                    "selector" => ".tve_ts_o img",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Box Border",
                    "selector" => ".tve_ts_t",
                    "property" => "border",
                    "value" => "3px solid [color]"
                ),
                1 => array(
                    "label" => "Picture Border",
                    "selector" => ".tve_ts_imc",
                    "property" => "border",
                    "value" => "3px solid [color]"
                ),
                2 => array(
                    "label" => "Background",
                    "selector" => ".tve_ts_t",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
            )
        ),
        "3" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Top Background",
                    "selector" => ".tve_ts_o",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Bottom Background",
                    "selector" => ".tve_ts_cn",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Divider",
                    "selector" => ".tve_ts_o",
                    "property" => "border-bottom-color",
                    "value" => "[color]"
                ),
                3 => array(
                    "label" => "Image Border",
                    "selector" => ".tve_ts_o img",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_ts3",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                1 => array(
                    "label" => "Name Background",
                    "selector" => ".tve_ts_o > span",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Picture Border",
                    "selector" => ".tve_ts_o img",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Border",
                    "selector" => ".tve_ts3",
                    "property" => "border",
                    "value" => "3px solid [color]"
                ),
                1 => array(
                    "label" => "Picture Border",
                    "selector" => ".tve_ts_o img",
                    "property" => "border",
                    "value" => "3px solid [color]"
                ),
                2 => array(
                    "label" => "Background",
                    "selector" => ".tve_ts_o, .tve_ts_cn",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
            )
        ),
        "4" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Border",
                    "selector" => ".tve_ts_cn",
                    "property" => "border",
                    "value" => "3px solid [color]"
                ),
                1 => array(
                    "label" => "Background",
                    "selector" => ".tve_ts_cn",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Image Border",
                    "selector" => ".tve_ts_imc",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_ts_cn",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Border",
                    "selector" => ".tve_ts_cn",
                    "property" => "border",
                    "value" => "1px solid [color]"
                )
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Border",
                    "selector" => ".tve_ts_cn",
                    "property" => "border",
                    "value" => "3px solid [color]"
                ),
                1 => array(
                    "label" => "Background",
                    "selector" => ".tve_ts_cn",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Picture Border",
                    "selector" => ".tve_ts_imc",
                    "property" => "border",
                    "value" => "3px solid [color]"
                ),
            )
        ),
        "5" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_ts_cn",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Image Border",
                    "selector" => ".tve_ts_imc",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_ts_cn",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Border",
                    "selector" => ".tve_ts_cn",
                    "property" => "border",
                    "value" => "1px solid [color]"
                )
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Border",
                    "selector" => ".tve_ts_cn",
                    "property" => "border",
                    "value" => "3px solid [color]"
                ),
                1 => array(
                    "label" => "Background",
                    "selector" => ".tve_ts_cn",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Picture Border",
                    "selector" => ".tve_ts_imc",
                    "property" => "border",
                    "value" => "3px solid [color]"
                ),
            )
        ),
        "6" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Border",
                    "selector" => ".tve_ts1",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
                1 => array(
                    "label" => "Background",
                    "selector" => ".tve_ts1",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Name Background",
                    "selector" => ".tve_ts_o",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Main Background",
                    "selector" => ".tve_ts1",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                1 => array(
                    "label" => "Button Background",
                    "selector" => ".tve_ts_o",
                    "property" => "background-image",
                    "value" => "gradient"
                )
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Border",
                    "selector" => ".tve_ts1",
                    "property" => "border",
                    "value" => "3px solid [color]"
                ),
                1 => array(
                    "label" => "Background",
                    "selector" => ".tve_ts1",
                    "property" => "background-color",
                    "value" => "[color]"
                )
            )
        ),
        "7" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Divider",
                    "selector" => ".tve_ts_o",
                    "property" => "border-bottom-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Background",
                    "selector" => ".tve_ts_cn",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Main Background",
                    "selector" => ".tve_ts2",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                1 => array(
                    "label" => "Name Background",
                    "selector" => ".tve_ts_o > span",
                    "property" => "background-color",
                    "value" => "[color]"
                )
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Border",
                    "selector" => ".tve_ts_t",
                    "property" => "border",
                    "value" => "3px solid [color]"
                ),
                1 => array(
                    "label" => "Background",
                    "selector" => ".tve_ts_t",
                    "property" => "background-color",
                    "value" => "[color]"
                )
            )
        ),
        "8" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Divider",
                    "selector" => ".tve_ts_o",
                    "property" => "border-bottom-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Background Top",
                    "selector" => ".tve_ts_o",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Background Bottom",
                    "selector" => ".tve_ts_cn",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Main Background",
                    "selector" => ".tve_ts3",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                1 => array(
                    "label" => "Name Background",
                    "selector" => ".tve_ts_o > span",
                    "property" => "background-image",
                    "value" => "gradient"
                )
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Border",
                    "selector" => ".tve_ts",
                    "property" => "border",
                    "value" => "3px solid [color]"
                ),
                1 => array(
                    "label" => "Background Top",
                    "selector" => ".tve_ts_o",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Background Bottom",
                    "selector" => ".tve_ts_cn",
                    "property" => "background-color",
                    "value" => "[color]"
                )
            )
        ),
        "9" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_ts_cn",
                    "property" => "background-color",
                    "value" => "[color]"
                )
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Main Background",
                    "selector" => ".tve_ts_cn",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Border",
                    "selector" => ".tve_ts_cn",
                    "property" => "border",
                    "value" => "1px solid [color]"
                )
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Border",
                    "selector" => ".tve_ts_cn",
                    "property" => "border",
                    "value" => "3px solid [color]"
                ),
                1 => array(
                    "label" => "Background",
                    "selector" => ".tve_ts_cn",
                    "property" => "background-color",
                    "value" => "[color]"
                )
            )
        )
    ),
    "calltoaction" => array(
        "1" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Button Colour",
                    "selector" => ".tve_btn",
                    "property" => "background",
                    "value" => "[color]"
                ),
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Headline Background",
                    "selector" => ".tve_line",
                    "property" => "background",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Headline Text Shadow",
                    "selector" => ".tve_ca h1, tve_ca h2, tve_ca h3, tve_ca h4, .tve_ca h5, .tve_ca h6",
                    "property" => "text-shadow",
                    "value" => "0 1px 1px [color]"
                ),
                2 => array(
                    "label" => "Button Background",
                    "selector" => ".tve_btn",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                3 => array(
                    "label" => "Button Text Shadow",
                    "selector" => ".tve_btn a > span",
                    "property" => "text-shadow",
                    "value" => "0 1px 0 [color]"
                ),
                4 => array(
                    "label" => "Border",
                    "selector" => ".tve_ca",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
                5 => array(
                    "label" => "Main Background",
                    "selector" => ".tve_ca",
                    "property" => "background",
                    "value" => "[color]"
                ),
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Border",
                    "selector" => ".tve_ca",
                    "property" => "border",
                    "value" => "3px solid [color]"
                ),
                1 => array(
                    "label" => "Button Background",
                    "selector" => ".tve_btn",
                    "property" => "background",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Main Background",
                    "selector" => ".tve_ca",
                    "property" => "background",
                    "value" => "[color]"
                )
            )
        ),
        "2" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Right Background",
                    "selector" => ".tve_ca_t",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Border",
                    "selector" => ".tve_ca",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
                2 => array(
                    "label" => "Main Background",
                    "selector" => ".tve_ca",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Button Background",
                    "selector" => ".tve_ca_t",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                1 => array(
                    "label" => "Main Background",
                    "selector" => ".tve_ca",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Border",
                    "selector" => ".tve_ca",
                    "property" => "border",
                    "value" => "2px solid [color]"
                )
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Border",
                    "selector" => ".tve_ca",
                    "property" => "border",
                    "value" => "3px solid [color]"
                ),
                1 => array(
                    "label" => "Button Border",
                    "selector" => ".tve_ca_t",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
                2 => array(
                    "label" => "Button Background",
                    "selector" => ".tve_ca_t",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                3 => array(
                    "label" => "Main Background",
                    "selector" => ".tve_ca",
                    "property" => "background-color",
                    "value" => "[color]"
                )
            )
        ),
        "3" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Border",
                    "selector" => ".tve_ca",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
                1 => array(
                    "label" => "Main Background",
                    "selector" => ".tve_ca",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Button Background",
                    "selector" => ".tve_btn",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                3 => array(
                    "label" => "Button Border",
                    "selector" => ".tve_btn",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Button Background",
                    "selector" => ".tve_btn",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                1 => array(
                    "label" => "Main Background",
                    "selector" => ".tve_ca",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Border",
                    "selector" => ".tve_ca",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
                3 => array(
                    "label" => "Button Text Shadow",
                    "selector" => "a.tve_btnLink",
                    "property" => "text-shadow",
                    "value" => "0 1px 0 [color]"
                )
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Border",
                    "selector" => ".tve_ca",
                    "property" => "border",
                    "value" => "3px solid [color]"
                ),
                1 => array(
                    "label" => "Button Border",
                    "selector" => ".tve_btn",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
                2 => array(
                    "label" => "Button Background",
                    "selector" => ".tve_btn",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                3 => array(
                    "label" => "Main Background",
                    "selector" => ".tve_ca",
                    "property" => "background-color",
                    "value" => "[color]"
                )
            )
        ),
        "4" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Border",
                    "selector" => ".tve_ca",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
                1 => array(
                    "label" => "Main Background",
                    "selector" => ".tve_ca",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Button Background",
                    "selector" => ".tve_btn",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                3 => array(
                    "label" => "Button Border",
                    "selector" => ".tve_btn",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Button Background",
                    "selector" => ".tve_btn",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                1 => array(
                    "label" => "Main Background",
                    "selector" => ".tve_ca",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Border",
                    "selector" => ".tve_ca",
                    "property" => "border",
                    "value" => "2px solid [color]"
                ),
                3 => array(
                    "label" => "Button Text Shadow",
                    "selector" => "a.tve_btnLink",
                    "property" => "text-shadow",
                    "value" => "0 1px 0 [color]"
                )
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Button Background",
                    "selector" => ".tve_btn",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Main Background",
                    "selector" => ".tve_ca",
                    "property" => "background-color",
                    "value" => "[color]"
                )
            )
        )
    ),
    "button" => array(
        "1" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Background Colour",
                    "selector" => ".tve_btn",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Vertical Divider",
                    "selector" => ".tve_btn a > div",
                    "property" => "border-right",
                    "value" => "1px solid [color]"
                ),
                2 => array(
                    "label" => "Box Shadow",
                    "selector" => ".tve_btn",
                    "property" => "box-shadow",
                    "value" => "0 5px 0 [color]"
                ),
                3 => array(
                    "label" => "Bottom Border",
                    "selector" => ".tve_btn",
                    "property" => "border-bottom-color",
                    "value" => "[color]"
                )
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Background Colour",
                    "selector" => ".tve_btn",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                1 => array(
                    "label" => "Vertical Divider",
                    "selector" => ".tve_btn a > div",
                    "property" => "border-right",
                    "value" => "1px solid [color]"
                ),
                2 => array(
                    "label" => "Box Shadow",
                    "selector" => ".tve_btn",
                    "property" => "box-shadow",
                    "value" => "0 5px 0 [color]"
                ),
                3 => array(
                    "label" => "Bottom Border",
                    "selector" => ".tve_btn",
                    "property" => "border-bottom-color",
                    "value" => "[color]"
                )
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_btn",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Vertical Divider",
                    "selector" => ".tve_btn a > div",
                    "property" => "border-right",
                    "value" => "1px solid [color]"
                ),
                2 => array(
                    "label" => "Box Shadow",
                    "selector" => ".tve_btn",
                    "property" => "box-shadow",
                    "value" => "0 5px 0 [color]"
                ),
                3 => array(
                    "label" => "Bottom Border",
                    "selector" => ".tve_btn",
                    "property" => "border-bottom-color",
                    "value" => "[color]"
                )
            )
        )
    ),
    "guarantee" => array(
        "1" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_fg",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Border",
                    "selector" => ".tve_fg",
                    "property" => "border",
                    "value" => "1px solid [color]"
                ),
                2 => array(
                    "label" => "Divider",
                    "selector" => "hr",
                    "property" => "background-color",
                    "value" => "[color]"
                )
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_fg",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Border",
                    "selector" => ".tve_fg",
                    "property" => "border",
                    "value" => "1px solid [color]"
                ),
                2 => array(
                    "label" => "Divider",
                    "selector" => "hr",
                    "property" => "background-color",
                    "value" => "[color]"
                )
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_fg",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Border",
                    "selector" => ".tve_fg",
                    "property" => "border",
                    "value" => "1px solid [color]"
                ),
            )
        ),
        "2" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_fg",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Border",
                    "selector" => ".tve_fg",
                    "property" => "border",
                    "value" => "1px solid [color]"
                ),
                2 => array(
                    "label" => "Divider",
                    "selector" => "hr",
                    "property" => "background-color",
                    "value" => "[color]"
                )
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_fg",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Border",
                    "selector" => ".tve_fg",
                    "property" => "border",
                    "value" => "1px solid [color]"
                ),
                2 => array(
                    "label" => "Divider",
                    "selector" => "hr",
                    "property" => "background-color",
                    "value" => "[color]"
                )
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_fg",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Border",
                    "selector" => ".tve_fg",
                    "property" => "border",
                    "value" => "1px solid [color]"
                ),
            )
        ),
        "3" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_fg",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Border",
                    "selector" => ".tve_fg",
                    "property" => "border",
                    "value" => "1px solid [color]"
                ),
                2 => array(
                    "label" => "Ribbon Background",
                    "selector" => ".tve_line",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_fg",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                1 => array(
                    "label" => "Border",
                    "selector" => ".tve_fg",
                    "property" => "border",
                    "value" => "1px solid [color]"
                ),
                2 => array(
                    "label" => "Ribbon Background",
                    "selector" => ".tve_line",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                3 => array(
                    "label" => "Ribbon Border",
                    "selector" => ".tve_line",
                    "property" => "border-bottom-color",
                    "value" => "[color]"
                ),
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_fg",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Border",
                    "selector" => ".tve_fg",
                    "property" => "border",
                    "value" => "1px solid [color]"
                ),
            )
        ),
        "4" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_fg",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Border",
                    "selector" => ".tve_fg",
                    "property" => "border",
                    "value" => "1px solid [color]"
                ),
                2 => array(
                    "label" => "Ribbon Background",
                    "selector" => ".tve_line",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_fg",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                1 => array(
                    "label" => "Border",
                    "selector" => ".tve_fg",
                    "property" => "border",
                    "value" => "1px solid [color]"
                ),
                2 => array(
                    "label" => "Ribbon Background",
                    "selector" => ".tve_line",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                3 => array(
                    "label" => "Ribbon Border",
                    "selector" => ".tve_line",
                    "property" => "border-bottom-color",
                    "value" => "[color]"
                ),
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Background",
                    "selector" => ".tve_fg",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Border",
                    "selector" => ".tve_fg",
                    "property" => "border",
                    "value" => "1px solid [color]"
                ),
            )
        )
    ),
    "pricing_table" => array(
        "1" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Main Column Background",
                    "selector" => ".tve_prt_in:not(.tve_prt_col.tve_hgh .tve_prt_in)",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Highlighted Column Background",
                    "selector" => ".tve_prt_col.tve_hgh .tve_prt_in",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Main Column Divider",
                    "selector" => ".tve_prt_in .tve_ftr:not(.tve_prt_col.tve_hgh .tve_prt_in .tve_ftr)",
                    "property" => "border-color",
                    "value" => "[color]"
                ),
                3 => array(
                    "label" => "Highlighted Column Divider",
                    "selector" => ".tve_prt_col.tve_hgh .tve_prt_in .tve_ftr",
                    "property" => "border-color",
                    "value" => "[color]"
                )
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Main Column Background",
                    "selector" => ".tve_prt_in:not(.tve_hgh .tve_prt_in)",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                1 => array(
                    "label" => "Highlighted Column Background",
                    "selector" => ".tve_hgh .tve_prt_in",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                2 => array(
                    "label" => "Main Column Divider",
                    "selector" => ".tve_prt_in .tve_ftr:not(.tve_prt_col.tve_hgh .tve_prt_in .tve_ftr)",
                    "property" => "border-color",
                    "value" => "[color]"
                ),
                3 => array(
                    "label" => "Highlighted Column Divider",
                    "selector" => ".tve_prt_col.tve_hgh .tve_prt_in .tve_ftr",
                    "property" => "border-color",
                    "value" => "[color]"
                ),
                4 => array(
                    "label" => "Main Column Text Shadow",
                    "selector" => ".tve_prt_in h2:not(.tve hgh .tve_prt_in h2)",
                    "property" => "text-shadow",
                    "value" => "0 1px 0 [color]"
                ),
                5 => array(
                    "label" => "Highlighted Column Text Shadow",
                    "selector" => ".tve_prt_col.tve_hgh .tve_prt_in h2",
                    "property" => "text-shadow",
                    "value" => "0 1px 0 [color]"
                ),
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Main Column Background",
                    "selector" => ".tve_prt_in:not(.tve_hgh .tve_prt_in)",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Highlighted Column Background",
                    "selector" => ".tve_hgh .tve_prt_in",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Main Column Border",
                    "selector" => ".tve_prt_in:not(.tve_hgh .tve_prt_in)",
                    "property" => "border-color",
                    "value" => "[color]"
                ),
                3 => array(
                    "label" => "Highlighted Column Border",
                    "selector" => ".tve_hgh .tve_prt_in",
                    "property" => "border-color",
                    "value" => "[color]"
                ),
                4 => array(
                    "label" => "Main Column Text Shadow",
                    "selector" => ".tve_prt_in h2:not(.tve hgh .tve_prt_in h2)",
                    "property" => "text-shadow",
                    "value" => "0 1px 0 [color]"
                ),
                5 => array(
                    "label" => "Highlighted Column Text Shadow",
                    "selector" => ".tve_prt_col.tve_hgh .tve_prt_in h2",
                    "property" => "text-shadow",
                    "value" => "0 1px 0 [color]"
                ),
            )
        )
    ),
    "page_section" => array(
        "1" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Background colour",
                    "selector" => ".out",
                    "property" => "background",
                    "value" => "[color]"
                )
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Background colour",
                    "selector" => ".out",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Background colour",
                    "selector" => ".out",
                    "property" => "background-color",
                    "value" => "[color]"
                ),
            )
        )
    ),
    "tabs" => array(
        "undefined" => array(
            "Flat" => array(
                0 => array(
                    "label" => "Active Tab Background Colour",
                    "selector" => "li.tve_tS",
                    "property" => "background",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Active Tab Border Colour",
                    "selector" => "li.tve_tS",
                    "property" => "border-color",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Standard Tab Background Colour",
                    "selector" => "li:not(li.tve_tS)",
                    "property" => "background",
                    "value" => "[color]"
                ),
                3 => array(
                    "label" => "Tab Content Background Colour",
                    "selector" => ".tve_scTC",
                    "property" => "background",
                    "value" => "[color]"
                ),
                4 => array(
                    "label" => "Tab Content Border Colour",
                    "selector" => ".tve_scTC",
                    "property" => "border-color",
                    "value" => "[color]"
                ),
            ),
            "Classy" => array(
                0 => array(
                    "label" => "Active Tab Background Colour",
                    "selector" => "li.tve_tS",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                1 => array(
                    "label" => "Active Tab Border Colour",
                    "selector" => "li.tve_tS",
                    "property" => "border-color",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Standard Tab Background Colour",
                    "selector" => "li:not(li.tve_tS)",
                    "property" => "background-image",
                    "value" => "gradient"
                ),
                3 => array(
                    "label" => "Tab Content Background Colour",
                    "selector" => ".tve_scTC",
                    "property" => "background",
                    "value" => "[color]"
                ),
                4 => array(
                    "label" => "Tab Content Border Colour",
                    "selector" => ".tve_scTC",
                    "property" => "border-color",
                    "value" => "[color]"
                ),
            ),
            "Minimal" => array(
                0 => array(
                    "label" => "Active Tab Background Colour",
                    "selector" => "li.tve_tS",
                    "property" => "background",
                    "value" => "[color]"
                ),
                1 => array(
                    "label" => "Active Tab Border Colour",
                    "selector" => "li.tve_tS",
                    "property" => "border-color",
                    "value" => "[color]"
                ),
                2 => array(
                    "label" => "Standard Tab Background Colour",
                    "selector" => "li:not(li.tve_tS)",
                    "property" => "background",
                    "value" => "[color]"
                ),
                3 => array(
                    "label" => "Tab Content Background Colour",
                    "selector" => ".tve_scTC",
                    "property" => "background",
                    "value" => "[color]"
                ),
                4 => array(
                    "label" => "Tab Content Border Colour",
                    "selector" => ".tve_scTC",
                    "property" => "border-color",
                    "value" => "[color]"
                ),
            ),
        )
    )
)
?>