<?php if (isset($_POST['caption']) && !empty($_POST['caption'])): ?>
    <div id="attachment_<?php echo $_POST['id']; ?>"
    style="width: <?php echo $_POST['width']; ?>px;"
    class="wp-caption alignnone thrv_wrapper">
<?php endif; ?>

    <img class="tve_image" alt="<?php echo $_POST['alt_text']; ?>" src="<?php echo $_POST['url']; ?>" <?php if(isset($_POST['width'])): ?>
    style="width: <?php echo $_POST['width']; ?>px;"<?php endif; ?>/>

<?php if (isset($_POST['caption']) && !empty($_POST['caption'])): ?>
    <p class="wp-caption-text"><?php echo $_POST['caption']; ?></p>
    </div>
<?php endif; ?>