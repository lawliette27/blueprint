<?php
/*
Plugin Name: Thrive Visual Editor
Plugin URI: http://www.thrivethemes.com
Version: 1.40
Author: <a href="http://www.thrivethemes.com">Thrive Themes</a>
Description: Live front end editor for your Wordpress content
*/

DEFINE("TVE_VERSION", 1.40);
DEFINE("TVE_DIR", plugins_url() . '/thrive-visual-editor');
DEFINE("TVE_EDITOR_JS", TVE_DIR . '/editor/js');
DEFINE("TVE_EDITOR_CSS", TVE_DIR . '/editor/css');
DEFINE("TVE_TEMPLATES_CSS", TVE_DIR . '/shortcodes/templates/css');


/** plugin updates **/
require 'plugin-updates/plugin-update-checker.php';
$MyUpdateChecker = new PluginUpdateChecker(
    'http://members.thrivethemes.com/plugin_versions/content_builder/update.json',
    __FILE__,
    'thrive-visual-editor'
);

add_action('edit_form_after_title', 'tve_admin_button');

// ajax calls
add_action('wp_ajax_tve_save_post', 'tve_save_post');
add_action('wp_ajax_tve_editor_display_config', 'tve_editor_display_config');
add_action('wp_ajax_tve_change_style_family', 'tve_change_style_family');
add_action('wp_ajax_tve_save_user_template', 'tve_save_user_template');
add_action('wp_ajax_tve_load_user_template', 'tve_load_user_template');
add_action('wp_ajax_tve_remove_user_template', 'tve_remove_user_template');
add_action('wp_ajax_load_element_from_api', 'tve_load_element_from_api');

add_action('wp_enqueue_scripts', 'tve_enqueue_editor_scripts');
add_action('wp_enqueue_scripts', 'tve_remove_conflicting_scripts', 100);
add_action('wp_footer', 'pre_save_filter_wrapper');
add_action('admin_menu', 'tve_add_settings_menu');

// add thrive edit link to admin bar
add_action('admin_bar_menu', 'thrive_editor_admin_bar', 100);
add_action('wp_enqueue_scripts', 'tve_frontend_enqueue_scripts');

// load script for edit page only
add_action('admin_enqueue_scripts', 'tve_edit_page_scripts');

// To fight against themes creating custom wpautop scripts and injecting rogue <br/> and <p> tags into content we have to apply shortcodes early, then add our content to the page
// at priority 101, hence the two separate "the_content" actions
if (is_editor_page_raw()) {
    add_filter('the_content', 'tve_editor_content', 102);
} else {
    add_filter('the_content', 'tve_editor_content');
}

// manipulate social sharing hooks so that they work with TCB
if (has_filter("dd_hook_wp_content")) {
    remove_filter('the_content', 'dd_hook_wp_content');
    add_filter('the_content', 'dd_hook_wp_content', 103);
}

// make sure WP editor page doesn't overwrite TCB content
add_filter('is_protected_meta', 'tve_hide_custom_fields', 10, 2);

// integration with YOAST SEO
add_filter('wpseo_pre_analysis_post_content', 'tve_yoast_seo_integration');

// add notification container
add_action('wp_footer', 'tve_add_notification_box');

// use settings API to store non post-level settings
add_action('init', 'tve_global_options_init');


// global options
// all style sheet families listed below will be added to the editor.
global $tve_style_families;
$tve_style_families = array(
    "Flat" => TVE_EDITOR_CSS . '/thrive_flat.css',
    "Classy" => TVE_EDITOR_CSS . '/thrive_classy.css',
    "Minimal" => TVE_EDITOR_CSS . '/thrive_minimal.css'
);

// control panel locations so that developers can add elements to the control panel through API
global $tve_cpanel_locations;
$tve_cpanel_locations = array(
    "main_left",
    "main_right",
    "main_extended_left",
    "main_extended_right",
    "text_menu_left",
    "text_menu_right",
    "button_menu_left",
    "button_menu_right",
    "content_box_menu_left",
    "content_box_menu_right",
    "calltoaction_menu_left",
    "calltoaction_menu_right",
    "testimonial_menu_left",
    "testimonial_menu_right",
    "bullets_menu_left",
    "bullets_menu_right",
    "tabs_menu_left",
    "tabs_menu_right",
    "custom_html_menu_left",
    "custom_html_menu_right"
);

// global variables for the API
global $tve_api_menu_items;
global $tve_api_dropdown_lists;
global $tve_api_includes;

foreach ($tve_cpanel_locations as $value) {
    $tve_api_menu_items[$value] = array();
    $tve_api_dropdown_lists[$value] = array();
}

// global variable to capture all API elements
global $tve_api_elements;
$tve_api_elements = array();

// set colour schemes for all shortcode templates.  The "tve_" prefix is added at a later stage, so no need to add these here.
global $tve_shortcode_colours;
$tve_shortcode_colours = array("black", "blue", "green", "orange", "purple", "red", "teal", "white");

// register thrive visual editor global settings
function tve_global_options_init()
{
    update_option('tve_version', TVE_VERSION);
}

// add live editor button to edit screen
function tve_admin_button()
{
    global $post;

    $post_type = get_post_type();
    if ($post_type == "page") {
        $url_var = "page_id";
    } else {
        $url_var = "p";
    }

    echo '<br/><a class="button" href="' . get_site_url() . '/?' . $url_var . '=' . get_the_ID() . '&preview=true&tve=true" id="thrive_preview_button" target="_blank"><span class="thrive-adminbar-icon"></span>Edit Using Thrive Content Builder</a><br/><br/>';
    ?>
    <style type="text/css">
        .thrive-adminbar-icon {
            background: url('<?php echo TVE_EDITOR_CSS; ?>/images/admin-bar-logo.png') no-repeat 0px 0px;
            padding-left: 25px !important;
            width: 20px !important;
            height: 20px !important;
        }
    </style>
<?php
}

// only enqueue scripts on our own editor pages
function tve_enqueue_editor_scripts()
{
    if (is_editor_page()) {

        if (get_option('tve_license_status')) {

            //set nonce for security
            $ajax_nonce = wp_create_nonce("tve-le-verify-sender-track129");

            global $tve_shortcode_colours;
            global $tve_style_families;

            // thrive content builder javascript file (loaded both frontend and backend).
            wp_enqueue_script("tve_frontend", TVE_EDITOR_JS . '/thrive_content_builder_frontend.min.js', array('jquery', 'editor'), '1.0.0', true);

            /** control panel scripts and dependancies */
            wp_enqueue_script("tve_editor", TVE_EDITOR_JS . '/editor.min.js',
                array(
                    'jquery',
                    'jquery-ui-sortable',
                    'jquery-ui-slider',
                    'jquery-ui-draggable',
                    'wp-color-picker',
                    'tve_clean_html',
                    'tve_undo_manager',
                    'tve-rangy-core',
                    'tve-rangy-css-module',
                    'tve-rangy-save-restore-module'
                ), '1.0.0', true);

            // jQuery UI stuff
            wp_enqueue_script("jquery");
            wp_enqueue_script("jquery-ui-core", array('jquery'));
            wp_enqueue_script("jquery-ui-droppable", array('jquery', 'jquery-ui-core'));
            wp_enqueue_script("jquery-ui-draggable", array('jquery', 'jquery-ui-core'));
            wp_enqueue_script("jquery-ui-slider", array('jquery', 'jquery-ui-core'));

            // WP colour picker
            wp_enqueue_script('iris', admin_url('js/iris.min.js'), array('jquery', 'jquery-ui-draggable', 'jquery-ui-slider', 'jquery-touch-punch'), false, 1);
            wp_enqueue_script('wp-color-picker', admin_url('js/color-picker.min.js'), array('jquery', 'iris'), false, 1);
            wp_enqueue_style('wp-color-picker');

            // helper scripts for various functions
            wp_enqueue_script("tve_clean_html", TVE_EDITOR_JS . '/jquery.htmlClean.min.js', array('jquery'), '1.0.0', true);
            wp_enqueue_script("tve_undo_manager", TVE_EDITOR_JS . '/tve_undo_manager.min.js', array('jquery'), '1.0.0', true);

            // we should use jquery ui sortable that comes with Wordpress but there is a bug here: http://bugs.jqueryui.com/ticket/8812 that is unresolved, so need to include own version to make modifications.
            // as soon as the fixed version of sortable is included with Wordpress we should revert back to using it.
            //wp_enqueue_script("jquery-ui-sortable");
            wp_deregister_script("jquery-ui-sortable");
            wp_enqueue_script("jquery-ui-sortable", TVE_EDITOR_JS . '/jquery-ui-sortable.min.js', array('jquery', 'jquery-ui-core', 'jquery-ui-mouse'), '1.10.3', true);

            // rangy for selection
            wp_enqueue_script("tve-rangy-core", TVE_EDITOR_JS . '/rangy-core.js', array('jquery'));
            wp_enqueue_script("tve-rangy-css-module", TVE_EDITOR_JS . '/rangy-cssclassapplier.js', array('jquery', 'tve-rangy-core'));
            wp_enqueue_script("tve-rangy-save-restore-module", TVE_EDITOR_JS . '/rangy-selectionsaverestore.js', array('jquery', 'tve-rangy-core'));

            // WP colour picker
            wp_enqueue_style('wp-color-picker');

            // now enqueue the styles
            wp_enqueue_style("tve_editor_style", TVE_EDITOR_CSS . '/editor.css');
            wp_enqueue_style("tve_default", TVE_EDITOR_CSS . '/thrive_default.css');
            wp_enqueue_style("tve_colors", TVE_EDITOR_CSS . '/thrive_colors.css');

            // load style family
            tve_enqueue_style_family();

            // load any custom css
            tve_load_custom_css();

            // scan templates directory and build array of template file names
            $shortcode_files = array_diff(scandir(dirname(__FILE__) . '/shortcodes/templates/', 1), array("..", ".", "css", "fonts", "images", "html", "js"));

            // get array of user templates
            $user_templates = get_option("tve_user_templates");

            $user_template_names = array();

            if (isset($user_templates) && is_array($user_templates)):
                $i = 0;
                foreach ($user_templates as $template_name => $template_content) {
                    array_push($user_template_names, $template_name);
                    $i++;
                }
            endif;


            // load API data into array sorted by menu location
            $tve_api_controls = tve_load_api_controls();

            // build html to be added to control panel
            $tve_api_html = tve_get_api_html($tve_api_controls);

            // get standing data array mapping id to template path
            $tve_map_api_paths = tve_get_api_paths($tve_api_controls);

            // pass any API path information
            $tve_api_includes = tve_get_api_includes();

            // list of credit cards for cc widget
            $tve_cc_icons = array("cc_amex, cc_discover, cc_mc, cc_paypal, cc_visa");

            // check if this is a thrive theme or not
            $tve_is_thrive_theme = tve_check_if_thrive_theme();

            // get a list of colours for page section shortcode
            $tve_page_section_colours = tve_get_page_section_colours();

            // get control panel display options and their defaults
            $tve_config_display_attributes = array(
                "position" => "right",
                "color" => "light"
            );

            // generate cp config array to send to front end
            $tve_cp_config = array();
            foreach ($tve_config_display_attributes as $attribute => $default_value) {
                $saved_setting = get_option("tve_config_" . $attribute, NULL);
                if ($saved_setting) {
                    $tve_cp_config[$attribute] = $saved_setting;
                } else {
                    $tve_cp_config[$attribute] = $default_value;
                }
            }

            // get loaded style family
            $current_post_style = get_post_meta(get_the_ID(), "tve_style_family", true);
            if (empty($current_post_style)) {
                $current_post_style = "Flat";
            }

            // get mapping for custom colour controls
            require_once(dirname(__FILE__) . '/custom_colour_mappings.php');

            // variables for custom colours used for post
            $tve_remembered_colours = get_option("thrv_custom_colours", null);

            if (!$tve_remembered_colours) {
                $tve_remembered_colours = array();
            }

            // pass variables needed to client side
            $tve_path_params = array(
                'cpanel_dir' => TVE_DIR . '/editor',
                'shortcodes_dir' => TVE_DIR . '/shortcodes/templates/',
                'editor_dir' => TVE_EDITOR_CSS,
                'shortcodes_array' => $shortcode_files,
                'shortcode_colours' => $tve_shortcode_colours,
                'style_families' => $tve_style_families,
                'post_id' => get_the_ID(),
                'preview_url' => get_site_url() . '/?p=' . get_the_ID() . '&preview=true',
                'tve_ajax_nonce' => $ajax_nonce,
                'tve_version' => TVE_VERSION,
                'user_templates' => $user_template_names,
                'tve_api_data' => $tve_api_html,
                'tve_api_paths' => $tve_map_api_paths,
                'tve_api_includes' => $tve_api_includes,
                'tve_cc_icons' => $tve_cc_icons,
                'is_thrive_theme' => $tve_is_thrive_theme,
                'page_section_colours' => $tve_page_section_colours,
                'cpanel_display_options' => $tve_cp_config,
                'tve_colour_mapping' => $tve_colour_mapping,
                'tve_loaded_stylesheet' => $current_post_style,
                'tve_colour_picker_colours' => $tve_remembered_colours,
                'ajax_url' => admin_url('admin-ajax.php')
            );
            wp_localize_script('tve_editor', 'tve_path_params', $tve_path_params);

            // enqueue scripts for tapping into media thickbox
            wp_enqueue_media();
        } else {
            tve_license_notice();
        }
    }

// now print scripts for preview logo in admin bar. will write directly to page because only a small snippet and thus will load faster than another external css file load.
    if (is_admin_bar_showing() && !isset($_GET['tve']) && (is_single() || is_page())): ?>
        <style type="text/css">
            .thrive-adminbar-icon {
                background: url('<?php echo TVE_EDITOR_CSS; ?>/images/admin-bar-logo.png') no-repeat 0px 0px;
                padding-left: 25px !important;
                width: 20px !important;
                height: 20px !important;
            }
        </style>
    <?php endif;

}

// only load scripts on post / page edit pages
function tve_edit_page_scripts($hook_suffix)
{
    if ('post.php' == $hook_suffix || 'post-new.php' == $hook_suffix) {
        wp_enqueue_style("wp-pointer");
        wp_enqueue_script("wp-pointer");
        wp_enqueue_script('tve_post_ready', TVE_EDITOR_JS . '/tve_admin_post_ready.min.js', array('autosave', 'wp-pointer'));
    }
}

// adds a div element to the page used to manipulate editor html before writing to database
function pre_save_filter_wrapper()
{
    if (isset($_GET["tve"]) && (current_user_can('manage_options') || current_user_can('edit_posts'))) {
        echo '<div id="pre_save_filter_wrapper" style="display:none"></div>';
    }
}

// Ajax listener to save the post in database.  Handles "Save" and "Update" buttons together.
// If either button pressed, then write to saved field.
// If publish button pressed, then write to both save and published fields
function tve_save_post()
{
    check_ajax_referer("tve-le-verify-sender-track129", "security");
    if (isset($_POST['post_id']) && current_user_can('edit_post', $_POST['post_id'])) {
        ob_clean();
        $content_split = tve_get_extended($_POST['tve_content_more']);
        update_post_meta($_POST['post_id'], "tve_content_before_more", $content_split['main']);
        update_post_meta($_POST['post_id'], "tve_content_more_found", $content_split['more_found']);
        update_post_meta($_POST['post_id'], "tve_save_post", $_POST['tve_content']);
        update_post_meta($_POST['post_id'], "tve_custom_css", $_POST['inline_rules']);
        /* user defined Custom CSS rules here, had to use different key because tve_custom_css was already used */
        update_post_meta($_POST['post_id'], 'tve_user_custom_css', $_POST['tve_custom_css']);
        update_option("thrv_custom_colours", $_POST['custom_colours']);
        if ($_POST['update'] == "true") {
            update_post_meta($_POST['post_id'], "tve_updated_post", $_POST['tve_content']);
        }
        die;
    } else {
        echo "You don't have administrator permissions to make a save!";
        die;
    }
}

/**
 *  axaj listener - saves control panel display configuration when user updates in front end.  Options are saved globally, rather than at post level
 */
function tve_editor_display_config()
{
    $attribute = $_POST['attribute'];
    $value = $_POST['value'];
    check_ajax_referer("tve-le-verify-sender-track129", "security");
    if (current_user_can('manage_options') || current_user_can('edit_posts')) {
        update_option('tve_config_' . $attribute, $value);
        return 1;
        die;
    }
}

// add the editor content to $content, but at priority 101 so not affected by custom theme shortcode functions that are common with some theme developers
function tve_editor_content($content)
{
    global $post;
    global $tve_blog_page;

    if (is_editor_page()) {
        // this is an editor page
        $tve_saved_content = get_post_meta(get_the_ID(), "tve_save_post", true);

    } else {
        // this is the frontend - check to see if on blog and find tve-more excerpt if available
        $post_id = get_the_ID();
        if (!tve_check_in_loop($post_id)) {
            tve_load_custom_css($post_id);
        }
        if (!is_singular()) {
            $more_found = get_post_meta(get_the_ID(), "tve_content_more_found", true);
            $content_before_more = get_post_meta(get_the_ID(), "tve_content_before_more", true);
            if (!empty($content_before_more) && $more_found) {
                $more_link = apply_filters('the_content_more_link', '<a href="' . get_permalink() . '#more-' . $post->ID . '" class="more-link">Continue Reading</a>', 'Continue Reading');
                $content = "<div id='tve_editor' class='tve_shortcode_editor'>" .
                    stripslashes($content_before_more) .
                    $more_link .
                    "</div>";
                return $content;
            }
        }
        $tve_saved_content = get_post_meta(get_the_ID(), "tve_updated_post", true);
    }
    $content = "<div id='tve_editor' class='tve_shortcode_editor'>" . stripslashes($tve_saved_content) . "</div>" . $content;
    return $content;
}

// determine whether the user is on the editor page or not
function is_editor_page()
{
    if (isset($_GET["tve"]) && (current_user_can('edit_post', get_the_ID())) && $_GET['preview']) {
        return true;
    } else {
        return false;
    }
}

// determine whether the user is on the editor page or not
function is_editor_page_raw()
{
    if (isset($_GET["tve"]) && $_GET['preview']) {
        return true;
    } else {
        return false;
    }
}


// load front end scripts
function tve_frontend_enqueue_scripts()
{
    wp_enqueue_style("tve_default", TVE_EDITOR_CSS . '/thrive_default.css');
    wp_enqueue_style("tve_colors", TVE_EDITOR_CSS . '/thrive_colors.css');
    tve_enqueue_style_family();

    wp_enqueue_script("tve_frontend", TVE_EDITOR_JS . '/thrive_content_builder_frontend.min.js', array('jquery'), '1.0.0', true);
    // hide tve more tag from front end display
    if (!is_editor_page()) {
        tve_load_custom_css();
        tve_hide_more_tag();
    }
}

// adds an icon and link to the admin bar for quick access to the editor. Only shows when not already in thrive content builder
function thrive_editor_admin_bar($wp_admin_bar)
{
    if (is_admin_bar_showing() && !isset($_GET['tve']) && (is_single() || is_page())) {
        $editor_link = get_site_url() . '/?p=' . get_the_ID() . '&preview=true&tve=true';
        $args = array(
            'id' => 'tve_button',
            'title' => '<span class="thrive-adminbar-icon"></span>Edit with Thrive Content Builder',
            'href' => $editor_link,
            'meta' => array(
                'class' => 'thrive-admin-bar'
            )
        );
        $wp_admin_bar->add_node($args);
    }
}

// checks post meta and enqueue correct style family
function tve_enqueue_style_family()
{
    global $tve_style_families;
    $current_post_style = get_post_meta(get_the_ID(), "tve_style_family", true);

    // Flat is default style family if nothing set
    if (empty($current_post_style)) {
        wp_enqueue_style("tve_style_family", $tve_style_families['Flat']);
    } else {
        wp_enqueue_style("tve_style_family", $tve_style_families[$current_post_style]);
    }
}

// ajax function for updating post meta with the current style family
function tve_change_style_family()
{
    check_ajax_referer("tve-le-verify-sender-track129", "security");
    if (current_user_can('manage_options') || current_user_can('edit_posts')) {
        ob_clean();
        update_post_meta($_POST['post_id'], "tve_style_family", $_POST['style_family']);
        die;
    }
}

// notice to be displayed if license not validated - going to load the styles inline because there are so few lines and not worth an extra server hit.
function tve_license_notice()
{
    ?>
    <div id="tve_license_notice">
        <img src="<?php echo TVE_EDITOR_CSS; ?>/images/Logo-Large.png">

        <p>You need to <a href="<?php echo admin_url(); ?>options-general.php?page=tve_license_validation">activate your
                license</a> before you can use the editor!</p></div>
    <style type="text/css">
        #tve_license_notice {
            width: 500px;
            margin: 0 auto;
            text-align: center;
            top: 50%;
            left: 50%;
            margin-top: -100px;
            margin-left: -250px;
            padding: 50px;
            z-index: 3000;
            position: fixed;
            -moz-border-radius-bottomleft: 10px;
            -webkit-border-bottom-left-radius: 10px;
            border-bottom-left-radius: 10px;
            -moz-border-radius-bottomright: 10px;
            -webkit-border-bottom-right-radius: 10px;
            border-bottom-right-radius: 10px;
            border-bottom: 1px solid #bdbdbd;
            background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgi…3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMDAlIiBmaWxsPSJ1cmwoI2dyYWQpIiAvPjwvc3ZnPiA=');
            background-size: 100%;
            background-image: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(20%, #ffffff), color-stop(100%, #e6e6e6));
            background-image: -webkit-linear-gradient(top, #ffffff 20%, #e6e6e6 100%);
            background-image: -moz-linear-gradient(top, #ffffff 20%, #e6e6e6 100%);
            background-image: -o-linear-gradient(top, #ffffff 20%, #e6e6e6 100%);
            background-image: linear-gradient(top, #ffffff 20%, #e6e6e6 100%);
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
            border-radius: 10px;
            -webkit-box-shadow: 2px 5px 3px #efefef;
            -moz-box-shadow: 2px 5px 3px #efefef;
            box-shadow: 2px 5px 3px #efefef;
        }
    </style>
<?php
}

function tve_license_validation()
{
    include('tve_settings.php');
}


function tve_add_settings_menu()
{
    add_options_page('Thrive Content Builder', 'Thrive Content Builder', 'manage_options', 'tve_license_validation', 'tve_license_validation');
}

function tve_add_notification_box()
{
    ?>
    <div id='tve_notification_box'></div>
<?php
}

function tve_save_user_template()
{
    check_ajax_referer("tve-le-verify-sender-track129", "security");

    if (current_user_can('manage_options') || current_user_can('edit_posts')) {
        $new_template = $_POST;
        $existing_templates = get_option("tve_user_templates");
        $existing_template_styles = get_option("tve_user_templates_styles", array());

        // check if template name exists.  If it does, return error. If not save to database and return new list of template names
        if (($existing_templates && is_array($existing_templates) && array_key_exists($new_template['template_name'], $existing_templates))) {
            $response = array(
                "success" => 0,
                "error" => "That template name already exists, please use another name"
            );
        } else {
            $existing_templates[$new_template["template_name"]] = $new_template['template_content'];
            update_option("tve_user_templates", $existing_templates);

            $existing_template_styles[$new_template["template_name"]] = $new_template['custom_css'];
            update_option("tve_user_templates_styles", $existing_template_styles);

            $response = array(
                "success" => 1,
                "template_names" => get_option("tve_user_templates"),
                "new_template_added" => stripslashes($new_template["template_name"])
            );
        }
        ob_clean();
        $response = json_encode($response);
        echo $response;
    }
    die;
}

function tve_load_user_template()
{
    check_ajax_referer("tve-le-verify-sender-track129", "security");
    if (current_user_can('manage_options') || current_user_can('edit_posts')) {
        $templates = get_option("tve_user_templates");
        $css = get_option("tve_user_templates_styles");
        $response = array(
            "html_code" => stripslashes($templates[$_POST['template_name']]),
            "css_code" => stripslashes($css[$_POST['template_name']])
        );
        ob_clean();
        $response = json_encode($response);
        echo $response;
        die;
    }
}

function tve_remove_user_template()
{
    check_ajax_referer("tve-le-verify-sender-track129", "security");
    if (current_user_can('manage_options') || current_user_can('edit_posts')) {
        $templates = get_option("tve_user_templates");
        foreach ($templates as $key => $value) {
            stripslashes($value);
        }
        unset($templates[$_POST['template_name']]);
        update_option("tve_user_templates", $templates);
        die;
    }
}

// integration with Wordpress SEO for page analysis.
function tve_yoast_seo_integration($content)
{
    $tve_saved_content = get_post_meta(get_the_ID(), "tve_updated_post", true);
    $content = $tve_saved_content . $content;
    return $content;
}

// load controls and dropdowns from API
function tve_load_api_controls()
{
    global $tve_api_menu_items;
    global $tve_api_dropdown_lists;
    $tve_api['menu_items'] = apply_filters("tcb_menu_items", $tve_api_menu_items);
    $tve_api['dropdown_lists'] = apply_filters("tcb_dropdown_lists", $tve_api_dropdown_lists);
    $tve_api_sorted = tve_organise_api_data($tve_api);
    return $tve_api;
}

// Sort each menu location by priority
function tve_organise_api_data($tve_api)
{
    global $tve_cpanel_locations;
    foreach ($tve_cpanel_locations as $menu_location) {
        usort($tve_api['menu_items'][$menu_location], "tve_cmp");
        usort($tve_api['dropdown_lists'][$menu_location], "tve_cmp");
    }
    return $tve_api;
}

// simple sorting function for multi-dimensional arrays
function tve_cmp($a, $b)
{
    return $a["menu_priority"] - $b["menu_priority"];
}

// function to take API array data and convert into HTML
// returns multidimensional array for each menu location
function tve_get_api_html($tve_api_controls)
{
    global $tve_cpanel_locations;

    // loop through the API data for each menu position and generate the HTML from the data.
    foreach ($tve_cpanel_locations as $menu_position) {
        $tve_api_html['dropdown_lists'][$menu_position] = null;
        $tve_api_html['menu_items'][$menu_position] = null;

        // handle dropdown lists
        foreach ($tve_api_controls['dropdown_lists'][$menu_position] as $key => $value) {
            $tve_api_html['dropdown_lists'][$menu_position] .= tve_generate_dropdown_html($value);
        }
        // handle menu items
        foreach ($tve_api_controls['menu_items'][$menu_position] as $key => $value) {
            $tve_api_html['menu_items'][$menu_position] .= tve_generate_menu_item_html($value);;
        }
    }
    return $tve_api_html;
}

// user heredoc to generate html for API dropdown menus
function tve_generate_dropdown_html($value)
{
    //generate icon for menu bar icon (top level of the drop down)
    $tve_generated_html = <<<EOF
    <li class="tve_btn" title="{$value['menu_tooltip_text']}">
    <span class="tve_api_icon tve_awesome tve_left tve_sub_btn tve_sub_btn_caret {$value['custom_class']}">{$value['menu_icon']} </span>
    <div class="tve_sub_btn" title="{$value['menu_tooltip_text']}">
    <div class="tve_sub">
            <ul>
EOF;

    // now generate all the sub_menu items
    foreach ($value['sub_menu_items'] as $sub_menu_key => $sub_menu_value):
        // generate html for all submenu items using heredoc
        $tve_generated_html .= <<<EOF
        <li class="cp_draggable tve_api_icon tve_api_id_{$sub_menu_value['id']} {$sub_menu_value['custom_class']}">
        {$sub_menu_value['menu_title']}
        </li>
EOF;

    endforeach;

    // now append the matching close tags
    $tve_generated_html .= <<<EOF
    </ul>
    </div>
    </div>
    </li>
EOF;
    return $tve_generated_html;
}

// Generate the HTML for the API menu items (these are single icons that don't have a dropdown attached)
function tve_generate_menu_item_html($value)
{
    $tve_generated_html = <<<EOF
<li class="tve_btn" title="{$value['menu_tooltip_text']}">
    <div class="tve_awesome tve_api_icon cp_draggable tve_api_id_{$value['id']} {$value['custom_class']} ui-draggable" title="{$value['menu_tooltip_text']}">{$value['menu_title']}</div>
</li>
EOF;

    return $tve_generated_html;
}

// searches through API data to build standing data table (as an array) of API element id and their respective template paths.
// this is passed through to the front end as an array and used to load the elements
function tve_get_api_paths($tve_api)
{
    $api_template_paths = array();

    // handle dropdown schema
    foreach ($tve_api['dropdown_lists'] as $dropdown_location_id => $dropdown_location_value) {
        foreach ($dropdown_location_value as $dropdown_list_id => $dropdown_contents) {
            foreach ($dropdown_contents['sub_menu_items'] as $sub_menu_item_id => $sub_menu_item_value) {
                $api_template_paths[$sub_menu_item_value['id']] = $sub_menu_item_value['html_on_drop'];
            }
        }
    }

    // handle menu item schema
    foreach ($tve_api['menu_items'] as $menu_items_location_id => $menu_item_location) {
        foreach ($menu_item_location as $menu_item_id => $menu_item_contents) {
            $api_template_paths[$menu_item_contents['id']] = $menu_item_contents['html_on_drop'];
        }
    }

    // return the standing data table array
    return $api_template_paths;
}

function tve_get_api_includes()
{
    global $tve_api_includes;
    $tve_api['includes'] = apply_filters("tcb_api_custom_data", $tve_api_includes);
    return $tve_api['includes'];
}

/**
 * some features in the editor can only be displayed if we have knowledge about the theme and thus should only display on a thrive theme (borderless content for instance)
 * this function checks the global variable that's set in all thrive themes to check if the user is using a thrive theme or not
 **/
function tve_check_if_thrive_theme()
{
    global $is_thrive_theme;
    if (isset($is_thrive_theme) && $is_thrive_theme == true) {
        return true;
    } else {
        return false;
    }
}

// get page section colours specific to theme through filter
function tve_get_page_section_colours()
{
    global $tve_page_section_colours;
    $tve_api['page_section_colours'] = apply_filters("tcb_page_section_colours", $tve_page_section_colours);
    return $tve_api['page_section_colours'];
}

/**
 * Hides thrive editor custom fields from being modified in the standard WP post / page edit screen
 * @param $protected
 * @param $meta_key
 * @return bool
 */
function tve_hide_custom_fields($protected, $meta_key)
{
    if ($meta_key == 'tve_save_post' ||
        $meta_key == "tve_updated_post" ||
        $meta_key == 'tve_content_before_more_shortcoded' ||
        $meta_key == 'tve_content_before_more' ||
        $meta_key == 'tve_style_family' ||
        $meta_key == 'tve_updated_post_shortcoded' ||
        $meta_key == 'tve_user_custom_css' ||
        $meta_key == 'tve_custom_css' ||
        $meta_key == 'tve_content_more_found'
    ) {
        return true;
    }
    return $protected;
}

/**
 * This is a replica of the WP function get_extended
 * The returned array has 'main', 'extended', and 'more_text' keys. Main has the text before
 * the <code><!--tvemore--></code>. The 'extended' key has the content after the
 * <code><!--tvemore--></code> comment. The 'more_text' key has the custom "Read More" text.
 * @param string $post Post content.
 * @return array Post before ('main'), after ('extended'), and custom readmore ('more_text').
 */
function tve_get_extended($post)
{
    //Match the new style more links
    if (preg_match('/<!--tvemore(.*?)?-->/', $post, $matches)) {
        list($main, $extended) = explode($matches[0], $post, 2);
        $more_text = $matches[1];
        $more_found = true;
    } else {
        $main = $post;
        $extended = '';
        $more_text = '';
        $more_found = false;
    }

    // ` leading and trailing whitespace
    $main = preg_replace('/^[\s]*(.*)[\s]*$/', '\\1', $main);
    $extended = preg_replace('/^[\s]*(.*)[\s]*$/', '\\1', $extended);
    $more_text = preg_replace('/^[\s]*(.*)[\s]*$/', '\\1', $more_text);

    return array('main' => $main, 'extended' => $extended, 'more_text' => $more_text, 'more_found' => $more_found);
}

/**
 * Adds inline script to hide more tag from the front end display
 */
function tve_hide_more_tag()
{
    ?>
    <style type="text/css">
        .tve_more_tag {
            display: none !important;
        }
    </style>
<?php
}

/**
 * Loads user defined custom css in the header to override style family css
 * If called with $post_id != null, it will load the custom css and user custom css from inside the loop (in case of homepage consisting of other pages, for example)
 */
function tve_load_custom_css($post_id = null)
{
    if (!is_null($post_id)) {
        echo sprintf(
            '<style type="text/css">%s%s</style>',
            get_post_meta($post_id, "tve_custom_css", true),
            get_post_meta($post_id, 'tve_user_custom_css', true)
        );
        return;
    }
    global $wp_query;
    $posts_to_load = $wp_query->posts;

    global $css_loaded_post_id;
    $css_loaded_post_id = array();

    /* user-defined css from the Custom CSS content element */
    $user_custom_css = '';
    if ($posts_to_load) {
        ?>
        <style type="text/css" class="tve_custom_style">
        <?php
        foreach($posts_to_load as $post) {
            $inline_styles = get_post_meta($post->ID, "tve_custom_css", true);
            echo $inline_styles;
            $user_custom_css .= get_post_meta($post->ID, 'tve_user_custom_css', true);
            array_push($css_loaded_post_id, $post->ID);
        }
        ?> </style><?php
        /* also check for user-defined custom CSS inserted via the "Custom CSS" content editor element */
        echo $user_custom_css ? sprintf('<style type="text/css" id="tve_head_custom_css">%s</style>', $user_custom_css) : '';
    }
}

/**
 * Sometimes the only way to make the plugin work with other scripts is by deregistering them on the editor page
 */
function tve_remove_conflicting_scripts()
{
    if (is_editor_page()) {

        /**  Genesis framework - Media Child theme contains a script that prevents users from being able to close the media library */
        wp_dequeue_script('yt-embed');
        wp_deregister_script('yt-embed');

    }
}

/**
 * checks to see if content being loaded is actually being loaded from within the loop (correctly) or being pulled
 * incorrectly to make up another page (for instance, a homepage that pulls different sections from pieces of content)
 */
function tve_check_in_loop($post_id)
{
    global $css_loaded_post_id;
    if (!empty($css_loaded_post_id) && in_array($post_id, $css_loaded_post_id)) {
        return true;
    }
    return false;
}